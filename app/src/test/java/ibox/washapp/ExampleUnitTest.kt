package ibox.washapp

import org.junit.Assert.assertEquals
import org.junit.Test
import java.text.SimpleDateFormat
import java.util.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }
    @Test
    fun time()
    {
        val sdf = SimpleDateFormat("dd/MM/yyyy")
        val currentDate = sdf.format(Date())
        System.out.println(" C DATE is  "+currentDate)
    }
//    @Test
//    fun Date()
//    {
//        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
//        val date =
//            dateFormat.parse("2020-02-04T13:27:31.000Z")!!
//        val formatter =
//            SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
//        val dateStr: String = formatter.format(date)// for complete date with time
//        val day = SimpleDateFormat("EE", Locale.ENGLISH).format(date.time)// for day like mon ,tue etc
//        val month = SimpleDateFormat("MMM", Locale.ENGLISH).format(date.time)// for month lik jan ,feb etc
//        val dates = SimpleDateFormat("dd", Locale.ENGLISH).format(date.time)// for date like 01,02,03
//        val mon = SimpleDateFormat("MM", Locale.ENGLISH).format(date.getTime())// for month in like 01,02,03
//        System.out.println(" C DATE is  "+dates+" "+day+" "+month+" "+mon)
//    }
}
