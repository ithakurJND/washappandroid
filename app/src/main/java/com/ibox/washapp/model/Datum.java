package com.ibox.washapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

@SerializedName("id")
@Expose
public Integer id;
@SerializedName("from_id")
@Expose
public Integer from_id;
@SerializedName("order_id")
@Expose
public Integer order_id;
@SerializedName("notif_type")
@Expose
public Integer notif_type;
@SerializedName("notif_msg")
@Expose
public String notif_msg;
@SerializedName("created_at")
@Expose
public String created_at;
@SerializedName("updated_at")
@Expose
public String updated_at;
@SerializedName("subject")
@Expose
public String subject;
@SerializedName("time_since")
@Expose
public String time_since;

}