package com.ibox.washapp.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PojoOrderDetails {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("order_type")
    @Expose
    public Integer order_type;
    @SerializedName("OrderId")
    @Expose
    public String orderId;
    @SerializedName("pickup_date")
    @Expose
    public String pickup_date;
    @SerializedName("pickup_time_slot")
    @Expose
    public String pickup_time_slot;
    @SerializedName("drop_off_date")
    @Expose
    public String drop_off_date;
    @SerializedName("drop_off_time_slot")
    @Expose
    public String drop_off_time_slot;
    @SerializedName("pickup_address")
    @Expose
    public String pickup_address;
    @SerializedName("dropoff_address")
    @Expose
    public String dropoff_address;
    @SerializedName("special_note")
    @Expose
    public String special_note;
    @SerializedName("status")
    @Expose
    public Integer status;
    @SerializedName("amount")
    @Expose
    public Float amount;
    @SerializedName("service_type")
    @Expose
    public Integer service_type;
    @SerializedName("created_at")
    @Expose
    public String created_at;
    @SerializedName("updated_at")
    @Expose
    public String updated_at;
    @SerializedName("deleted_at")
    @Expose
    public Object deleted_at;
    @SerializedName("cancelled_at")
    @Expose
    public Object cancelled_at;
    @SerializedName("completed_at")
    @Expose
    public Object completed_at;
    @SerializedName("user_id")
    @Expose
    public Integer user_id;
    @SerializedName("laundry_id")
    @Expose
    public Integer laundry_id;
    @SerializedName("pickup_address_id")
    @Expose
    public Integer pickup_address_id;
    @SerializedName("drop_off_address_id")
    @Expose
    public Integer drop_off_address_id;

    @SerializedName("preference")
    @Expose
    public String  preference;
    @SerializedName("preferrence_id")
    @Expose
    public Integer preferrence_id;
    @SerializedName("area_id")
    @Expose
    public Object area_id;
    @SerializedName("emirate_id")
    @Expose
    public Object emirate_id;
    @SerializedName("laundry_name")
    @Expose
    public String laundry_name;
    @SerializedName("order_items")
    @Expose
    public List<Product> order_items = null;
    @SerializedName("categories")
    @Expose
    public List<String> categories = null;
    @SerializedName("is_rated")
    @Expose
    public Integer is_rated;
    @SerializedName("payment")
    @Expose
    public Payment payment;

}