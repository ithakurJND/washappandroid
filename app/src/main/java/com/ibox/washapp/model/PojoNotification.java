package com.ibox.washapp.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PojoNotification {

@SerializedName("data")
@Expose
public List<Datum> data = null;
@SerializedName("count")
@Expose
public Integer count;

}
