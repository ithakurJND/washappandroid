
package com.ibox.washapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Payment {

@SerializedName("id")
@Expose
public Integer id;
@SerializedName("payment_method")
@Expose
public Integer payment_method;
@SerializedName("payment_type")
@Expose
public Integer payment_type;
@SerializedName("coupen_applied")
@Expose
public String coupen_applied;
@SerializedName("description")
@Expose
public Object description;
@SerializedName("TransactionId")
@Expose
public String transactionId;
@SerializedName("amount")
@Expose
public Float amount;
@SerializedName("payable_amount")
@Expose
public Float payable_amount;
@SerializedName("credits")
@Expose
public String credits;
@SerializedName("invoice_id")
@Expose
public String invoice_id;
@SerializedName("created_at")
@Expose
public String created_at;
@SerializedName("updated_at")
@Expose
public String updated_at;
@SerializedName("deleted_at")
@Expose
public Object deleted_at;
@SerializedName("user_id_1")
@Expose
public Integer user_id_1;
@SerializedName("user_id_2")
@Expose
public Integer user_id_2;
@SerializedName("order_id")
@Expose
public Integer order_id;

}