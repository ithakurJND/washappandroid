package com.ibox.washapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Product {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("quantity")
    @Expose
    public int quantity = 0;

    @SerializedName("regular_price")
    @Expose
    public Integer regular_price;
    @SerializedName("urgent_price")
    @Expose
    public Integer urgent_price;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("category_id")
    @Expose
    public Integer category_id;
    @SerializedName("created_at")
    @Expose
    public String created_at;
    @SerializedName("updated_at")
    @Expose
    public String updated_at;
    @SerializedName("deleted_at")
    @Expose
    public Object deleted_at;

    @SerializedName("product_name")
    @Expose
    public String product_name;
    @SerializedName("category_name")
    @Expose
    public String category_name;
    @SerializedName("product_image")
    @Expose
    public String product_image;
    @SerializedName("product_id")
    @Expose
    public String product_id;

}