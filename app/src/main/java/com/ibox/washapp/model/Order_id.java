package com.ibox.washapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Order_id {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("order_type")
    @Expose
    public String order_type;
    @SerializedName("pickup_date")
    @Expose
    public String pickup_date;
    @SerializedName("pickup_time_slot")
    @Expose
    public String pickup_time_slot;
    @SerializedName("drop_off_date")
    @Expose
    public String drop_off_date;
    @SerializedName("drop_off_time_slot")
    @Expose
    public String drop_off_time_slot;
    @SerializedName("special_note")
    @Expose
    public String special_note;
    @SerializedName("amount")
    @Expose
    public Integer amount;
    @SerializedName("service_type")
    @Expose
    public Integer service_type;
    @SerializedName("created_at")
    @Expose
    public String created_at;
    @SerializedName("updated_at")
    @Expose
    public String updated_at;
    @SerializedName("deleted_at")
    @Expose
    public String deleted_at;
    @SerializedName("user_id")
    @Expose
    public Integer user_id;
    @SerializedName("laundry_id")
    @Expose
    public Integer laundry_id;
    @SerializedName("pickup_address_id")
    @Expose
    public Integer pickup_address_id;
    @SerializedName("drop_off_address_id")
    @Expose
    public Integer drop_off_address_id;
    @SerializedName("status")
    @Expose
    public Integer status;
    @SerializedName("OrderId")
    @Expose
    public Integer OrderId;
    @SerializedName("cancelled_at")
    @Expose
    public String cancelled_at;
    @SerializedName("pickup_address")
    @Expose
    public String pickup_address;
    @SerializedName("dropoff_address")
    @Expose
    public String dropoff_address;
    @SerializedName("completed_at")
    @Expose
    public String completed_at;
    @SerializedName("preferrence_id")
    @Expose
    public Integer preferrence_id;
    @SerializedName("is_basket_complete")
    @Expose
    public String is_basket_complete;
    @SerializedName("area_id")
    @Expose
    public String area_id;
    @SerializedName("order_id")
    @Expose
    public Integer order_id;
    @SerializedName("is_rated")
    @Expose
    public String is_rated;
    @SerializedName("laundry_name")
    @Expose
    public String laundry_name;
}
