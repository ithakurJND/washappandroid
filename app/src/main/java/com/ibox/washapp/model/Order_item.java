package com.ibox.washapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Order_item {

@SerializedName("id")
@Expose
public Integer id;
@SerializedName("quantity")
@Expose
public Integer quantity;
@SerializedName("created_at")
@Expose
public String created_at;
@SerializedName("updated_at")
@Expose
public String updated_at;
@SerializedName("deleted_at")
@Expose
public Object deleted_at;
@SerializedName("order_id")
@Expose
public Integer order_id;
@SerializedName("product_id")
@Expose
public Integer product_id;
@SerializedName("regular_price")
@Expose
public Integer regular_price;
@SerializedName("urgent_price")
@Expose
public Integer urgent_price;
@SerializedName("category_id")
@Expose
public Integer category_id;
@SerializedName("product_name")
@Expose
public String product_name;
@SerializedName("category_name")
@Expose
public String category_name;
@SerializedName("product_image")
@Expose
public String product_image;

}