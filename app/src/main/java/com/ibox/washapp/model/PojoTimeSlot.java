package com.ibox.washapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PojoTimeSlot {

@SerializedName("id")
@Expose
public Integer id;
@SerializedName("time_slot")
@Expose
public String time_slot;
@SerializedName("created_at")
@Expose
public String created_at;
@SerializedName("updated_at")
@Expose
public String updated_at;
@SerializedName("deleted_at")
@Expose
public Object deleted_at;

}