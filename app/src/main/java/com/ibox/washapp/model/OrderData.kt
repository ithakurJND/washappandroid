package com.ibox.washapp.model

class OrderData {
    public var product: HashMap<Int, com.ibox.washapp.model.Product>? = null
    public var laundry_id = 0
    public var service_type = 0
    public var laundpickup_address_idry_id = 0
    public var order_type = 0
    public var area_id =0
}