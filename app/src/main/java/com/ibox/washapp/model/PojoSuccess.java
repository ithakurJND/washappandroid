package com.ibox.washapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PojoSuccess {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("credits")
    @Expose
    public Integer credits;
    @SerializedName("amount")
    @Expose
    public String amount;

    @SerializedName("final_amount")
    @Expose
    public String final_amount;

}