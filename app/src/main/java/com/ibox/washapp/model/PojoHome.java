package com.ibox.washapp.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PojoHome {

    @SerializedName("banner_imgs")
    @Expose
    public List<PojoBanner> banner_imgs = null;
    @SerializedName("services")
    @Expose
    public List<PojoCategory> services = null;

    @SerializedName("days")
    @Expose
    public List<Day> days = null;

    @SerializedName("order_id")
    @Expose
    public Order_id  order_id = new Order_id();
}