package com.ibox.washapp.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PojoOrderCategory {

@SerializedName("id")
@Expose
public Integer id;

public int page;
@SerializedName("name")
@Expose
public String name;
@SerializedName("products")
@Expose
public List<Product> products = null;

}