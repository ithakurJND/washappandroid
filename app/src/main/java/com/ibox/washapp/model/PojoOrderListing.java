package com.ibox.washapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PojoOrderListing {

@SerializedName("id")
@Expose
public Integer id;
@SerializedName("OrderId")
@Expose
public String OrderId;
@SerializedName("laundry_id")
@Expose
public Integer laundry_id;
@SerializedName("laundry_name")
@Expose
public String laundry_name;
@SerializedName("laundry_logo")
@Expose
public String laundry_logo;
@SerializedName("created_at")
@Expose
public String created_at;
@SerializedName("status")
@Expose
public Integer status;
@SerializedName("payment_type")
@Expose
public Object payment_type;

}