package com.ibox.washapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PojoSchdeule {

@SerializedName("id")
@Expose
public Integer id;
@SerializedName("schedule_no")
@Expose
public Integer schedule_no;
@SerializedName("pickup_date")
@Expose
public String pickup_date;
@SerializedName("pickup_time_slot")
@Expose
public String pickup_time_slot;
@SerializedName("dropoff_date")
@Expose
public String dropoff_date;
@SerializedName("dropoff_time_slot")
@Expose
public String dropoff_time_slot;
@SerializedName("created_at")
@Expose
public String created_at;
@SerializedName("updated_at")
@Expose
public String updated_at;
@SerializedName("deleted_at")
@Expose
public Object deleted_at;

}