package com.ibox.washapp.model

data class Calender(var day: String, var date: String, var month: String, var mon: String)
data class ServiceWeOffer(var image: Int, var name: String)
data class AddAddress(
    val address_id: Int,
    val address: String,
    val area_id: Int,
    val building_name: String,
    val city: String,
    val emirate_id: Int,
    val flat_no: String,
    val is_default: Int,
    val lat: Double,
    val lng: Double,
    val postal_code: String,
    val title: String,
    val user_id: Int
)

data class UserLogin(
    val user: User?,
    val user_token: UserToken?,
    var errorMessage: String?
)

data class User(
    val addresses: MutableList<Address>,
    val blocked_at: Any,
    val created_at: String,
    val credits: Any,
    val deleted_at: Any,
    val email: String,
    val fb_id: String?,
    val google_id: String?,
    val id: Int,
    val image: String,
    val is_approved: Any,
    val is_phone_verified: Any,
    val name: String,
    val phone_no: String?,
    val tax_reg_no: Any,
    val trade_license: Any,
    val updated_at: String,
    val user_type: Int
)

data class Address(
    val address: String,
    val area_id: Int,
    val building_name: String,
    val city: String,
    val created_at: String,
    val deleted_at: Any,
    val emirate_id: Int,
    val flat_no: String,
    val id: Int,
    val is_default: Int,
    val lat: Float,
    val lng: Float,
    val postal_code: String,
    val title: String,
    val updated_at: Any,
    val user_id: Int
)

data class UserToken(
    val access_token: String,
    val expires_in: String
)

data class Error(
    val error: String,
    val message: String
)

data class LoginModel(
    val device_type: Int,
    val email: String,
    val fb_id: String,
    val fcm_id: String,
    val google_id: String,
    val name: String,
    val password: String
)

data class AddToBasket(
    val created_at: String,
    val deleted_at: Any,
    val id: Int,
    val name: String,
    val products: List<com.ibox.washapp.model.Product>,
    val updated_at: String
)


data class LaundryList(
    val badge: String,
    val address: String,
    val area_id: Int,
    val building_name: String,
    val categories: List<Category>?,
    val created_at: String,
    val deleted_at: Any,
    val emirate_id: Int,
    val id: Int,
    val lat: Any,
    val lng: Any,
    val logo: String,
    val name: String,
    val postal_code: Any,
    val rating: Float,
    val shop_no: String,
    val slogan: String,
    val updated_at: Any,
    val user_id: Int
)

data class Category(
    val id: Int,
    val name: String
)

data class Locations(
    val area: String,
    val created_at: String,
    val deleted_at: String,
    val drop_off_price: Int,
    val emirate: String,
    val emirate_id: Int,
    val id: Int,
    val lat: Double,
    val lng: Double,
    val pickup_price: Int,
    val updated_at: String,
    val area_id: Int
)

data class SignUpModel(
    val device_type: Int,
    val email: String,
    val fcm_id: String,
    val name: String,
    val password: String
)

data class Message(
    val message: String
)

data class EditProfile(
    val id: Int,
    var message: String
)

data class HomeModel(
    val banner_imgs: List<BannerImg>,
    val days: List<Day>,
    val services: List<Service>,
    val order: Order?
)

data class BannerImg(
    val created_at: String,
    val deleted_at: Any,
    val id: Int,
    val image: String,
    val updated_at: String
)

data class Day(
    val created_at: String,
    val day: Int,
    val deleted_at: Any,
    val id: Int,
    val service_type: Int,
    val updated_at: Any
)

data class Order(
    val OrderId: Int,
    val amount: Int,
    val area_id: Int,
    val cancelled_at: Any,
    val completed_at: String,
    val created_at: String,
    val deleted_at: Any,
    val delivery_dis_amount: Any,
    val drop_off_address_id: Int,
    val drop_off_date: String,
    val drop_off_time_slot: String,
    val dropoff_address: String,
    val dropoff_amount: Int,
    val id: Int,
    val is_basket_complete: Any,
    val is_rated: String,
    val laundry_id: Int,
    val laundry_name: String,
    val order_id: Int,
    val order_type: Int,
    val pickup_address: String,
    val pickup_address_id: Int,
    val pickup_amount: Int,
    val pickup_date: String,
    val pickup_time_slot: String,
    val preferrence_id: Any,
    val service_type: Int,
    val special_note: Any,
    val status: Int,
    val updated_at: String,
    val user_id: Int
)

data class Service(
    val created_at: String,
    val deleted_at: Any,
    val id: Int,
    val image: String,
    val name: String,
    val updated_at: String
)

data class ReviewResponse(
    val count: Int,
    val detail: List<Detail>
)

data class Detail(
    val created_at: String,
    val id: Int,
    val rated_by: String,
    val rater_id: Int,
    val rater_image: Any,
    val rating: Float,
    val review: String
)

data class OrderResponeLaundry(
    val amount: Int,
    val credits: Int,
    val id: Int,
    val message: String,
    val order_items: List<OrderItem>
)

data class OrderItem(
    val created_at: String,
    val deleted_at: Any,
    val id: Int,
    val product_name: String,
    val product_image: String,
    val order_id: Int,
    val product_id: Int,
    val quantity: Int,
    val regular_price: Int,
    val updated_at: String,
    val urgent_price: Int
)
data class NewNewOrder(
    val OrderId: Int,
    val amount: Int,
    val area_id: Int,
    val cancelled_at: Any,
    val categories: List<String>,
    val completed_at: Any,
    val created_at: String,
    val deleted_at: Any,
    val delivery_dis_amount: Any,
    val drop_off_address_id: Int,
    val drop_off_date: String,
    val drop_off_time_slot: String,
    val dropoff_address: String,
    val dropoff_amount: Int,
    val emirate_id: Int,
    val id: Int,
    val is_basket_complete: Any,
    val is_rated: Int,
    val laundry_id: Int,
    val laundry_name: String,
    val order_items: List<OrderItem>,
    val order_type: Int,
    val payment: PaymentNew,
    val pickup_address: String,
    val pickup_address_id: Int,
    val pickup_amount: Int,
    val pickup_date: String,
    val pickup_time_slot: String,
    val preference: String,
    val preferrence_id: Int,
    val service_type: Int,
    val special_note: String,
    val status: Int,
    val updated_at: String,
    val user_id: Int
)

data class PaymentNew(
    val TransactionId: String,
    val amount: Int,
    val coupen_applied: String,
    val created_at: String,
    val credits: Any,
    val deleted_at: Any,
    val description: Any,
    val id: Int,
    val invoice_id: String,
    val order_id: Int,
    val payable_amount: Int,
    val payment_method: Int,
    val payment_type: Int,
    val status: Int,
    val updated_at: String,
    val user_id_1: Int,
    val user_id_2: Int
)
data class NewLocations(
    val areas: List<Area>,
    val count: Int
)

data class Area(
    val area: String,
    val created_at: String,
    val deleted_at: String,
    val drop_off_price: Int,
    val emirate: String,
    val emirate_id: Int,
    val id: Int,
    val lat: Double,
    val lng: Double,
    val pickup_price: Int,
    val updated_at: String,
    val area_id: Int
)
data class GetRSA(
    val rsa_key: String
)