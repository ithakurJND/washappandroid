package com.ibox.washapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PojoBanner {

@SerializedName("id")
@Expose
public Integer id;
@SerializedName("image")
@Expose
public String image;
@SerializedName("created_at")
@Expose
public String created_at;
@SerializedName("updated_at")
@Expose
public String updated_at;
@SerializedName("deleted_at")
@Expose
public Object deleted_at;

}