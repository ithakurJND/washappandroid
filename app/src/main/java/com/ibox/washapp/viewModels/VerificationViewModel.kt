package com.ibox.washapp.viewModels

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.techautovity.myeve.retrofit.RestClient
import com.ibox.washapp.model.Message
import com.ibox.washapp.utils.CommonFunctions
import com.ibox.washapp.utils.Constants
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class VerificationViewModel : ViewModel() {
    private var resendMutableLiveData = MutableLiveData<Message>()
    private var varificationMutableLiveData = MutableLiveData<Message>()
    fun varifyResponse()=varificationMutableLiveData

    fun otpVerification(numberOtp: String,context: Context) {
        val otp = HashMap<String, String>()
        otp.put("otp", numberOtp)
        CommonFunctions.showProgress(context)
        RestClient.modalApiService.otpVerify(com.ibox.washapp.utils.Prefs.with(context).getString(Constants.PREFS_TOKEN,""),otp).enqueue(object : Callback<Message> {
            override fun onFailure(call: Call<Message>, t: Throwable) {
                CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(context,t.message!!)
            }

            override fun onResponse(call: Call<Message>, response: Response<Message>) {
               CommonFunctions.dismissProgress()
                if (response.isSuccessful) {
                    Log.i("OTP", "Success" + response.body())
                    varificationMutableLiveData.value = response.body()
                } else {
                    CommonFunctions.handleError(context,response.errorBody(),response.code())
                    Log.i("OTP", "Success" + response.errorBody())
                }
            }
        })
    }
    fun resendOtp(numberOtp: String,context: Context,code:String) {
        val otp = HashMap<String, String>()
        otp.put("phone_no", numberOtp)
        otp.put("country_code",code)
        CommonFunctions.showProgress(context)
        RestClient.modalApiService.resendOtp(com.ibox.washapp.utils.Prefs.with(context).getString(Constants.PREFS_TOKEN,""),otp).enqueue(object : Callback<Message> {
            override fun onFailure(call: Call<Message>, t: Throwable) {
                CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(context,t.message!!)
            }

            override fun onResponse(call: Call<Message>, response: Response<Message>) {
                CommonFunctions.dismissProgress()
                if (response.isSuccessful) {
                    Log.i("OTP", "Success" + response.body())
                   Toast.makeText(context,"Otp Sent",Toast.LENGTH_SHORT).show()
                } else {
                    CommonFunctions.handleError(context,response.errorBody(),response.code())
                    Log.i("OTP", "Success" + response.errorBody())
                }
            }
        })
    }
}