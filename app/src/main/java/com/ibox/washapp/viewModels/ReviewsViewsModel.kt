package com.ibox.washapp.viewModels

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.techautovity.myeve.retrofit.RestClient
import com.ibox.washapp.model.ReviewResponse
import com.ibox.washapp.utils.CommonFunctions
import com.ibox.washapp.utils.Constants
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ReviewsViewsModel : ViewModel(){

    var reviwesMutableLiveData = MutableLiveData<ReviewResponse>()


    fun laundryReviwList(
        context: Context, la:Int,
        isLoader: Boolean,page:Int) {
        val map = HashMap<String,Any>()
        map["laundry_id"] = la
        map["page"] = page
        if (isLoader)
        CommonFunctions.showProgress(context)
        RestClient.modalApiService.getReviewAl(com.ibox.washapp.utils.Prefs.with(context).getString(Constants.PREFS_TOKEN,""),map).enqueue(object :
            Callback<ReviewResponse> {
            override fun onFailure(call: Call<ReviewResponse>, t: Throwable) {
                CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(context,t.message!!)
                Log.i("LaundryReview", " " + t.message)
            }

            override fun onResponse(call: Call<ReviewResponse>,
                                    response: Response<ReviewResponse>

            ) {
                CommonFunctions.dismissProgress()
                if (response.isSuccessful) {
                    Log.i("LaundryReview", " " + response.body())
                    reviwesMutableLiveData.value = response.body() }
                else {
                    CommonFunctions.handleError(context,response.errorBody(),response.code())
                    Log.i("LaundryReview", " " + response.errorBody())
                }
            }
        })
    }
}