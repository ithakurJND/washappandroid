package com.ibox.washapp.viewModels

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.techautovity.myeve.retrofit.RestClient
import com.ibox.washapp.model.*
import com.ibox.washapp.utils.CommonFunctions
import com.ibox.washapp.utils.Constants
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LaundryViewModel : ViewModel() {


    var laundryMutableLiveData = MutableLiveData<MutableList<LaundryList>>()
    var orderMutableLiveData = MutableLiveData<List<com.ibox.washapp.model.PojoOrderCategory>>()
    var orderDetailsMutableLiveData = MutableLiveData<com.ibox.washapp.model.PojoOrderDetails>()
    var createOrderMutableLiveData = MutableLiveData<OrderResponeLaundry>()
    var paymentMutableLiveData = MutableLiveData<com.ibox.washapp.model.PojoSuccess>()
    var cancelOrderLiveData = MutableLiveData<com.ibox.washapp.model.PojoSuccess>()
    var productPagingMutableLiveData=MutableLiveData<List<com.ibox.washapp.model.Product>>()

    var appLyCoupMutableLiveData = MutableLiveData<com.ibox.washapp.model.PojoSuccess>()
    fun getVendors() = laundryMutableLiveData
    fun getProducts() = orderMutableLiveData
    fun createOrder() = createOrderMutableLiveData
    fun getPaging()=productPagingMutableLiveData

    fun laundryList(context: Context, map: HashMap<String, Any>) {
        CommonFunctions.showProgress(context)
        RestClient.modalApiService.laundriesList(
            com.ibox.washapp.utils.Prefs.with(context).getString(
                Constants.PREFS_TOKEN,
                ""
            ), map
        ).enqueue(object : Callback<MutableList<LaundryList>> {
            override fun onFailure(call: Call<MutableList<LaundryList>>, t: Throwable) {
                CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(context, t.message!!)
                Log.i("Laundry", " " + t.message)
            }

            override fun onResponse(
                call: Call<MutableList<LaundryList>>,
                response: Response<MutableList<LaundryList>>
            ) {
                CommonFunctions.dismissProgress()
                if (response.isSuccessful) {
                    Log.i("Laundry", " " + response.body())
                    laundryMutableLiveData.value = response.body()
                } else {
                    CommonFunctions.handleError(context, response.errorBody(), response.code())
                    Log.i("Laundry", " " + response.errorBody())
                }
            }
        })
    }

    fun laundryBasketList(context: Context, map: HashMap<String, Any>) {
        CommonFunctions.showProgress(context)
        RestClient.modalApiService.basketLaundriesList(
            com.ibox.washapp.utils.Prefs.with(context).getString(
                Constants.PREFS_TOKEN,
                ""
            ), map
        ).enqueue(object : Callback<MutableList<LaundryList>> {
            override fun onFailure(call: Call<MutableList<LaundryList>>, t: Throwable) {
                CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(context, t.message!!)
                Log.i("Laundry", " " + t.message)
            }

            override fun onResponse(
                call: Call<MutableList<LaundryList>>,
                response: Response<MutableList<LaundryList>>
            ) {
                CommonFunctions.dismissProgress()
                if (response.isSuccessful) {
                    Log.i("Laundry", " " + response.body())
                    laundryMutableLiveData.value = response.body()
                } else {
                    CommonFunctions.handleError(context, response.errorBody(), response.code())
                    Log.i("Laundry", " " + Gson().toJson(response.errorBody()))
                }
            }
        })
    }

    fun productListing(context: Context, map: Int) {
        CommonFunctions.showProgress(context)
        RestClient.modalApiService.getOrderCategories(map)
            .enqueue(object : Callback<List<com.ibox.washapp.model.PojoOrderCategory>> {
                override fun onFailure(call: Call<List<com.ibox.washapp.model.PojoOrderCategory>>, t: Throwable) {
                    CommonFunctions.dismissProgress()
                    CommonFunctions.handelInternet(context, t.message!!)
                    Log.i("Laundry", " " + t.message)
                }

                override fun onResponse(
                    call: Call<List<com.ibox.washapp.model.PojoOrderCategory>>,
                    response: Response<List<com.ibox.washapp.model.PojoOrderCategory>>
                ) {
                    CommonFunctions.dismissProgress()
                    if (response.isSuccessful) {
                        Log.i("Laundry", " " + response.body())
                        orderMutableLiveData.value = response.body()
                    } else {
                        CommonFunctions.handleError(context, response.errorBody(), response.code())
                        Log.i("Laundry", " " + response.errorBody())
                    }
                }
            })
    }

    fun productPaging(context: Context, map: HashMap<String, Int>) {
        RestClient.modalApiService.getPaging(
            com.ibox.washapp.utils.Prefs.with(context).getString(
                Constants.PREFS_TOKEN,
                ""
            ), map
        ).enqueue(object : Callback<List<com.ibox.washapp.model.Product>> {
            override fun onFailure(call: Call<List<com.ibox.washapp.model.Product>>, t: Throwable) {
                Log.i("PAGING", "ERROR  " + Gson().toJson(t.message))
            }

            override fun onResponse(
                call: Call<List<com.ibox.washapp.model.Product>>,
                response: Response<List<com.ibox.washapp.model.Product>>
            ) {
                if (response.isSuccessful) {
                    Log.i("PAGING", "SUC" + Gson().toJson(response.body()))
                   // orderMutableLiveData.value = response.body()
                    productPagingMutableLiveData.value = response.body()
                } else {
                    Log.i("PAGING", "FAIL" + Gson().toJson(response.body()))
                }
            }
        })
    }

    fun orderDetails(context: Context, map: Int) {
        CommonFunctions.showProgress(context)
        RestClient.modalApiService.getOrderDetails(
            com.ibox.washapp.utils.Prefs.with(context)
                .getString(Constants.PREFS_TOKEN, ""), map
        ).enqueue(object : Callback<com.ibox.washapp.model.PojoOrderDetails> {
            override fun onFailure(call: Call<com.ibox.washapp.model.PojoOrderDetails>, t: Throwable) {
                CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(context, t.message!!)
                Log.i("Laundry", " " + t.message)
            }

            override fun onResponse(
                call: Call<com.ibox.washapp.model.PojoOrderDetails>,
                response: Response<com.ibox.washapp.model.PojoOrderDetails>
            ) {
                CommonFunctions.dismissProgress()
                if (response.isSuccessful) {
                    Log.i("Laundry", " " + response.body())
                    orderDetailsMutableLiveData.value = response.body()
                    com.ibox.washapp.utils.Prefs.with(context).save(Constants.ORDER_ITEMS_COMP_ORDER, response.body())
                } else {
                    CommonFunctions.handleError(context, response.errorBody(), response.code())
                    Log.i("Laundry", " " + response.errorBody())
                }
            }
        })
    }

    fun createOrder(context: Context, map: HashMap<String, Any>) {
        Log.i("Mapmap"," $map")
        CommonFunctions.showProgress(context)
        RestClient.modalApiService.createOrder(
            com.ibox.washapp.utils.Prefs.with(context).getString(
                Constants.PREFS_TOKEN,
                ""
            ), map
        ).enqueue(object : Callback<OrderResponeLaundry> {
            override fun onFailure(call: Call<OrderResponeLaundry>, t: Throwable) {
                CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(context, t.message!!)
                Log.i("Laundry", " " + t.message)
            }

            override fun onResponse(
                call: Call<OrderResponeLaundry>,
                response: Response<OrderResponeLaundry>
            ) {
                CommonFunctions.dismissProgress()
                if (response.isSuccessful) {
                    Log.i("LaundryID", " " + response.body())
                    com.ibox.washapp.utils.Prefs.with(context).save(Constants.ORDER_ID, response.body()!!.id)
                    createOrderMutableLiveData.value = response.body()
                } else {
                    CommonFunctions.handleError(context, response.errorBody(), response.code())
                    Log.i("Laundry", " " + response.errorBody())
                }
            }
        })
    }


    fun createBasketOrder(context: Context, map: HashMap<String, Any>) {
        CommonFunctions.showProgress(context)
        RestClient.modalApiService.createBasketOrder(
            com.ibox.washapp.utils.Prefs.with(context).getString(
                Constants.PREFS_TOKEN,
                ""
            ), map
        ).enqueue(object : Callback<OrderResponeLaundry> {
            override fun onFailure(call: Call<OrderResponeLaundry>, t: Throwable) {
                CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(context, t.message!!)
                Log.i("Laundry", " " + t.message)
            }

            override fun onResponse(
                call: Call<OrderResponeLaundry>,
                response: Response<OrderResponeLaundry>
            ) {
                CommonFunctions.dismissProgress()
                if (response.isSuccessful) {
                    Log.i("Laundry", " " + response.body())

                    createOrderMutableLiveData.value = response.body()
                } else {
                    CommonFunctions.handleError(context, response.errorBody(), response.code())
                    Log.i("Laundry", " " + response.errorBody())
                }
            }
        })
    }


    fun applyCoupon(context: Context, map: HashMap<String, Any>) {
        CommonFunctions.showProgress(context)
        RestClient.modalApiService.applyCoupon(
            com.ibox.washapp.utils.Prefs.with(context).getString(
                Constants.PREFS_TOKEN,
                ""
            ), map
        ).enqueue(object : Callback<com.ibox.washapp.model.PojoSuccess> {
            override fun onFailure(call: Call<com.ibox.washapp.model.PojoSuccess>, t: Throwable) {
                CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(context, t.message!!)
                Log.i("Laundry", " " + t.message)
            }

            override fun onResponse(
                call: Call<com.ibox.washapp.model.PojoSuccess>,
                response: Response<com.ibox.washapp.model.PojoSuccess>
            ) {
                CommonFunctions.dismissProgress()
                if (response.isSuccessful) {
                    Log.i("Laundry", " " + response.body())
                    appLyCoupMutableLiveData.value = response.body()
                } else {
                    CommonFunctions.handleError(context, response.errorBody(), response.code())
                    Log.i("Laundry", " " + response.errorBody())
                }
            }
        })
    }


    fun payment(context: Context, map: HashMap<String, Any>) {
        CommonFunctions.showProgress(context)
        RestClient.modalApiService.payment(
            com.ibox.washapp.utils.Prefs.with(context).getString(Constants.PREFS_TOKEN, ""),
            map
        ).enqueue(object : Callback<com.ibox.washapp.model.PojoSuccess> {
            override fun onFailure(call: Call<com.ibox.washapp.model.PojoSuccess>, t: Throwable) {
                CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(context, t.message!!)
                Log.i("Laundry", " " + t.message)
            }

            override fun onResponse(
                call: Call<com.ibox.washapp.model.PojoSuccess>,
                response: Response<com.ibox.washapp.model.PojoSuccess>
            ) {
                CommonFunctions.dismissProgress()
                if (response.isSuccessful) {
                    Log.i("Laundry", " " + response.body())
                    paymentMutableLiveData.value = response.body()
                } else {
                    CommonFunctions.handleError(context, response.errorBody(), response.code())
                    Log.i("Laundry", " " + response.errorBody())
                }
            }
        })
    }

    fun editOrder(context: Context, map: HashMap<String, Any>) {
        CommonFunctions.showProgress(context)
        RestClient.modalApiService.editOrder(
            com.ibox.washapp.utils.Prefs.with(context).getString(
                Constants.PREFS_TOKEN,
                ""
            ), map
        ).enqueue(object : Callback<OrderResponeLaundry> {
            override fun onFailure(call: Call<OrderResponeLaundry>, t: Throwable) {
                CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(context, t.message!!)
                Log.i("LaundryEDIT", " " + t.message)
            }

            override fun onResponse(
                call: Call<OrderResponeLaundry>,
                response: Response<OrderResponeLaundry>
            ) {
                CommonFunctions.dismissProgress()
                if (response.isSuccessful) {
                    Log.i("LaundryEDIT", " " + Gson().toJson(response.body()))
                    createOrderMutableLiveData.value = response.body()
                } else {
                    CommonFunctions.handleError(context, response.errorBody(), response.code())
                    Log.i(
                        "LaundryEDIT",
                        "editOrderErrorBody " + Gson().toJson(response.errorBody())
                    )
                }
            }
        })
    }

    fun editBasketOrder(context: Context, map: HashMap<String, Any>) {
        CommonFunctions.showProgress(context)
        RestClient.modalApiService.editBasketOrder(
            com.ibox.washapp.utils.Prefs.with(context).getString(
                Constants.PREFS_TOKEN,
                ""
            ), map
        ).enqueue(object : Callback<OrderResponeLaundry> {
            override fun onFailure(call: Call<OrderResponeLaundry>, t: Throwable) {
                CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(context, t.message!!)
                Log.i("Laundry", " " + t.message)
            }

            override fun onResponse(
                call: Call<OrderResponeLaundry>,
                response: Response<OrderResponeLaundry>
            ) {
                CommonFunctions.dismissProgress()
                if (response.isSuccessful) {
                    Log.i("Laundry", " " + response.body())

                    createOrderMutableLiveData.value = response.body()
                } else {
                    CommonFunctions.handleError(context, response.errorBody(), response.code())
                    Log.i("Laundry", "errorBody " + Gson().toJson(response.errorBody()))
                }
            }
        })
    }

    fun cancelOrder(context: Context, map: Int) {
        CommonFunctions.showProgress(context)
        RestClient.modalApiService.cancelOrder(
            com.ibox.washapp.utils.Prefs.with(context).getString(
                Constants.PREFS_TOKEN,
                ""
            ), map
        ).enqueue(object : Callback<com.ibox.washapp.model.PojoSuccess> {
            override fun onFailure(call: Call<com.ibox.washapp.model.PojoSuccess>, t: Throwable) {
                CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(context, t.message!!)
                Log.i("Laundry", " " + t.message)
            }

            override fun onResponse(
                call: Call<com.ibox.washapp.model.PojoSuccess>,
                response: Response<com.ibox.washapp.model.PojoSuccess>
            ) {
                CommonFunctions.dismissProgress()
                if (response.isSuccessful) {
                    Log.i("Laundry", " " + response.body())
                    cancelOrderLiveData.value = response.body()
                } else {
                    CommonFunctions.handleError(context, response.errorBody(), response.code())
                    Log.i("Laundry", " " + response.errorBody())
                }
            }
        })
    }
}