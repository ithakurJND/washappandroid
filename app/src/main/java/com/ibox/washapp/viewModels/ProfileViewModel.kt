package com.ibox.washapp.viewModels

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.techautovity.myeve.retrofit.RestClient
import com.ibox.washapp.model.EditProfile
import com.ibox.washapp.utils.CommonFunctions
import com.ibox.washapp.utils.Constants
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProfileViewModel() : ViewModel() {
    internal lateinit var context: Context
    private var messgeMutableLiveData = MutableLiveData<EditProfile>()
    private var changePassMutableLiveData = MutableLiveData<EditProfile>()
    fun showMessage() = messgeMutableLiveData
    fun showChangePass() = changePassMutableLiveData

    fun getValues(context: Context) {
        this.context = context
    }

    fun putUser(
        name: String,
        phone: String
    ,context: Context) {

        val user = HashMap<String, String>()
        user.put("name", name)
        user.put("phone_no", phone)
        CommonFunctions.showProgress(context)
        RestClient.modalApiService.phoneNumber(com.ibox.washapp.utils.Prefs.with(context).getString(Constants.PREFS_TOKEN,""),user).enqueue(object : Callback<EditProfile> {
            override fun onFailure(call: Call<EditProfile>, t: Throwable) {
                CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(context,t.message!!)
            }

            override fun onResponse(call: Call<EditProfile>, response: Response<EditProfile>) {
                CommonFunctions.dismissProgress()
                if (response.isSuccessful) {
                    Log.i("PutUser", " " + response.body())
                    messgeMutableLiveData.value = response.body()
                } else {
                    CommonFunctions.handleError(context,response.errorBody(),response.code())
                    Log.i("PutUser", " " + response.errorBody())
                }
            }
        })
    }

    fun changePass(
        oldPass: String,
        newPass: String
    ,context: Context) {
        val pass = HashMap<String, String>()
        pass.put("old_password", oldPass)
        pass.put("new_password", newPass)
CommonFunctions.showProgress(context)
        RestClient.modalApiService.changePassword(com.ibox.washapp.utils.Prefs.with(context).getString(Constants.PREFS_TOKEN,""),pass).enqueue(object : Callback<EditProfile> {
            override fun onFailure(call: Call<EditProfile>, t: Throwable) {
        CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(context,t.message!!)
            }

            override fun onResponse(
                call: Call<EditProfile>, response: Response<EditProfile>
            ) {
                CommonFunctions.dismissProgress()
                if (response.isSuccessful) {
                    Log.i("Change", "Success" + response.body())
                    changePassMutableLiveData.value = response.body()
                } else {
                    CommonFunctions.handleError(context,response.errorBody(),response.code())
                }
            }
        })

    }
}