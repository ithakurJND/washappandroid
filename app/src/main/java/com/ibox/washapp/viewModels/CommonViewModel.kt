package com.ibox.washapp.viewModels

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.techautovity.myeve.retrofit.RestClient
import com.ibox.washapp.model.*
import com.ibox.washapp.utils.CommonFunctions
import com.ibox.washapp.utils.Constants
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CommonViewModel : ViewModel() {
    private val userMutableLiveData = MutableLiveData<UserLogin>()
    private var addToBasketMutableLiveData = MutableLiveData<List<com.ibox.washapp.model.PojoOrderCategory>>()
    private var locationMutableLiveData = MutableLiveData<NewLocations>()
    private var messageMutableLiveData = MutableLiveData<Message>()
    private var editProfileMutableLiveData = MutableLiveData<EditProfile>()
    private var getUserMutableLiveData = MutableLiveData<User>()
    private var bannerListData = MutableLiveData<HomeModel>()
    private var notofocationData = MutableLiveData<com.ibox.washapp.model.PojoNotification>()
    private var timeSlotsMutableLiveData = MutableLiveData<List<com.ibox.washapp.model.PojoTimeSlot>>()
    private var prefsMutableLiveData = MutableLiveData<List<com.ibox.washapp.model.PojoPreference>>()

    fun userResponse() = userMutableLiveData
    fun getBannerImages() = bannerListData
    fun getAddToBasket() = addToBasketMutableLiveData
    fun getLocations() = locationMutableLiveData
    fun getMessage() = messageMutableLiveData
    fun getEditProfile() = editProfileMutableLiveData
    fun getUserData() = getUserMutableLiveData
    fun getNotifications() = notofocationData
    fun getTimeSlots() = timeSlotsMutableLiveData
    fun getPrefernces() = prefsMutableLiveData


    fun notificationListing(context: Context) {
        CommonFunctions.showProgress(context)
        RestClient.modalApiService.getNotifications(
            com.ibox.washapp.utils.Prefs.with(context)
                .getString(Constants.PREFS_TOKEN, "")
        ).enqueue(object : Callback<com.ibox.washapp.model.PojoNotification> {
            override fun onFailure(call: Call<com.ibox.washapp.model.PojoNotification>, t: Throwable) {
                CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(context, t.message!!)
            }

            override fun onResponse(
                call: Call<com.ibox.washapp.model.PojoNotification>,
                response: Response<com.ibox.washapp.model.PojoNotification>
            ) {
                CommonFunctions.dismissProgress()
                if (response.isSuccessful) {
                    notofocationData.value = response.body()
                } else {
                    CommonFunctions.handleError(context, response.errorBody(), response.code())
                }
                //To change body of created functions use File | Settings | File Templates.
            }

        })
    }


    fun bannerImages(context: Context) {
        CommonFunctions.showProgress(context)
        RestClient.modalApiService.getBanner(
            com.ibox.washapp.utils.Prefs.with(context).getString(
                Constants.PREFS_TOKEN,
                ""
            )
        ).enqueue(object : Callback<HomeModel> {
            override fun onFailure(call: Call<HomeModel>, t: Throwable) {
                CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(context, t.message!!)
            }

            override fun onResponse(
                call: Call<HomeModel>,
                response: Response<HomeModel>
            ) {
                CommonFunctions.dismissProgress()
                if (response.isSuccessful) {
                    bannerListData.value = response.body()
                    Log.i("ResponseHome",""+response.body())
                } else {
                    CommonFunctions.handleError(context, response.errorBody(), response.code())
                }
                //To change body of created functions use File | Settings | File Templates.
            }

        })
    }


    fun userLogIn(
        fcm_id: String,
        device_type: Int,
        email: String,
        password: String,
        fb_id: String,
        google_id: String,
        name: String
        , context: Context
    ) {
        val user = UserLogin(null, null, null)
        CommonFunctions.showProgress(context)
        RestClient.modalApiService
            .UserLogin(LoginModel(device_type, email, fb_id, fcm_id, google_id, name, password))
            .enqueue(object : Callback<UserLogin> {
                override fun onFailure(call: Call<UserLogin>, t: Throwable) {
                    CommonFunctions.dismissProgress()
                    CommonFunctions.handelInternet(context, t.message!!)
                }

                override fun onResponse(call: Call<UserLogin>, response: Response<UserLogin>) {
                    CommonFunctions.dismissProgress()
                    if (response.isSuccessful) {
                        Log.i("ResponseLogin", "Success" + response.body())
                        userMutableLiveData.value = response.body()
                        com.ibox.washapp.utils.Prefs.with(context).save(Constants.USER_DATA, response.body()!!.user)
                        com.ibox.washapp.utils.Prefs.with(context).save(
                            Constants.PREFS_TOKEN,
                            response.body()!!.user_token!!.access_token
                        )

                    } else {
                        CommonFunctions.handleError(context, response.errorBody(), response.code())
                    }
                }
            })
    }

    fun userSignUp(
        device_type: Int,
        email: String,
        fcm_id: String,
        name: String,
        password: String
        , context: Context
    ) {
        val user = UserLogin(null, null, null)
        CommonFunctions.showProgress(context)
        RestClient.modalApiService.UserSignUp(
            SignUpModel(
                device_type,
                email,
                fcm_id,
                name,
                password
            )
        )
            .enqueue(object : Callback<UserLogin> {
                override fun onFailure(call: Call<UserLogin>, t: Throwable) {
                    CommonFunctions.dismissProgress()
                    CommonFunctions.handelInternet(context, t.message!!)
                }

                override fun onResponse(call: Call<UserLogin>, response: Response<UserLogin>) {
                    CommonFunctions.dismissProgress()
                    if (response.isSuccessful) {
                        Log.i("ResponseSignUp", "Success" + response.body())
                        userMutableLiveData.value = response.body()
                        com.ibox.washapp.utils.Prefs.with(context).save(Constants.USER_DATA, response.body()!!.user)
                        com.ibox.washapp.utils.Prefs.with(context).save(
                            Constants.PREFS_TOKEN,
                            response.body()!!.user_token!!.access_token
                        )

                    } else {
                        CommonFunctions.handleError(context, response.errorBody(), response.code())
                    }
                }
            })
    }

    fun forgotPass(emailPut: String, context: Context) {
        val email = HashMap<String, String>()
        email.put("email", emailPut)
        CommonFunctions.showProgress(context)
        RestClient.modalApiService.forgotPassword(email).enqueue(object : Callback<Message> {
            override fun onFailure(call: Call<Message>, t: Throwable) {
                CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(context, t.message!!)
            }

            override fun onResponse(
                call: Call<Message>,
                response: Response<Message>
            ) {
                CommonFunctions.dismissProgress()
                if (response.isSuccessful) {
                    Log.i("ResponseForgot", "Success" + response.body())
                    messageMutableLiveData.value = response.body()
                } else {
                    CommonFunctions.handleError(context, response.errorBody(), response.code())
                }
            }
        })
    }

    fun logout(fcm: String, context: Context) {
        val fcm_id = HashMap<String, String>()
        fcm_id.put("fcm_id", fcm)
        Log.i("FCM", "" + fcm)
        CommonFunctions.showProgress(context)
        RestClient.modalApiService.logout(
            com.ibox.washapp.utils.Prefs.with(context).getString(Constants.PREFS_TOKEN, ""),
            fcm_id
        ).enqueue(object : Callback<EditProfile> {
            override fun onFailure(call: Call<EditProfile>, t: Throwable) {
                CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(context, t.message!!)
            }

            override fun onResponse(call: Call<EditProfile>, response: Response<EditProfile>) {
                CommonFunctions.dismissProgress()
                if (response.isSuccessful) {
                    Log.i("ResponseLogout", "Success" + response.body())
                    editProfileMutableLiveData.value = response.body()
                } else {
                    CommonFunctions.handleError(context, response.errorBody(), response.code())
                    Log.i("ResponseLogout", "Success" + response.errorBody())
                }
            }
        })
    }

    fun phoneNumber(phoneNumber: String,code:String, context: Context) {
        val phone = HashMap<String, String>()
        phone.put("phone_no", phoneNumber)
        phone.put("country_code",code)
        CommonFunctions.showProgress(context)
        RestClient.modalApiService.phoneNumber(
            com.ibox.washapp.utils.Prefs.with(context).getString(
                Constants.PREFS_TOKEN,
                ""
            ), phone
        ).enqueue(object : Callback<EditProfile> {
            override fun onFailure(call: Call<EditProfile>, t: Throwable) {
                CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(context, t.message!!)
            }

            override fun onResponse(call: Call<EditProfile>, response: Response<EditProfile>) {
                CommonFunctions.dismissProgress()
                if (response.isSuccessful) {
                    Log.i("Response", "Success" + response.body())
                    editProfileMutableLiveData.value = response.body()
                } else {
                    CommonFunctions.handleError(context, response.errorBody(), response.code())
                    Log.i("Response", "Success" + response.errorBody())
                }
            }
        })
    }

    fun addToBasket(context: Context, isLoader: Boolean, page: Int) {
        if (isLoader)
            CommonFunctions.showProgress(context)
        val map = HashMap<String, Any>()
        map["page"] = page
        map["limit"] = 20
        RestClient.modalApiService.addToBasketPaging(
            com.ibox.washapp.utils.Prefs.with(context).getString(
                Constants.PREFS_TOKEN,
                ""
            )
            , map
        ).enqueue(object : Callback<List<com.ibox.washapp.model.PojoOrderCategory>> {
            override fun onFailure(call: Call<List<com.ibox.washapp.model.PojoOrderCategory>>, t: Throwable) {
                CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(context, t.message!!)
            }

            override fun onResponse(
                call: Call<List<com.ibox.washapp.model.PojoOrderCategory>>,
                response: Response<List<com.ibox.washapp.model.PojoOrderCategory>>
            ) {
                CommonFunctions.dismissProgress()
                if (response.isSuccessful) {
                    Log.i("ResponseAddToBasket", "Success" + response.body())
                    val dat = response.body()
                    dat!![0].page = page
                    addToBasketMutableLiveData.value = dat
                } else {
                    CommonFunctions.handleError(context, response.errorBody(), response.code())
                    Log.i("ResponseAddToBasket", "Failure" + response.errorBody())
                }
            }
        })
    }


    fun addToBasketPaging(context: Context, isLoader: Boolean) {
        if (isLoader)
            CommonFunctions.showProgress(context)
        RestClient.modalApiService.addToBasket(
            com.ibox.washapp.utils.Prefs.with(context).getString(
                Constants.PREFS_TOKEN,
                ""
            )
        ).enqueue(object : Callback<List<com.ibox.washapp.model.PojoOrderCategory>> {
            override fun onFailure(call: Call<List<com.ibox.washapp.model.PojoOrderCategory>>, t: Throwable) {
                CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(context, t.message!!)
            }

            override fun onResponse(
                call: Call<List<com.ibox.washapp.model.PojoOrderCategory>>,
                response: Response<List<com.ibox.washapp.model.PojoOrderCategory>>
            ) {
                CommonFunctions.dismissProgress()
                if (response.isSuccessful) {
                    Log.i("ResponseAddToBasket", "Success" + response.body())
                    addToBasketMutableLiveData.value = response.body()
                } else {
                    CommonFunctions.handleError(context, response.errorBody(), response.code())
                    Log.i("ResponseAddToBasket", "Failure" + response.errorBody())
                }
            }
        })
    }

    fun locations(context: Context,keyword:String,page: Int,limit:Int) {
      //  CommonFunctions.showProgress(context)
        val map = HashMap<String,Any>()
        map.put("keyword",keyword)
        map.put("page",page)
        map.put("limit",limit)
        RestClient.modalApiService.locations(map).enqueue(object : Callback<NewLocations> {
            override fun onFailure(call: Call<NewLocations>, t: Throwable) {
                CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(context, t.message!!)
            }

            override fun onResponse(
                call: Call<NewLocations>,
                response: Response<NewLocations>
            ) {
                CommonFunctions.dismissProgress()
                if (response.isSuccessful) {
                    Log.i("Locations", " " + response.body())
                    locationMutableLiveData.value = response.body()
                } else {
                    CommonFunctions.handleError(context, response.errorBody(), response.code())
                }
            }
        })
    }


    fun timeSlots(context: Context) {
        CommonFunctions.showProgress(context)
        RestClient.modalApiService.getTimeSlots().enqueue(object : Callback<List<com.ibox.washapp.model.PojoTimeSlot>> {
            override fun onFailure(call: Call<List<com.ibox.washapp.model.PojoTimeSlot>>, t: Throwable) {
                CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(context, t.message!!)
            }

            override fun onResponse(
                call: Call<List<com.ibox.washapp.model.PojoTimeSlot>>,
                response: Response<List<com.ibox.washapp.model.PojoTimeSlot>>
            ) {
                CommonFunctions.dismissProgress()
                if (response.isSuccessful) {
                    Log.i("Locations", " " + response.body())
                    timeSlotsMutableLiveData.value = response.body()
                } else {
                    CommonFunctions.handleError(context, response.errorBody(), response.code())
                }
            }
        })
    }


    fun preferences(context: Context) {
        CommonFunctions.showProgress(context)
        RestClient.modalApiService.getPreferences()
            .enqueue(object : Callback<List<com.ibox.washapp.model.PojoPreference>> {
                override fun onFailure(call: Call<List<com.ibox.washapp.model.PojoPreference>>, t: Throwable) {
                    CommonFunctions.dismissProgress()
                    CommonFunctions.handelInternet(context, t.message!!)
                }

                override fun onResponse(
                    call: Call<List<com.ibox.washapp.model.PojoPreference>>,
                    response: Response<List<com.ibox.washapp.model.PojoPreference>>
                ) {
                    CommonFunctions.dismissProgress()
                    if (response.isSuccessful) {
                        Log.i("Locations", " " + response.body())
                        prefsMutableLiveData.value = response.body()
                    } else {
                        CommonFunctions.handleError(context, response.errorBody(), response.code())
                    }
                }
            })
    }


    fun getUser(context: Context) {
       CommonFunctions.showProgress(context)
        RestClient.modalApiService.user(com.ibox.washapp.utils.Prefs.with(context).getString(Constants.PREFS_TOKEN, ""))
            .enqueue(object : Callback<User> {
                override fun onFailure(call: Call<User>, t: Throwable) {
                    CommonFunctions.dismissProgress()
                    CommonFunctions.handelInternet(context, t.message!!)
                }

                override fun onResponse(call: Call<User>, response: Response<User>) {
                    CommonFunctions.dismissProgress()
                    if (response.isSuccessful) {
                        Log.i("LocationsGetUser", " " + response.body())
                        getUserMutableLiveData.value = response.body()
                        com.ibox.washapp.utils.Prefs.with(context).save(Constants.USER_DATA, response.body()!!)
                    } else {
                        CommonFunctions.handleError(context, response.errorBody(), response.code())
                        Log.i("LocationsGetUser", "Failure " + response.body())
                    }
                }
            })
    }
}