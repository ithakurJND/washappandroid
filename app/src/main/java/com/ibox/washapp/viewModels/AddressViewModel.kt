package com.ibox.washapp.viewModels

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.techautovity.myeve.retrofit.RestClient
import com.ibox.washapp.model.AddAddress
import com.ibox.washapp.model.EditProfile
import com.ibox.washapp.utils.CommonFunctions
import com.ibox.washapp.utils.Constants
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddressViewModel : ViewModel() {
    var addressMutableLiveData = MutableLiveData<EditProfile>()
    fun addressResponse() = addressMutableLiveData
    fun addAddress(
        address: AddAddress
    ,context: Context) {
        CommonFunctions.showProgress(context)
        RestClient.modalApiService.addAddress(
           com.ibox.washapp.utils.Prefs.with(context).getString(Constants.PREFS_TOKEN,""),address
        ).enqueue(object :Callback<EditProfile>{
            override fun onFailure(call: Call<EditProfile>, t: Throwable) {
                CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(context,t.message!!)
            }

            override fun onResponse(call: Call<EditProfile>, response: Response<EditProfile>) {
                CommonFunctions.dismissProgress()
                if (response.isSuccessful){
                addressMutableLiveData.value = response.body()
                }else{
                    CommonFunctions.handleError(context,response.errorBody(),response.code())
                }
            }
        })
    }

    fun editAddress(
        address: AddAddress
        ,context: Context) {
        CommonFunctions.showProgress(context)
        RestClient.modalApiService.editAddress(
            com.ibox.washapp.utils.Prefs.with(context).getString(Constants.PREFS_TOKEN,""),address
        ).enqueue(object :Callback<EditProfile>{
            override fun onFailure(call: Call<EditProfile>, t: Throwable) {
                CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(context,t.message!!)
            }

            override fun onResponse(call: Call<EditProfile>, response: Response<EditProfile>) {
                CommonFunctions.dismissProgress()
                if (response.isSuccessful){
                    addressMutableLiveData.value = response.body()
                }else{
                    CommonFunctions.handleError(context,response.errorBody(),response.code())
                }
            }
        })
    }
}