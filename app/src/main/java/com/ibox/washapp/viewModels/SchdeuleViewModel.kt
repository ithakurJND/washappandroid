package com.ibox.washapp.viewModels

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.techautovity.myeve.retrofit.RestClient
import com.ibox.washapp.utils.CommonFunctions
import com.ibox.washapp.utils.Constants
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SchdeuleViewModel : ViewModel() {
    var schdeuleMutableLiveData = MutableLiveData<List<com.ibox.washapp.model.PojoSchdeule>>()
    var postMutableLiveData = MutableLiveData<com.ibox.washapp.model.PojoSuccess>()
    var orderListMutableLiveData = MutableLiveData<List<com.ibox.washapp.model.PojoOrderListing>>()
    fun addAddress(
        context: Context
    ) {
        CommonFunctions.showProgress(context)
        RestClient.modalApiService.getSchedules(
            com.ibox.washapp.utils.Prefs.with(context).getString(Constants.PREFS_TOKEN, "")
        ).enqueue(object : Callback<List<com.ibox.washapp.model.PojoSchdeule>> {
            override fun onFailure(call: Call<List<com.ibox.washapp.model.PojoSchdeule>>, t: Throwable) {
                CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(context, t.message!!)
            }

            override fun onResponse(
                call: Call<List<com.ibox.washapp.model.PojoSchdeule>>,
                response: Response<List<com.ibox.washapp.model.PojoSchdeule>>
            ) {
                CommonFunctions.dismissProgress()
                if (response.isSuccessful) {
                    schdeuleMutableLiveData.value = response.body()
                } else {
                    CommonFunctions.handleError(context, response.errorBody(), response.code())
                }
            }
        })
    }

    fun postSchedule(
        context: Context
        , map: HashMap<String, Any>
    ) {
        CommonFunctions.showProgress(context)
        RestClient.modalApiService.postSchedules(
            com.ibox.washapp.utils.Prefs.with(context).getString(Constants.PREFS_TOKEN, "")
            , map
        ).enqueue(object : Callback<com.ibox.washapp.model.PojoSuccess> {
            override fun onFailure(call: Call<com.ibox.washapp.model.PojoSuccess>, t: Throwable) {
                CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(context, t.message!!)
            }

            override fun onResponse(call: Call<com.ibox.washapp.model.PojoSuccess>, response: Response<com.ibox.washapp.model.PojoSuccess>) {
                CommonFunctions.dismissProgress()
                if (response.isSuccessful) {
                    postMutableLiveData.value = response.body()
                } else {
                    CommonFunctions.handleError(context, response.errorBody(), response.code())
                }
            }
        })
    }

    fun getOrders(
        context: Context
        , typr: Int, isLoader: Boolean
    ) {
        if (isLoader)
            CommonFunctions.showProgress(context)
        val map = HashMap<String,Any>()
        map["type"] = typr
        map["limit"] = 1000
        map["page"] = 0
        RestClient.modalApiService.getOrderListing(
            com.ibox.washapp.utils.Prefs.with(context).getString(Constants.PREFS_TOKEN, "")
            , map
        ).enqueue(object : Callback<List<com.ibox.washapp.model.PojoOrderListing>> {
            override fun onFailure(call: Call<List<com.ibox.washapp.model.PojoOrderListing>>, t: Throwable) {
                CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(context, t.message!!)
            }

            override fun onResponse(
                call: Call<List<com.ibox.washapp.model.PojoOrderListing>>,
                response: Response<List<com.ibox.washapp.model.PojoOrderListing>>
            ) {
                CommonFunctions.dismissProgress()
                if (response.isSuccessful) {
                    orderListMutableLiveData.value = response.body()
                } else {
                    CommonFunctions.handleError(context, response.errorBody(), response.code())
                }
            }
        })
    }
}