package com.ibox.washapp.utils

import com.ibox.washapp.R

class Constants {
    companion object {
        const val AREA_ID: String = "area-id"
        const val EMIRAT_ID: String = "emirat-id"
        const val HOME_DATA: String = "PREFSHOmeDATA"
        const val ORDER_DATA: String = "OrderDataWashNew"
        const val ORDER_DATABASKET: String = "OrderDataWashNewBASKET"
        const val USER_DATA: String = "Prefs_user_data"
        const val PREFS_TOKEN = "user_token"
        const val SERVICE_TYPE = "serviceType"
        const val NEW_RESPONSE_USER_DATA: String = "New_User_Order_Response"
        const val LAUNDRY_ID: String = "Laundry_id"
        const val ORDER_ID: String = "Order_id"

        const val APP_NAME = R.string.app_name
        // const val BASE_URL = "http://163.47.10.222:8000/api/image?filename="
        const val BASE_URL = "https://app.washapp.ae:8001/api/image?filename="
        const val IMAGE_FOLDER = "&folder=medium"
        const val PICK_TIME = "Pick_Time"
        const val PICK_ADDRESS = "Pick_Address"
        const val DROP_TIME = "Drop_Time"
        const val DROP_ADDRESS = "Drop_Address"
        const val ORDER_ITEMS_COMP_ORDER = "Complete_Order"
        const val PICKADD = "PICKADD"
        const val PICKADDID = "PICKADDID"
        const val DROPADD = "DROPADD"
        const val DROPADDID = "DROPADDID"
        const val LAT2 = "lat2"
        const val LNG2 = "lng2"
        const val TEXT = "text"

        const val PARAMETER_SEP = "&"
        const val PARAMETER_EQUALS = "="
        const val TRANS_URL = "https://secure.ccavenue.ae/transaction/initTrans"
        const val COMMAND = "command"
        const val ACCESS_CODE = "access_code"
        const val MERCHANT_ID = "merchant_id"
        const val ORDER_ID_FOR = "order_id"
        const val AMOUNT = "amount"
        const val CURRENCY = "currency"
        const val ENC_VAL = "enc_val"
        const val REDIRECT_URL = "redirect_url"
        const val CANCEL_URL = "cancel_url"
        const val RSA_KEY_URL = "rsa_key_url"
        const val BILLING_NAME = "billing_name"
        const val BILLING_ADDRESS = "billing_address"
        const val BILLING_CITY = "billing_city"
        const val BILLING_STATE = "billing_state"
        const val BILLING_COUNTRY = "billing_country"
        const val BILLING_EMAIL= "billing_email"
        const val BILLING_TEL= "billing_tel"
        const val BILLING_ZIP= "billing_zip"
    }
}