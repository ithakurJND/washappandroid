package com.ibox.washapp.utils.facebook;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;

import androidx.fragment.app.Fragment;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class FacebookLogin {

    private Context mContext;
    private FacebookLoginListener facebookLoginListener;
    private CallbackManager mCallbackManager;
    private Object mUiRef;

    public FacebookLogin(Context context, Object uiRef) {
        mContext = context;
        mUiRef = uiRef;
        printHashKey();
      //  FacebookSdk.sdkInitialize(context.getApplicationContext());
        mCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                facebookLoginListener.onFbLoginSuccess();
            }

            @Override
            public void onCancel() {

                facebookLoginListener.onFbLoginCancel();
            }

            @Override
            public void onError(FacebookException exception) {
                facebookLoginListener.onFbLoginError();
            }
        });
    }

    public void performLogin() {
        if(mUiRef instanceof Activity){
            LoginManager.getInstance().logInWithReadPermissions((Activity)mUiRef,
                    Arrays.asList("email", "public_profile"));
        }else if(mUiRef instanceof Fragment){
            LoginManager.getInstance().logInWithReadPermissions((Fragment)mUiRef,
                    Arrays.asList("email","public_profile"));
        }

    }

    public void getUserProfile() {
        GraphRequest request = GraphRequest.newMeRequest(
                AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
                        facebookLoginListener.onGetprofileSuccess(object, response);
                        LoginManager.getInstance().logOut();
                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields","id,first_name,last_name,email,gender,picture.height(300).width(300)");
        request.setParameters(parameters);
        request.executeAsync();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void setFacebookLoginListener(FacebookLoginListener facebookLoginListener){
        this.facebookLoginListener = facebookLoginListener;
    }

    private void printHashKey() {
        try {
            String packageName = mContext.getApplicationContext().getPackageName();
            PackageInfo info = mContext.getPackageManager().getPackageInfo(
                    packageName,
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("FacebookLogin","Package Name not found");
        } catch (NoSuchAlgorithmException e) {
            Log.e("FacebookLogin","No such Algorithm");
        }
    }
}

