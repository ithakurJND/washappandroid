package com.ibox.washapp.utils

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.os.Environment
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import com.ibox.washapp.R
import com.ibox.washapp.activities.SplashActivity
import com.ibox.washapp.model.OrderData
import okhttp3.ResponseBody
import org.json.JSONObject
import java.io.File
import java.io.IOException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

object CommonFunctions {
    private var dialog: Dialog? = null

    fun showSnackBar(message: String, view: View) {
        val snack = Snackbar
            .make(view, message, Snackbar.LENGTH_LONG)
        snack.show()
    }

    fun getScreenWidth(activity: Context): Int {
        val w = activity.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val size = Point()
        w.defaultDisplay.getSize(size)
        return size.x

    }

    fun formatDate(original: String, target: String, date: String): String {

        val originalFormat = SimpleDateFormat(original)
        val targetFormat = SimpleDateFormat(target)

        var newDate: Date? = null
        try {
            newDate = originalFormat.parse(date)
        } catch (e: ParseException) {
            newDate = Date()
            e.printStackTrace()
        }

        return targetFormat.format(newDate)
    }


    fun addProduct(context: Context, product: com.ibox.washapp.model.Product) {
        var data = com.ibox.washapp.utils.Prefs.with(context).getObject(Constants.ORDER_DATA, OrderData::class.java)
        if (data == null) {
            data = OrderData()
            val productMap = HashMap<Int, com.ibox.washapp.model.Product>()
            data.product = productMap
        }
        val productMap = data.product
        if (product.quantity > 0) {
            productMap?.set(product.id, product)
        } else {
            productMap!!.remove(product.id)
        }
        com.ibox.washapp.utils.Prefs.with(context).save(Constants.ORDER_DATA, data)
    }

    fun addProductCount(context: Context): Int? {
        var data = com.ibox.washapp.utils.Prefs.with(context).getObject(Constants.ORDER_DATA, OrderData::class.java)
        return data.product!!.size
    }

    fun addProductBasket(context: Context, product: com.ibox.washapp.model.Product) {
        var data = com.ibox.washapp.utils.Prefs.with(context).getObject(Constants.ORDER_DATABASKET, OrderData::class.java)
        if (data == null) {
            data = OrderData()
            val productMap = HashMap<Int, com.ibox.washapp.model.Product>()
            data.product = productMap
        }
        val productMap = data.product
        if (product.quantity > 0) {
            productMap?.set(product.id, product)
        } else {
            productMap!!.remove(product.id)
        }
        com.ibox.washapp.utils.Prefs.with(context).save(Constants.ORDER_DATABASKET, data)
    }

    fun addProductBasketCount(context: Context): Int {
        var data = com.ibox.washapp.utils.Prefs.with(context).getObject(Constants.ORDER_DATABASKET, OrderData::class.java)
        return data.product!!.size
    }

    fun getBasketTotal(context: Context, type: Int): Int {
        val data = com.ibox.washapp.utils.Prefs.with(context).getObject(Constants.ORDER_DATABASKET, OrderData::class.java)
        val produsctsa = data.product!!.values
        var total = 0
        for (pr in produsctsa) {
            if (type == 1) {
                total += pr.regular_price * pr.quantity
            } else {
                total += pr.urgent_price * pr.quantity
            }
        }
        return total
    }

    fun getBasketQuant(context: Context): Int {
        val data = com.ibox.washapp.utils.Prefs.with(context).getObject(Constants.ORDER_DATABASKET, OrderData::class.java)
        val produsctsa = data.product!!.values
        var total = 0
        for (pr in produsctsa) {
            total += pr.quantity

        }
        return total
    }

    fun dpToPx(context: Context, dip: Float): Float {
        val r = context.resources
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            dip,
            r.displayMetrics
        )
    }

    fun getScreenHeight(activity: Context): Int {
        val w = activity.getSystemService(Context.WINDOW_SERVICE) as WindowManager

        val size = Point()
        w.defaultDisplay.getSize(size)
        return size.y

    }

    fun showProgress(activity: Context) {
        if (dialog != null) {
            if (dialog!!.isShowing) {
                return
            }
        }
        try {
            dialog = Dialog(activity)
            dialog!!.window!!.requestFeature(Window.FEATURE_NO_TITLE)
            dialog!!.setContentView(R.layout.layout_progress)
            dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog!!.setCanceledOnTouchOutside(false)
            dialog!!.setCancelable(false)

            dialog!!.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun dismissProgress() {
        try {
            if (dialog != null) {
                if (dialog!!.isShowing) {
                    dialog!!.dismiss()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun sessionExpired(context: Context) {
        Toast.makeText(
            context,
            context.getString(R.string.device_lo_in_from_other_device),
            Toast.LENGTH_SHORT
        ).show()
        // context?.let { showError(it, "Sorry your account had been logged in from other device.") }
        try {
            com.ibox.washapp.utils.Prefs.with(context).removeAll(context)
        } catch (e: java.lang.Exception) {
            com.ibox.washapp.utils.Prefs.with(context).removeAll(context)
        }
        (context as Activity).finishAffinity()
        context.startActivity(Intent(context, SplashActivity::class.java))
    }

    private val JPEG_FILE_PREFIX = "IMG_"
    private val JPEG_FILE_SUFFIX = ".jpg"

    @Throws(IOException::class)
    fun setUpImageFile(directory: String): File? {
        var imageFile: File? = null
        if (Environment.MEDIA_MOUNTED == Environment
                .getExternalStorageState()
        ) {
            val storageDir = File(directory)
            if (!storageDir.mkdirs()) {
                if (!storageDir.exists()) {
                    Log.d("CameraSample", "failed to create directory")
                    return null
                }
            }

            imageFile = File.createTempFile(
                JPEG_FILE_PREFIX
                        + System.currentTimeMillis() + "_",
                JPEG_FILE_SUFFIX, storageDir
            )
        }
        return imageFile
    }

    fun handelInternet(activity: Context, t: String) {
        if (t == "Unable to resolve host \"www.myeve.com.au\": No address associated with hostname") {
            Toast.makeText(activity, "Internet connection not available", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(activity, t, Toast.LENGTH_SHORT).show()
        }
    }

    fun handleError(callback: Context, errorBody: ResponseBody?, code: Int) {
        if (code == 403) {
            sessionExpired(callback)
        } else if (code == 500) {
            Toast.makeText(
                callback, "Internal Server Error",
                Toast.LENGTH_SHORT
            ).show()
        }
//         else if (JSONObject(errorBody?.string()).getString("message") == "Amount is already paid for this order.") {
//            val builder = AlertDialog.Builder(callback)
//            builder.setTitle("Create new order")
//            builder.setMessage("Amount is already paid for this order.")
//            builder.setPositiveButton("YES") { dialog, which ->
//                val intent = Intent(callback, MainActivity::class.java)
//                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
//                callback.startActivity(intent)
//            }
//
//            builder.setNegativeButton("No") { dialog, which ->
//                dialog.dismiss()
//            }
//            val dialog: AlertDialog = builder.create()
//            dialog.setOnShowListener {
//                dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
//                    .setTextColor(callback.resources.getColor(R.color.drakBlue))
//                dialog.getButton(AlertDialog.BUTTON_POSITIVE)
//                    .setTextColor(callback.resources.getColor(R.color.drakBlue))
//                dialog.getButton(AlertDialog.BUTTON_NEUTRAL)
//                    .setTextColor(callback.resources.getColor(R.color.drakBlue))
//            }
//            dialog.show()
//        }
       else {
            Toast.makeText(
                callback, JSONObject(errorBody!!.string()).getString("message"),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    fun getDays(context: Context): Int {
        var day: Int
        val homeData = com.ibox.washapp.utils.Prefs.with(context).getObject(Constants.HOME_DATA, com.ibox.washapp.model.PojoHome::class.java)
            .days
        day = if (com.ibox.washapp.utils.Prefs.with(context).getInt(
                "serviceType",
                1
            ) == 1
        ) homeData[0].day else homeData[1].day

        return day
    }

    fun getCurrentDate(): Int? {
        var ss = ""
        val calendar = Calendar.getInstance().time
        val sdf = SimpleDateFormat("HH", Locale.getDefault())
        val currentTime = sdf.format(calendar)
        val nextHour = currentTime.toInt() + 1
        ss =
            "" + (if (currentTime.toInt() > 12) (currentTime.toInt() - 12).toString() + " PM"
            else
                currentTime + " AM") + " - " + (if (nextHour.toInt() > 12) (nextHour.toInt() - 12).toString() + " PM"
            else
                nextHour.toString() + " AM")
        return nextHour
    }

    fun millseconds(original: String, date: String): Long {

        val originalFormat = SimpleDateFormat(original)
        originalFormat.timeZone = TimeZone.getTimeZone("UTC")

        var newDate: Date? = null
        try {
            newDate = originalFormat.parse(date)
        } catch (e: ParseException) {
            newDate = Date()
            e.printStackTrace()
        }
        return newDate!!.time
    }

}