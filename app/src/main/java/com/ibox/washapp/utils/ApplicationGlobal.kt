package com.ibox.washapp.utils

import android.app.Application
import com.crashlytics.android.Crashlytics
import com.facebook.drawee.backends.pipeline.Fresco
import io.fabric.sdk.android.Fabric


class ApplicationGlobal : Application() {

    override fun onCreate() {
        super.onCreate()
        Fresco.initialize(this)
        Fabric.with(this, Crashlytics())
    }
}