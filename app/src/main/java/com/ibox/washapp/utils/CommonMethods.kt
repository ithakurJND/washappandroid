package com.ibox.washapp.utils

import android.view.animation.CycleInterpolator
import android.view.animation.TranslateAnimation
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.ibox.washapp.R
import java.util.*

class CommonMethods {
    companion object {

        fun showDialogFragmentWithoutAnimation(
            activity: AppCompatActivity, dialogFragment: DialogFragment
        ) {
            dialogFragment.setStyle(
                DialogFragment.STYLE_NO_TITLE,
                R.style.ThemeOverlay_AppCompat_Dialog
            )
            dialogFragment.show(activity.supportFragmentManager, null)
        }
        fun addFragment(fragmentManager: FragmentManager, fragment: Fragment) {
            /* Defining fragment transaction */
            with(fragmentManager.beginTransaction()) {
                add(R.id.splashFrame, fragment)
//                addToBackStack("")
                commit()
            }

        }
        fun addFragmentwithBackStack(fragmentManager: FragmentManager, fragment: Fragment) {
            /* Defining fragment transaction */
            with(fragmentManager.beginTransaction()) {
                add(R.id.splashFrame, fragment)
                addToBackStack("")
                commit()
            }

        }
        fun replaceFragment(fragmentManager: FragmentManager, fragment: Fragment) {
            /* Defining fragment transaction */
            with(fragmentManager.beginTransaction()) {
                replace(R.id.splashFrame, fragment)
                commit()
            }
        }

        fun replaceFragmentOnSplashWithBackStack(
            fragmentManager: FragmentManager,
            fragment: Fragment
        ) {
            /* Defining fragment transaction */
            with(fragmentManager.beginTransaction()) {
                replace(R.id.splashFrame, fragment)
                addToBackStack("")
                commit()
            }
        }

        fun replaceFragmentWithBackStack(fragmentManager: FragmentManager, fragment: Fragment) {
            /* Defining fragment transaction */
            with(fragmentManager.beginTransaction()) {
                replace(R.id.frameLayoutCommon, fragment)
                commit()
                addToBackStack("")
            }
        }

        fun getNext30Dates(day: Int): List<Date> {
            val calendar = Calendar.getInstance()
            if (day != 0) {
                calendar.apply {
                    add(Calendar.DATE, day)
                }
            }
            val date1 = calendar.time
            calendar.apply {
                add(Calendar.DATE, 30)
            }
            val date2 = calendar.time
            val dates = ArrayList<Date>()
            val cal1 = Calendar.getInstance()
            cal1.time = date1
            val cal2 = Calendar.getInstance()
            cal2.time = date2
            while (!cal1.after(cal2)) {
                dates.add(cal1.time)
                cal1.add(Calendar.DATE, 1)
            }
            return dates
        }


        fun shakeError(): TranslateAnimation {
            val shake = TranslateAnimation(0f, 10f, 0f, 0f)
            shake.duration = 500
            shake.interpolator = CycleInterpolator(7f)!!
            return shake
        }

        fun addToPostParams(paramKey: String, paramValue: String?): String {
            return if (paramValue != null) paramKey + Constants.PARAMETER_EQUALS + paramValue + Constants.PARAMETER_SEP else ""
        }

        fun randInt(min: Int, max: Int): Int { // Usually this should be a field rather than a method variable so
            // that it is not re-seeded every call.
            val rand = Random()
            // nextInt is normally exclusive of the top value,
            // so add 1 to make it inclusive
            return rand.nextInt(max - min + 1) + min
        }
        fun chkNull(pData: Any?): Any {
            return pData ?: ""
        }
    }
}