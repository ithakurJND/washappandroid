package com.ibox.washapp.utils

import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import com.google.gson.GsonBuilder
import com.ibox.washapp.retrofit.API
import com.ibox.washapp.retrofit.Config
import com.techautovity.myeve.retrofit.RestClient
import okhttp3.OkHttpClient
import org.apache.http.HttpEntity
import org.apache.http.HttpResponse
import org.apache.http.NameValuePair
import org.apache.http.client.ClientProtocolException
import org.apache.http.client.entity.UrlEncodedFormEntity
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPost
import org.apache.http.client.utils.URLEncodedUtils
import org.apache.http.impl.client.DefaultHttpClient
import org.apache.http.params.HttpProtocolParams
import org.apache.http.util.EntityUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.io.UnsupportedEncodingException
import java.math.BigInteger
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder
import java.security.MessageDigest

class ServiceHandler {
    /**
     * Making service call
     * @url - url to make request
     * @method - http request method
     * @params - http request params
     */
    /**
     * Making service call
     * @url - url to make request
     * @method - http request method
     */

    var apiServiceModel: API? = null

    @RequiresApi(Build.VERSION_CODES.N)
    @JvmOverloads
    fun makeServiceCall(
        url: String?, method: Int,
        params: List<NameValuePair?>? = null
    ): String? {
       // Log.i("URL", "url $url")
        Log.i("URL", "params  $params")
        var url = "https://app.washapp.ae:8001/api/image/file?folder=pdfs&filename=GetRSA.jsp"

        Log.i("URL", "url $url")


        /**
         * By using this method your response will be coming in  modal class object
         *
         * @return object of ApiService interface
         */
        // .client(fetchHeaders(context))
        val modalApiService: API

        val gson = GsonBuilder()
            .setLenient()
            .create()
        if (apiServiceModel == null) {

            val okHttpClient = OkHttpClient()
            val retrofit = Retrofit.Builder()
                .baseUrl("https://test.ccavenue.com/transaction/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(RestClient.getOkHttpClient())
                // okHttpClient.newBuilder().connectTimeout(60, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS).writeTimeout(60, TimeUnit.SECONDS).build()
                .build()

            apiServiceModel = retrofit.create(API::class.java)
        }
      //  /getRSAKey?access_code=AVAE03HF44AB13EABAIAC&order_id=7395432
        var map = HashMap<String, Any>()
        map["access_code"] = "AVAE03HF44AB13EABAIAC"
        map["order_id"] = "12334235"
        apiServiceModel!!.getRSA(map).enqueue(object : Callback<String> {
            override fun onFailure(call: Call<String>, t: Throwable) {

            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                if (response.isSuccessful) {
                    Log.i("ResponeHandler", "succ  ${response.body()}")
                } else {
                    Log.i("ResponeHandler", "fail  ${response.errorBody()}")
                }
            }
        })
//         RestClient.modalApiService.getRsaKey(map).enqueue(object :Callback<String>{
//             override fun onFailure(call: Call<String>, t: Throwable) {
//             }
//
//             override fun onResponse(call: Call<String>, response: Response<String>) {
//                 if (response.isSuccessful) {
//                    Log.i("ResponeHandler", "succ  ${response.body()}")
//                } else {
//                    Log.i("ResponeHandler", "fail  ${response.errorBody()}")
//                }
//             }
//         })
//
//        try { // http client
//            val httpClient = DefaultHttpClient()
//            var httpEntity: HttpEntity? = null
//            var httpResponse: HttpResponse? = null
//            //Setting user agent
//            httpClient.getParams().setParameter(
//                HttpProtocolParams.USER_AGENT,
//                "Mozilla/5.0 (Linux; U; Android-4.0.3; en-us; Galaxy Nexus Build/IML74K) AppleWebKit/535.7 (KHTML, like Gecko) CrMo/16.0.912.75 Mobile Safari/535.7"
//            )
//            // Checking http request method type
//            if (method == POST) {
//                val httpPost = HttpPost(url)
//                // adding post params
//                if (params != null) {
//                    httpPost.setEntity(UrlEncodedFormEntity(params))
//                }
//                httpResponse = httpClient.execute(httpPost)
//            } else if (method == GET) { // appending params to url
//                if (params != null) {
//                    val paramString: String = URLEncodedUtils.format(params, "utf-8")
//                    url += "?$paramString"
//                }
//                val httpGet = HttpGet(url)
//                httpResponse = httpClient.execute(httpGet)
//            }
//            httpEntity = httpResponse?.getEntity()
//            response = EntityUtils.toString(httpEntity)
//        } catch (e: UnsupportedEncodingException) {
//            e.printStackTrace()
//        } catch (e: ClientProtocolException) {
//            e.printStackTrace()
//        } catch (e: IOException) {
//            e.printStackTrace()
//        }

//    val url2 = URL(url)
//        val urlConnection =  url2.openConnection() as HttpURLConnection
//        try {
//            urlConnection.doOutput = true;
//            urlConnection.setChunkedStreamingMode(0)
//          //  val typeIn = BufferedOutputStream(urlConnection.outputStream)
//
//            val show = BufferedInputStream(urlConnection.inputStream)
//
//        }
//        finally {
//            urlConnection.disconnect()
//        }
        Log.i("URL", "url response $response")
        response = ""
        return response


    }

    companion object {
        var response: String? = null
        const val GET = 1
        const val POST = 2
    }

//    override fun createSocket(params: HttpParams?): Socket {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//
//    override fun connectSocket(
//        sock: Socket?,
//        remoteAddress: InetSocketAddress?,
//        localAddress: InetSocketAddress?,
//        params: HttpParams?
//    ): Socket {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//
//    override fun isSecure(sock: Socket?): Boolean {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }

    //    override fun usingProxy(): Boolean {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//
//    override fun connect() {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//
//    override fun disconnect() {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
    @RequiresApi(Build.VERSION_CODES.N)
    fun sendGet(url: String?, params: List<NameValuePair?>?) {
        //Log.i("RSA"," $url")


        val vEncVal = StringBuffer("")
        vEncVal.append(
            CommonMethods.addToPostParams(
                Constants.MERCHANT_ID,
                "9487848753"
            )
        )
        vEncVal.append(
            CommonMethods.addToPostParams(
                Constants.ACCESS_CODE,
                "AVII03HC39CA35IIAC"
            )
        )
        val encVal = RSAUtility.encrypt(
            vEncVal.substring(
                0,
                vEncVal.length - 1
            ), "2653A58BD2239DCC19DA0F6DE16D64E4"
        )
        Log.i("StringAppend", "$vEncVal")
        Log.i("StringAppend", "encypt $encVal")
        val enc = "2653A58BD2239DCC19DA0F6DE16D64E4".md5()
        Log.i("URL", "md5  $enc")
        val url2 = URL("https://secure.ccavenue.ae/transaction")
        val builder: Uri.Builder = Uri.Builder()
            .appendQueryParameter(Constants.ORDER_ID, "9487848753")
            .appendQueryParameter(Constants.ACCESS_CODE, "AVII03HC39CA35IIAC")

        val query: String? = builder.build().encodedQuery
        with(url2.openConnection() as HttpURLConnection) {
            doInput = true
            doOutput = true
            requestMethod = "POST"  // optional default is GET
//            val os: OutputStream = it.getOutputStream()
//            val writer = BufferedWriter(
//                OutputStreamWriter(outputStream, "UTF-8")
//            )
//            writer.write(getQuery((params as List<NameValuePair>?)!!))
//
//            writer.flush()
//            writer.close()
//            outputStream.close()
            outputStream.bufferedWriter().use {
                it.write(query)
            }
            Log.i(
                "HHTT",
                "jhfds   ${"\nSent 'GET' request to URL : $url2; Response Code : $responseCode"}"
            )
        }

    }

    @Throws(UnsupportedEncodingException::class)
    private fun getQuery(params: List<NameValuePair>): String? {
        val result = StringBuilder()
        var first = true
        for (pair in params) {
            if (first) first = false else result.append("&")
            result.append(URLEncoder.encode(pair.name, "UTF-8"))
            result.append("=")
            result.append(URLEncoder.encode(pair.value, "UTF-8"))
        }
        return result.toString()
    }

    fun String.md5(): String {
        val md = MessageDigest.getInstance("MD5")
        return BigInteger(1, md.digest(toByteArray())).toString(16).padStart(32, '0')
    }
}