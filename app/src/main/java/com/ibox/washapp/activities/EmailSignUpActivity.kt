package com.ibox.washapp.activities

import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.iid.FirebaseInstanceId
import com.ibox.washapp.R
import com.ibox.washapp.model.UserLogin
import com.ibox.washapp.utils.CommonMethods
import com.ibox.washapp.viewModels.CommonViewModel
import kotlinx.android.synthetic.main.framgent_email_signup.*

class EmailSignUpActivity : AppCompatActivity(), View.OnClickListener {
    var commonViewModel = CommonViewModel()
    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ivBackArrow -> {
                onBackPressed()
            }
            R.id.btnLoginWithEmail -> {
                if (etName.text.isEmpty()) {
                    etName.startAnimation(CommonMethods.shakeError())
                    etName.error = "Required"
                    etName.requestFocus()
                } else if (etEmailAddress.text.isEmpty()) {
                    etEmailAddress.startAnimation(CommonMethods.shakeError())
                    etEmailAddress.error = "Required"
                    etEmailAddress.requestFocus()
                } else if (!Patterns.EMAIL_ADDRESS.matcher(etEmailAddress.text).matches()) {
                    etEmailAddress.startAnimation(CommonMethods.shakeError())
                    etEmailAddress.error = "Invalid Email"
                    etEmailAddress.requestFocus()
                } else if (etCreatePass.text.isEmpty()) {
                        etCreatePass.startAnimation(CommonMethods.shakeError())
                        etCreatePass.error = "Spaces not allowed"
                        etCreatePass.requestFocus()
                } else if (!etCreatePass.text.matches(Regex("^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%.*#?&])[A-Za-z\\d@$!%.*#?&]{8,}$"))) {
                    etCreatePass.startAnimation(CommonMethods.shakeError())
                    etCreatePass.error =
                        "Password should be alphanumeric and 8 char and contains special char"
                    etCreatePass.requestFocus()
                } else if (etConfirmPass.text.isEmpty()) {
                    etConfirmPass.startAnimation(CommonMethods.shakeError())
                    etConfirmPass.error = "Required"
                    etConfirmPass.requestFocus()
                } else if (!etCreatePass.text.toString().equals(etConfirmPass.text.toString())) {
                    etConfirmPass.startAnimation(CommonMethods.shakeError())
                    etConfirmPass.error = "Password does not match"
                    etConfirmPass.requestFocus()
                } else
                    commonViewModel.userSignUp(
                        1, etEmailAddress.text.toString().trim(),
                        FirebaseInstanceId.getInstance().token!!, etName.text.toString().trim(),
                        etConfirmPass.text.toString()
                        , this
                    )
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        super.onCreate(savedInstanceState)
        setContentView(R.layout.framgent_email_signup)
        initalize()
    }

    private fun initalize() {
        tvAlreadyAccount.movementMethod = LinkMovementMethod.getInstance()
        tvAlreadyAccount.append(" ")
        tvAlreadyAccount.append("Already have account? ")
        tvAlreadyAccount.append(spanString("LOGIN"))

        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel::class.java)
        commonViewModel.userResponse().observe(this, object : Observer<UserLogin> {
            override fun onChanged(t: UserLogin?) {
                if (t!!.errorMessage != null) {
                    Toast.makeText(this@EmailSignUpActivity, t.errorMessage, Toast.LENGTH_SHORT)
                        .show()
                } else {
                    val intent = Intent(this@EmailSignUpActivity, com.ibox.washapp.activities.PhoneNumberActivity::class.java)
                    intent.putExtra("Change", "FromEmailSignUp")
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    finish()
                }
            }
        })

        ivBackArrow.setOnClickListener(this)
        btnLoginWithEmail.setOnClickListener(this)

    }

    fun spanString(message: String): SpannableString {
        val ss = SpannableString(message)
        val span1 = object : ClickableSpan() {
            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false
            }

            override fun onClick(v: View) {
                startActivity(Intent(this@EmailSignUpActivity, com.ibox.washapp.activities.LoginActivity::class.java))
            }
        }
        ss.setSpan(
            StyleSpan(Typeface.BOLD),
            0,
            ss.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        ss.setSpan(
            ForegroundColorSpan(Color.WHITE),
            0,
            ss.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        ss.setSpan(span1, 0, ss.length, 0)
        return ss
    }
}