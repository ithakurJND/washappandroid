package com.ibox.washapp.activities

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.ibox.washapp.R
import com.ibox.washapp.fragments.PaymentFragment
import com.ibox.washapp.fragments.PickAndDeliveryFragment
import com.ibox.washapp.fragments.ReviewFragment

class CommonActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        setContentView(R.layout.activity_common)
        Log.i("GOTO", "" + intent.getStringExtra("GOTO"))

        if (intent.getStringExtra("GOTO") == "Payment") {
            val trans = supportFragmentManager.beginTransaction()
            val fragment = PaymentFragment()
            val bundle = Bundle()
            bundle.putBoolean("isFromBasket", true)
            bundle.putString(
                "amount", intent.getFloatExtra("amount", 0f)
                    .toString()
            )
            bundle.putInt("id", intent.getIntExtra("id", 0))
            bundle.putInt("credits", 0)
            fragment.arguments = bundle
            trans.add(R.id.frameLayoutCommon, fragment)
            trans.commit()
        } else if (intent.getStringExtra("GOTO") == "PickAndDelivery") {
            val trans = supportFragmentManager.beginTransaction()
            val fragment = PickAndDeliveryFragment()
            val bundle = Bundle()
            bundle.putInt("id", intent.getIntExtra("id", 0))
            bundle.putString("isFromInComplete", "isFromInComplete")
            fragment.arguments = bundle
            trans.add(R.id.frameLayoutCommon, fragment)
            trans.commit()

        }
        else if (intent.getStringExtra("GOTO")=="Review")
        {
            val trans = supportFragmentManager.beginTransaction()
            val fragment = ReviewFragment()
            val bundle = Bundle()
            bundle.putInt("id", intent.getIntExtra("id", 0))
            bundle.putString("isFrom","ReviewInComplete")
            bundle.putString("pickupAddress",intent.getStringExtra("pickupAddress"))
            bundle.putString("deliveryAddress",intent.getStringExtra("deliveryAddress"))
            bundle.putString("pickupDate",intent.getStringExtra("pickupDate"))
            bundle.putString("deliveryDate",intent.getStringExtra("deliveryDate"))
            bundle.putString("PickUpTime",intent.getStringExtra("PickUpTime"))
            bundle.putString("DropOffTime",intent.getStringExtra("DropOffTime"))
            bundle.putString("TimePick",intent.getStringExtra("TimePick"))
            bundle.putString("TimeDrop",intent.getStringExtra("TimeDrop"))

            fragment.arguments = bundle
            trans.add(R.id.frameLayoutCommon, fragment)
            trans.commit()

        }
    }
}