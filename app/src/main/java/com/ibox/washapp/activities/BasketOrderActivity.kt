package com.ibox.washapp.activities

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.ibox.washapp.R
import com.ibox.washapp.adapters.BasketQuantityAdapter
import com.ibox.washapp.model.OrderData
import com.ibox.washapp.utils.Constants
import kotlinx.android.synthetic.main.activity_basket_order.*
import java.util.*
import kotlin.Comparator


class BasketOrderActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_basket_order)
        setupToolbar()
        intialize()
        setListner()
    }

    private fun setListner() {
        btnAddContinue.setOnClickListener {
            startActivity(
                Intent(this, com.ibox.washapp.activities.SelectServicesActivity::class.java)
                    .putExtra("isFromBasket",true))
        }
    }

    private fun intialize() {
        rvProducts.layoutManager = LinearLayoutManager(this)
        val data = com.ibox.washapp.utils.Prefs.with(this).getObject(Constants.ORDER_DATABASKET, OrderData::class.java)
        val listProduct : MutableList<com.ibox.washapp.model.Product> = mutableListOf()
        listProduct.clear()
        listProduct.addAll(data.product!!.values)
        Collections.sort(listProduct, Comparator<com.ibox.washapp.model.Product> { obj1, obj2 ->
            // ## Ascending order
            obj1.name.compareTo(obj2.name,true) // To compare string values
            // return Integer.valueOf(obj1.empId).compareTo(Integer.valueOf(obj2.empId)); // To compare integer values
            // ## Descending order
            // return obj2.firstName.compareToIgnoreCase(obj1.firstName); // To compare string values
            // return Integer.valueOf(obj2.empId).compareTo(Integer.valueOf(obj1.empId)); // To compare integer values
        })
        rvProducts.adapter = BasketQuantityAdapter(listProduct,this)
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.back_arrow)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}