package com.ibox.washapp.activities

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.ibox.washapp.R
import com.ibox.washapp.fragments.SelectItemsFragment
import com.ibox.washapp.fragments.SelectServicesFragment
import com.ibox.washapp.utils.Constants
import com.ibox.washapp.viewModels.LaundryViewModel


class SelectServicesActivity : AppCompatActivity() {
    val laundryViewModel = LaundryViewModel()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        setContentView(R.layout.activity_common)
        val trans = supportFragmentManager.beginTransaction()
        if (intent.hasExtra("isFromBasket")) {
            if (intent.getBooleanExtra("isFromBasket", false)) {
                val fragment = SelectServicesFragment()
                val bundle = Bundle()
                bundle.putBoolean("isFromBasket", true)
                fragment.arguments = bundle
                trans.add(R.id.frameLayoutCommon, fragment)
                trans.commit()
            } else {
                trans.add(R.id.frameLayoutCommon, SelectServicesFragment())
                trans.commit()
            }
        } else {
            trans.add(R.id.frameLayoutCommon, SelectServicesFragment())
            trans.commit()
        }

    }

    override fun onBackPressed() {
 val fragment =SelectItemsFragment()
        val f: Fragment =
           this.supportFragmentManager.findFragmentById(R.id.frameLayoutCommon)!!
        if (f is SelectItemsFragment)
        {
          Log.i("ORDER",""+f.OrderId)
            if (f.OrderId != 0) {
                val builder = android.app.AlertDialog.Builder(
                    this)
                builder.setTitle("Cancel the Order?")
                builder.setMessage("Are you sure you want to Cancel the order?")
                builder.setPositiveButton("YES") { dialog, which ->
                    com.ibox.washapp.utils.Prefs.with(this).remove(Constants.PICK_TIME)
                    com.ibox.washapp.utils.Prefs.with(this).remove(Constants.PICK_ADDRESS)
                    com.ibox.washapp.utils.Prefs.with(this).remove(Constants.DROP_TIME)
                    com.ibox.washapp.utils.Prefs.with(this).remove(Constants.DROP_ADDRESS)
                    com.ibox.washapp.utils.Prefs.with(this).remove(Constants.PICKADDID)
                    com.ibox.washapp.utils.Prefs.with(this).remove(Constants.PICKADD)
                    com.ibox.washapp.utils.Prefs.with(this).remove(Constants.DROPADD)
                    com.ibox.washapp.utils.Prefs.with(this).remove(Constants.DROPADDID)
                    laundryViewModel.cancelOrder(this, f.OrderId)
                     finish()
                }
                builder.setNegativeButton("No") { dialog, which ->
                    dialog.dismiss()
                }
                val dialog: android.app.AlertDialog = builder.create()
                dialog.setOnShowListener {
                    dialog.getButton(android.app.AlertDialog.BUTTON_NEGATIVE)
                        .setTextColor(resources.getColor(R.color.drakBlue))
                    dialog.getButton(android.app.AlertDialog.BUTTON_POSITIVE)
                        .setTextColor(resources.getColor(R.color.drakBlue))
                    dialog.getButton(android.app.AlertDialog.BUTTON_NEUTRAL)
                        .setTextColor(resources.getColor(R.color.drakBlue))
                }
                dialog.show()
            }
            else
                super.onBackPressed()
        }
        else
        super.onBackPressed()


    }
}
