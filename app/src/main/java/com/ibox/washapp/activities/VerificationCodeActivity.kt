package com.ibox.washapp.activities

import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.util.Log
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ibox.washapp.R
import com.ibox.washapp.utils.CommonMethods
import com.ibox.washapp.viewModels.VerificationViewModel
import kotlinx.android.synthetic.main.fragment_verification_code.*

class VerificationCodeActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var verificationViewModel: VerificationViewModel
    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.btnVerify -> {
                if (etOtpNumOne.text.isEmpty() or etOtpNumTwo.text.isEmpty() or
                    etOtpNumThree.text.isEmpty() or etOtpNumFour.text.isEmpty()
                ) {

                    etOtpNumOne.startAnimation(CommonMethods.shakeError())
                    etOtpNumOne.setHintTextColor(Color.RED)
                    val colorStateList = ColorStateList.valueOf(Color.RED)
                    etOtpNumOne.backgroundTintList = (colorStateList)

                    etOtpNumTwo.startAnimation(CommonMethods.shakeError())
                    etOtpNumTwo.setHintTextColor(Color.RED)
                    etOtpNumTwo.backgroundTintList = (colorStateList)

                    etOtpNumThree.startAnimation(CommonMethods.shakeError())
                    etOtpNumThree.setHintTextColor(Color.RED)
                    etOtpNumThree.backgroundTintList = (colorStateList)

                    etOtpNumFour.startAnimation(CommonMethods.shakeError())
                    etOtpNumFour.setHintTextColor(Color.RED)
                    etOtpNumFour.backgroundTintList = (colorStateList)
                } else {
                    val otp =
                        etOtpNumOne.text.toString() + etOtpNumTwo.text.toString() +
                                etOtpNumThree.text.toString() +
                                etOtpNumFour.text.toString()
                    Log.i("OTP", "" + otp)
                    verificationViewModel.otpVerification(otp, this)
                }
            }

        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_verification_code)
        intialize()
    }

    private fun intialize() {
        tvGetNumber.text = intent.getStringExtra("countryCode")+intent.getStringExtra("number")
        editTextNext(etOtpNumOne, etOtpNumTwo, etOtpNumOne)
        editTextNext(etOtpNumTwo, etOtpNumThree, etOtpNumOne)
        editTextNext(etOtpNumThree, etOtpNumFour, etOtpNumTwo)
        editTextNext(etOtpNumFour, etOtpNumFour, etOtpNumThree)
        tvResend.movementMethod = LinkMovementMethod.getInstance()
        tvResend.append(" ")
        tvResend.append("Didn't receive the Code? ")
        tvResend.append(spanString("RESEND"))

        /*argument sent from profile through phonenumberfragment */
        val argu = intent.getStringExtra("From")
        Log.i("LOG", "" + argu)
        ivBackArrow.setOnClickListener()
        {
            onBackPressed()
//            Log.i("VERI", "" + argu)
//            if (argu == "Address") {
//                startActivity(Intent(this, PhoneNumberActivity::class.java))
//            } else if (argu == "FromEmailSignUp") {
//                finish()
//                startActivity(Intent(this, PhoneNumberActivity::class.java))
//            }


        }
        verificationViewModel = ViewModelProviders.of(this).get(VerificationViewModel::class.java)
        verificationViewModel.varifyResponse().observe(this, Observer { message ->
            if (message.message == "Phone Number Verified") {
                if (intent.hasExtra("phone")) {
                    val intent = Intent(this, com.ibox.washapp.activities.OderProfileAddressActivity::class.java)
                    intent.putExtra("Selected", "ProfileFromVerify")
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                    startActivity(intent)
                } else {
                    finishAffinity()
                    startActivity(Intent(this, AfterSignupAddressDefaultActivity::class.java))
                }
            }
        })
        btnVerify.setOnClickListener(this)
    }

    private fun editTextNext(editTextFrom: EditText, editTextTo: EditText, editTextBack: EditText) {
        editTextFrom.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (editTextFrom.text.isEmpty()) {
                    editTextBack.requestFocus()
                }
                val colorStateList = ColorStateList.valueOf(Color.WHITE)
                etOtpNumOne.backgroundTintList = (colorStateList)
                etOtpNumTwo.backgroundTintList = (colorStateList)
                etOtpNumThree.backgroundTintList = (colorStateList)
                etOtpNumFour.backgroundTintList = (colorStateList)
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (editTextFrom.text.length == 1) {
                    editTextTo.requestFocus()
                }
            }
        })
    }

   private fun spanString(message: String): SpannableString {
        val ss = SpannableString(message)
        val span1 = object : ClickableSpan() {
            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false
            }

            override fun onClick(v: View) {
                verificationViewModel.resendOtp(
                    intent.getStringExtra("number")!!,
                    this@VerificationCodeActivity,
                    intent.getStringExtra("countryCode")!!
                )
            }
        }
        ss.setSpan(
            StyleSpan(Typeface.BOLD),
            0,
            ss.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        ss.setSpan(
            ForegroundColorSpan(Color.WHITE),
            0,
            ss.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        ss.setSpan(span1, 0, ss.length, 0)
        return ss
    }

}