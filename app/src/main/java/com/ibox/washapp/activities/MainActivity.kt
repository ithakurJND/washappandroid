package com.ibox.washapp.activities

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.ibox.washapp.R
import com.ibox.washapp.fragments.*
import com.ibox.washapp.utils.Constants
import com.ibox.washapp.viewModels.CommonViewModel
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), View.OnClickListener {
    var commonViewModel = CommonViewModel()
    internal var broadcastReceiverForBottomsheet: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val clicked = intent.getStringExtra("CLICKED")
            if (clicked == "AddToBasket") {
                llBasket.performClick()
            } else if (clicked == "ScheduledOrder") {
                llSchedule.performClick()
            } else if (clicked == "HOME") {
                llHome.performClick()
            }

        }
    }

    private fun getCurrentFragment(): Fragment {
        return supportFragmentManager.findFragmentByTag("android:switcher:" + R.id.nonSwipableViewPager + ":" + nonSwipableViewPager.currentItem)!!

    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.llHome -> {
                com.ibox.washapp.utils.Prefs.with(this).remove(Constants.PICK_TIME)
                com.ibox.washapp.utils.Prefs.with(this).remove(Constants.PICK_ADDRESS)
                com.ibox.washapp.utils.Prefs.with(this).remove(Constants.DROP_TIME)
                com.ibox.washapp.utils.Prefs.with(this).remove(Constants.DROP_ADDRESS)
                com.ibox.washapp.utils.Prefs.with(this).remove(Constants.PICKADDID)
                com.ibox.washapp.utils.Prefs.with(this).remove(Constants.PICKADD)
                com.ibox.washapp.utils.Prefs.with(this).remove(Constants.DROPADD)
                com.ibox.washapp.utils.Prefs.with(this).remove(Constants.DROPADDID)
                unSelectAll()
                llHome.isSelected = true
                nonSwipableViewPager.currentItem = 0
            }
            R.id.llBasket -> {
                unSelectAll()
                llBasket.isSelected = true
                nonSwipableViewPager.currentItem = 1
                (getCurrentFragment() as AddToBasketFragment).sortList()
            }
            R.id.llSchedule -> {
                com.ibox.washapp.utils.Prefs.with(this).remove(Constants.PICK_TIME)
                com.ibox.washapp.utils.Prefs.with(this).remove(Constants.PICK_ADDRESS)
                com.ibox.washapp.utils.Prefs.with(this).remove(Constants.DROP_TIME)
                com.ibox.washapp.utils.Prefs.with(this).remove(Constants.DROP_ADDRESS)
                com.ibox.washapp.utils.Prefs.with(this).remove(Constants.PICKADDID)
                com.ibox.washapp.utils.Prefs.with(this).remove(Constants.PICKADD)
                com.ibox.washapp.utils.Prefs.with(this).remove(Constants.DROPADD)
                com.ibox.washapp.utils.Prefs.with(this).remove(Constants.DROPADDID)
                unSelectAll()
                llSchedule.isSelected = true
                nonSwipableViewPager.currentItem = 3

            }
            R.id.llMore -> {
                com.ibox.washapp.utils.Prefs.with(this).remove(Constants.PICK_TIME)
                com.ibox.washapp.utils.Prefs.with(this).remove(Constants.PICK_ADDRESS)
                com.ibox.washapp.utils.Prefs.with(this).remove(Constants.DROP_TIME)
                com.ibox.washapp.utils.Prefs.with(this).remove(Constants.DROP_ADDRESS)
                com.ibox.washapp.utils.Prefs.with(this).remove(Constants.PICKADDID)
                com.ibox.washapp.utils.Prefs.with(this).remove(Constants.PICKADD)
                com.ibox.washapp.utils.Prefs.with(this).remove(Constants.DROPADD)
                com.ibox.washapp.utils.Prefs.with(this).remove(Constants.DROPADDID)
                unSelectAll()
                llMore.isSelected = true
                nonSwipableViewPager.currentItem = 4
            }
            R.id.ivAddServices -> {
                BottomSheetFragment().show(supportFragmentManager, BottomSheetFragment().tag)
                unSelectAll()
            }
        }
    }

    fun unSelectAll() {
        llHome.isSelected = false
        llBasket.isSelected = false
        llSchedule.isSelected = false
        llMore.isSelected = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        setContentView(R.layout.activity_main)
        val intent = IntentFilter("ADD_TO_BASKET")
        this.registerReceiver(broadcastReceiverForBottomsheet, intent)
        llHome.isSelected = true
        nonSwipableViewPager.offscreenPageLimit = 5
        nonSwipableViewPager.adapter = tabAddapter(supportFragmentManager)

        llHome.setOnClickListener(this)
        llMore.setOnClickListener(this)
        llBasket.setOnClickListener(this)
        llSchedule.setOnClickListener(this)
        ivAddServices.setOnClickListener(this)

        /*From Confirmation fragment*/
        val selected = getIntent().extras
        if (selected != null) {
            val string = selected.getString("Selected")
            if (string == "BottomSheet") {
                ivAddServices.performClick()
            }
        }
        nonSwipableViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                if (position == 1) {
                    commonViewModel.addToBasket(this@MainActivity, true, 0)
                }
            }
        })

    }

    override fun onBackPressed() {
        if (nonSwipableViewPager.currentItem != 0) {
            llHome.performClick()
        } else {
            super.onBackPressed()
        }
    }

    inner class tabAddapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        override fun getItem(position: Int): Fragment {
            if (position == 0) {
                return HomeFragment()
            } else if (position == 1) {
                return AddToBasketFragment()
            } else if (position == 2) {
                return Fragment()
            } else if (position == 3) {
                return Schedulefragment()
            } else {
                return MoreFragment()
            }
        }

        override fun getCount(): Int {
            return 5
        }

    }

    override fun onResumeFragments() {
        super.onResumeFragments()
    }

    override fun onDestroy() {
        super.onDestroy()
        this.unregisterReceiver(broadcastReceiverForBottomsheet)
    }
}



