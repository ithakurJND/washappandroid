package com.ibox.washapp.activities

import android.app.AlertDialog
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.iid.FirebaseInstanceId
import com.ibox.washapp.R
import com.ibox.washapp.utils.CommonMethods
import com.ibox.washapp.viewModels.CommonViewModel
import com.mukesh.countrypicker.CountryPicker
import com.mukesh.countrypicker.listeners.OnCountryPickerListener
import kotlinx.android.synthetic.main.fragment_phone_number.*


class PhoneNumberActivity : AppCompatActivity() {
    var countryName = ""
    var countryCode = ""
    var commonViewModel = CommonViewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_phone_number)
        intialize()
        countryCode = tvCountryCode.text.toString()
    }

    private fun intialize() {
        if (intent.hasExtra("phone")) {
            etPhoneNumber.setText(intent.getStringExtra("phone"))
            tvLogoutPhone.visibility = View.GONE
        }
        ivFlag.setOnClickListener {
            val builder = CountryPicker.Builder().with(this)
                .listener(OnCountryPickerListener {
                    countryName = it.name
                    countryCode = it.dialCode
                    ivFlag.setImageResource(it.flag)
                    tvCountryCode.text = countryCode
                })
            val picker: CountryPicker = builder.build();
            picker.showDialog(this)
            CountryPicker.Builder().listener { }
        }

        etPhoneNumber.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                val colorStateList = ColorStateList.valueOf(Color.WHITE)
                etPhoneNumber.backgroundTintList = (colorStateList)
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

        })




        tvsendCodeInfo.movementMethod = LinkMovementMethod.getInstance()
        tvsendCodeInfo.append(" ")
        tvsendCodeInfo.append("we will send you the ")
        tvsendCodeInfo.append(spanString("Verification Code"))
        tvsendCodeInfo.append("\n on this mobile number")

        val argu = intent.getStringExtra("Change")
        Log.i("LOG", "" + argu)
        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel::class.java)
        commonViewModel.getEditProfile().observe(this, Observer { phone ->
            /* argument From CommonActivity and emailsignUp*/
            if (!phone.message.isEmpty()) {
                if (intent.hasExtra("phone")) {
                    startActivity(
                        Intent(this, VerificationCodeActivity::class.java)
                            .putExtra("from", argu)
                            .putExtra("number",  etPhoneNumber.text.toString())
                            .putExtra("countryCode",countryCode )
                            .putExtra("phone", true)
                    )
                } else {
                    startActivity(
                        Intent(this, VerificationCodeActivity::class.java)
                            .putExtra("from", argu)
                            .putExtra("number",  etPhoneNumber.text.toString())
                            .putExtra("countryCode",countryCode )
                    )
                }
            }
        })
        commonViewModel.getEditProfile().observe(this, Observer { logout ->
            if (logout.message == "Logged out Successfully") {
                com.ibox.washapp.utils.Prefs.with(this).removeAll(this)
                val intent = Intent(this, SplashActivity::class.java)
                intent.flags =
                    Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                finish()

            }
        })
        btnSendCode.setOnClickListener()
        {
            if (etPhoneNumber.text.isEmpty()) {
                etPhoneNumber.error = "Enter number"
                etPhoneNumber.startAnimation(CommonMethods.shakeError())
                etPhoneNumber.setHintTextColor(Color.RED)
                val colorStateList = ColorStateList.valueOf(Color.RED)
                etPhoneNumber.backgroundTintList = (colorStateList)
            } else {
                commonViewModel.phoneNumber( etPhoneNumber.text.toString(),countryCode , this)
            }

        }
        tvLogoutPhone.setOnClickListener()
        {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Logout?")
            builder.setMessage("Are you sure you want to logout?")
            builder.setPositiveButton("YES") { dialog, which ->
                commonViewModel.logout(FirebaseInstanceId.getInstance().token!!, this)

            }

            builder.setNegativeButton("No") { dialog, which ->
                dialog.dismiss()
            }
            val dialog: AlertDialog = builder.create()
            dialog.setOnShowListener {
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
                    .setTextColor(resources.getColor(R.color.drakBlue))
                dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                    .setTextColor(resources.getColor(R.color.drakBlue))
                dialog.getButton(AlertDialog.BUTTON_NEUTRAL)
                    .setTextColor(resources.getColor(R.color.drakBlue))
            }
            dialog.show()
        }

    }


    fun spanString(message: String): SpannableString {
        val ss = SpannableString(message)
        val span1 = object : ClickableSpan() {
            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false
            }

            override fun onClick(v: View) {
                // Toast.makeText(context, "Clickable Span", Toast.LENGTH_SHORT).show()
//                CommonMethods.replaceFragment(fragmentManager!!, SignUpFragment())
            }
        }
        ss.setSpan(
            StyleSpan(Typeface.BOLD),
            0,
            ss.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        ss.setSpan(
            ForegroundColorSpan(Color.WHITE),
            0,
            ss.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        ss.setSpan(span1, 0, ss.length, 0)
        return ss
    }
}
