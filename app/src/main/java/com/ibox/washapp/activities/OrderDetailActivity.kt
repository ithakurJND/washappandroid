package com.ibox.washapp.activities

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.PopupMenu
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.ibox.washapp.R
import com.ibox.washapp.adapters.OrderDetailProductAdapter
import com.ibox.washapp.dialog.ReviewDialog
import com.ibox.washapp.utils.CommonFunctions
import com.ibox.washapp.utils.Constants
import com.ibox.washapp.viewModels.LaundryViewModel
import kotlinx.android.synthetic.main.fragment_pick_and_delivery.rvAddedToCart
import kotlinx.android.synthetic.main.fragment_view_order.*
import kotlinx.android.synthetic.main.fragment_view_order.tvAddedNo

class OrderDetailActivity : AppCompatActivity() {
    private var laundryViewModel = LaundryViewModel()
    private var orderDetails: com.ibox.washapp.model.PojoOrderDetails =
        com.ibox.washapp.model.PojoOrderDetails()
    var DropOffTime = ""
    var PickUptime = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_view_order)
        intialize()
    }

    private fun intialize() {
        rvAddedToCart.layoutManager = LinearLayoutManager(this)
        laundryViewModel = ViewModelProviders.of(this).get(LaundryViewModel::class.java)
        laundryViewModel.orderDetailsMutableLiveData.observe(this, Observer {
            orderDetails = it
            Log.i("FromBasket", "" + Gson().toJson(it.order_items))
            rvAddedToCart.adapter = OrderDetailProductAdapter(
                it.order_items, this
                , it.service_type
            )
            if (it.laundry_name != null && it.laundry_name.isNotEmpty()) {
                tvShopName.text = it.laundry_name
            } else {
                tvShopName.text = "Basket Order"
            }

            tvOrderNo.text = "Order #" + it.orderId
            tvPayedMoney.text =
                "AED " + if (it.payment != null) it.payment.payable_amount else it.amount
            tvAddedNo.text = it.order_items.size.toString()
            tvPickUpAddress.text = it.pickup_address
            if (it.pickup_address == null || it.pickup_address.length == 0) {
                cardViewDetails.visibility = View.GONE
            } else {
                cardViewDetails.visibility = View.VISIBLE
            }
            try {
                tvPickUpTime.text = CommonFunctions.formatDate(
                    "yyyy-MM-dd'T'HH:mm:ss.sss'Z'",
                    "dd-MMM",
                    it.pickup_date
                ) + ", " + it.pickup_time_slot

                PickUptime = CommonFunctions.formatDate(
                    "yyyy-MM-dd'T'HH:mm:ss.sss'Z'",
                    "dd-MMM",
                    it.pickup_date
                ) + ", " + it.pickup_time_slot

            } catch (e: Exception) {
            }
            tvDropOffAddress.text = it.dropoff_address
            try {
                tvDropOffTime.text = CommonFunctions.formatDate(
                    "yyyy-MM-dd'T'HH:mm:ss.sss'Z'",
                    "dd-MMM",
                    it.drop_off_date
                ) + ", " + it.drop_off_time_slot
                DropOffTime = CommonFunctions.formatDate(
                    "yyyy-MM-dd'T'HH:mm:ss.sss'Z'",
                    "dd-MMM",
                    it.pickup_date
                ) + ", " + it.pickup_time_slot
                Log.i("DROP", "" + DropOffTime)
            } catch (e: Exception) {
            }

            tvInstruction.text = it.special_note
            tvPref.text = it.preference

            if (((it.special_note == null || it.special_note.length == 0) && (it.preference == null || it.preference.length == 0))) {
                sa.visibility = View.GONE
            } else {
                sa.visibility = View.VISIBLE
                if (it.preference == null || it.preference.length == 0) {
                    llPre.visibility = View.GONE
                } else {
                    llPre.visibility = View.VISIBLE
                }
                if (it.special_note == null || it.special_note.length == 0) {
                    llSpe.visibility = View.GONE
                } else {
                    llSpe.visibility = View.VISIBLE
                }
            }

            if (it.payment == null) {
                tvPaymentMethod.text = "Payment InComplete"
                btnContinueAndPay.visibility = View.VISIBLE
            } else {
                ivMenu.visibility = View.GONE
                btnContinueAndPay.visibility = View.INVISIBLE
                when (it.payment.payment_method) {
                    1 -> {
                        tvPaymentMethod.text = "Paid Via Cash"
                    }
                    2 -> {
                        tvPaymentMethod.text = "Paid Via Card"
                    }
                    3 -> {
                        tvPaymentMethod.text = "Paid Via Credits"
                    }
                    4 -> {
                        tvPaymentMethod.text = "Paid Via Cash and Credits"
                    }
                    5 -> {
                        tvPaymentMethod.text = "Paid Via Card and Credits"
                    }
                }
            }
            try {
                var s = ""
                for (ia in it.categories) {
                    s = if (s == "") {
                        ia
                    } else {
                        s + ", " + ia
                    }
                }
                tvService.text = s
            }
            catch (e:Exception)
            {}

            when (it.status) {
                0 -> {
                    tvStatus.background =
                        ContextCompat.getDrawable(this, R.drawable.txt_new_order_background)
                    tvStatus.text = "Hold"
                    if (it.payment == null)
                        ivMenu.visibility = View.VISIBLE
                    else
                        ivMenu.visibility = View.GONE
                }
                1 -> {
                    btnContinueAndPay.visibility = View.VISIBLE
                    btnContinueAndPay.text = "Order Cancelled"
                    tvStatus.background = ContextCompat.getDrawable(this, R.drawable.cancel_status)
                    tvStatus.text = "Cancelled"
                    ivMenu.visibility = View.GONE
                }
                2 -> {
                    tvStatus.background =
                        ContextCompat.getDrawable(this, R.drawable.txt_new_order_background)
                    tvStatus.text = "Failed"
                    ivMenu.visibility = View.GONE
                }
                3 -> {
                    tvStatus.background =
                        ContextCompat.getDrawable(this, R.drawable.txt_new_order_background)
                    tvStatus.text = "Receive the goods"
                    ivMenu.visibility = View.GONE
                }
                4 -> {
                    tvStatus.background =
                        ContextCompat.getDrawable(this, R.drawable.txt_new_order_background)
                    tvStatus.text = "Processing"
                    ivMenu.visibility = View.GONE
                }
                5 -> {
                    tvStatus.background =
                        ContextCompat.getDrawable(this, R.drawable.txt_new_order_background)
                    tvStatus.text = "Ready to pick"
                    ivMenu.visibility = View.GONE
                }
                6 -> {
                    tvStatus.background =
                        ContextCompat.getDrawable(this, R.drawable.txt_new_order_background)
                    tvStatus.text = "Driver picked"
                    ivMenu.visibility = View.GONE
                }
                7 -> {
                    tvStatus.background =
                        ContextCompat.getDrawable(this, R.drawable.txt_new_order_background)
                    ivMenu.visibility = View.GONE
                    tvStatus.text = "Delivered"
                }
                8 -> {
                    tvStatus.background =
                        ContextCompat.getDrawable(this, R.drawable.txt_new_order_background)
                    ivMenu.visibility = View.GONE
                    btnContinueAndPay.visibility = View.VISIBLE
                    btnContinueAndPay.text = "Rate order"
                    tvStatus.text = "Completed"
                }
                9 -> {
                    tvStatus.background =
                        ContextCompat.getDrawable(this, R.drawable.txt_new_order_background)
                    ivMenu.visibility = View.GONE
                    btnContinueAndPay.visibility = View.VISIBLE
                    btnContinueAndPay.text = "Order Rated"
                    tvStatus.text = "Completed"
                }
            }

            if (orderDetails.is_rated >= 1) {
                btnContinueAndPay.visibility = View.VISIBLE
                btnContinueAndPay.text = "Order Rated"
            }

            btnContinueAndPay.setOnClickListener {
                if (btnContinueAndPay.text.toString() == "Order Cancelled") {
                    Toast.makeText(this, "Order cancelled", Toast.LENGTH_LONG).show()
                } else {

                    if (orderDetails.status == 8 && orderDetails.is_rated == 0) {
                        showReviewDialog()
                    } else if (orderDetails.status == 0 && orderDetails.pickup_time_slot == null) {
                        startActivity(
                            Intent(this, com.ibox.washapp.activities.CommonActivity::class.java)
                                .putExtra("id", orderDetails.id)
                                .putExtra("GOTO", "PickAndDelivery")
                        )
                        com.ibox.washapp.utils.Prefs.with(this)
                            .save(Constants.ORDER_ITEMS_COMP_ORDER, orderDetails)
                        Log.i("Order_item", "" + Gson().toJson(orderDetails.order_items))
                    } else if (orderDetails.status == 0 && orderDetails.special_note == null
                        || orderDetails.preferrence_id == null
                    ) {
                        startActivity(
                            Intent(this, com.ibox.washapp.activities.CommonActivity::class.java)
                                .putExtra("id", orderDetails.id)
                                .putExtra("GOTO", "Review")
                                .putExtra("pickupAddress", orderDetails.pickup_address)
                                .putExtra("deliveryAddress", orderDetails.dropoff_address)
                                .putExtra("pickupDate", orderDetails.pickup_date)
                                .putExtra("deliveryDate", orderDetails.drop_off_date)
                                .putExtra("PickUpTime", PickUptime)
                                .putExtra("DropOffTime", DropOffTime)
                                .putExtra("TimePick", orderDetails.pickup_time_slot)
                                .putExtra("TimeDrop", orderDetails.drop_off_time_slot)
                        )
                        Log.i("TIMESS", "" + orderDetails.pickup_time_slot)

                    } else if (orderDetails.status == 0 && orderDetails.payment == null) {
                        startActivity(
                            Intent(this, com.ibox.washapp.activities.CommonActivity::class.java)
                                .putExtra("id", orderDetails.id)
                                .putExtra("amount", orderDetails.amount)
                                .putExtra("GOTO", "Payment")

                        )
                    }

                }
            }
        })

        laundryViewModel.cancelOrderLiveData.observe(this, Observer {
            laundryViewModel.orderDetails(this, intent.getIntExtra("id", 0))
        })

        laundryViewModel.orderDetails(this, intent.getIntExtra("id", 0))



        ivBackArrow.setOnClickListener()
        {
            onBackPressed()
        }
        ivMenu.setOnClickListener()
        {
            val popupMenu = PopupMenu(this, ivMenu)
            popupMenu.menuInflater.inflate(R.menu.order_details_menu, popupMenu.menu)
            popupMenu.show()
            popupMenu.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.item1 -> {
                        confirmDialog()
                    }
                }
                true
            })
        }
        rlAddedToCart.setOnClickListener()
        {
            if (rvAddedToCart.visibility == View.GONE) {
                rvAddedToCart.visibility = View.VISIBLE
                ivUpArrowReview.visibility = View.GONE
                ivDropDownReview.visibility = View.VISIBLE
            } else {
                rvAddedToCart.visibility = View.GONE
                ivUpArrowReview.visibility = View.VISIBLE
                ivDropDownReview.visibility = View.GONE
            }
        }
    }

    private fun confirmDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Cancel Order")
        builder.setMessage("Do you really want to cancel the order?")
        builder.setPositiveButton("YES") { dialog, which ->
            laundryViewModel.cancelOrder(this, orderDetails.id)
        }

        builder.setNegativeButton("No") { dialog, which ->
            dialog.dismiss()
        }
        val dialog: AlertDialog = builder.create()
        dialog.setOnShowListener {
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
                .setTextColor(resources.getColor(R.color.drakBlue))
            dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                .setTextColor(resources.getColor(R.color.drakBlue))
            dialog.getButton(AlertDialog.BUTTON_NEUTRAL)
                .setTextColor(resources.getColor(R.color.drakBlue))
        }
        dialog.show()
    }

    private fun showReviewDialog() {
        val dialog =
            ReviewDialog(this, orderDetails.id, orderDetails.laundry_name, orderDetails.orderId)
        dialog.setOnDismissListener {
            laundryViewModel.orderDetails(this, intent.getIntExtra("id", 0))

        }
        dialog.show()
    }

    override fun onBackPressed() {
        if (intent.getStringExtra("isFrom") == "PushThroughSplash") {
            startActivity(Intent(this, com.ibox.washapp.activities.MainActivity::class.java))
            finish()
        } else
            super.onBackPressed()
    }

}