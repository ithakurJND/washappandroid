package com.ibox.washapp.activities

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.ibox.washapp.R
import com.ibox.washapp.fragments.MyOrderFragment
import com.ibox.washapp.fragments.NotificationFragment

class OderProfileAddressActivity : AppCompatActivity() {
    val myOrderFragment = MyOrderFragment()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        setContentView(R.layout.activity_common)
        /*Intent from more fragment And Verification number fragment*/
        val order = intent.getStringExtra("Selected")
        Log.i("Action", " " + order)
        val bundle = Bundle()
        bundle.putString("Clicked", order)
        myOrderFragment.arguments = bundle
        if (order == "Notification") {
            supportFragmentManager.beginTransaction()
                .add(R.id.frameLayoutCommon, NotificationFragment())
                .commit()
        } else
            supportFragmentManager.beginTransaction().add(R.id.frameLayoutCommon, myOrderFragment)
                .commit()
    }
}