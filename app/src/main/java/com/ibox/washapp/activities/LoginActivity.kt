package com.ibox.washapp.activities

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.iid.FirebaseInstanceId
import com.ibox.washapp.R
import com.ibox.washapp.utils.CommonMethods
import com.ibox.washapp.viewModels.CommonViewModel
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.item_dialog_box.view.*

class LoginActivity : AppCompatActivity(), View.OnClickListener {
    var commonViewModel = CommonViewModel()
    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.btnLogin -> {

                if (etEmail.text.isEmpty()) {
                    etEmail.error = "Enter Email"
                    etEmail.startAnimation(CommonMethods.shakeError())
                } else if (etPass.text.isEmpty()) {
                    etPass.error = "Enter Password"
                    etPass.startAnimation(CommonMethods.shakeError())
                } else if (!Patterns.EMAIL_ADDRESS.matcher(etEmail.text).matches()) {
                    etEmail.error = "Invalid Email"
                    etEmail.startAnimation(CommonMethods.shakeError())
                } else {
                    commonViewModel.userLogIn(
                        FirebaseInstanceId.getInstance().token!!, 1,
                        etEmail.text.toString(), etPass.text.toString(), "", "", ""
                        , this
                    )
                }
            }
            R.id.tvForgetPass -> {
                showForgotPasswordDialog()
            }
        }
    }

    private fun showForgotPasswordDialog() {
        val view = LayoutInflater.from(this).inflate(R.layout.item_dialog_box, null)
        val builder = AlertDialog.Builder(this, R.style.TransparentDilaog)
        builder.setView(view)
        val dialog: AlertDialog = builder.create()
        dialog.show()
        view.btnSubmit.setOnClickListener()
        {
            if (view.etEmail.text.isEmpty()) {
                Toast.makeText(this, "Enter email", Toast.LENGTH_LONG).show()
            } else if (!Patterns.EMAIL_ADDRESS.matcher(view.etEmail.text).matches()) {
                Toast.makeText(this, "Enter valid email", Toast.LENGTH_LONG).show()
            } else {
                commonViewModel.forgotPass(view.etEmail.text.toString(), this)
                dialog.dismiss()
            }

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_login)
        intialize()
    }

    private fun intialize() {

        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel::class.java)
        commonViewModel.userResponse().observe(this, Observer { login ->
            if (login.errorMessage != null) {
                Toast.makeText(this, "" + login.errorMessage, Toast.LENGTH_LONG).show()
            } else if (login.user!!.phone_no==null) {
                startActivity(Intent(this, com.ibox.washapp.activities.PhoneNumberActivity::class.java))
            } else {
              finishAffinity()
                startActivity(Intent(this, com.ibox.washapp.activities.MainActivity::class.java))

            }
        })
        commonViewModel.getMessage().observe(this, Observer { message ->
            Toast.makeText(this, "" + message.message, Toast.LENGTH_LONG).show()
        })
        tvCreateAccount.movementMethod = LinkMovementMethod.getInstance()
        tvCreateAccount.append(" ")
        tvCreateAccount.append("Don't have an account? ")
        tvCreateAccount.append(spanString("SIGNUP"))
        btnLogin.setOnClickListener(this)
        tvForgetPass.setOnClickListener(this)
    }

    fun spanString(message: String): SpannableString {
        val ss = SpannableString(message)
        val span1 = object : ClickableSpan() {
            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false
            }

            override fun onClick(v: View) {
//                finishAffinity()
                startActivity(Intent(this@LoginActivity, com.ibox.washapp.activities.SignupActivity::class.java))
            }
        }
        ss.setSpan(
            StyleSpan(Typeface.BOLD),
            0,
            ss.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        ss.setSpan(
            ForegroundColorSpan(Color.WHITE),
            0,
            ss.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        ss.setSpan(span1, 0, ss.length, 0)
        return ss
    }
}