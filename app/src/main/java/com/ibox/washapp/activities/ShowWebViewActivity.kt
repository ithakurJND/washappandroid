package com.ibox.washapp.activities

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.View
import android.webkit.JavascriptInterface
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.ibox.washapp.R
import com.ibox.washapp.model.GetRSA
import com.ibox.washapp.utils.*
import com.techautovity.myeve.retrofit.RestClient
import kotlinx.android.synthetic.main.activity_web_view.*
import org.apache.http.NameValuePair
import org.apache.http.message.BasicNameValuePair
import org.apache.http.util.EncodingUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.URLEncoder
import java.util.ArrayList
import java.util.regex.Pattern

class ShowWebViewActivity : AppCompatActivity() {
    private var dialog: ProgressDialog? = null
    var mainIntent: Intent? = null
    var html: String? = null
    var encVal: String? = null
    var vResponse: String = ""
    public override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
        setContentView(R.layout.activity_web_view)
        mainIntent = intent
        // Calling async task to get display content
        var map = HashMap<String, Any>()
        map["access_code"] = mainIntent!!.getStringExtra(Constants.ACCESS_CODE)!!.toString()
        map["order_id"] = mainIntent!!.getStringExtra(Constants.ORDER_ID_FOR)!!.toString()
        CommonFunctions.showProgress(this)
        // MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqznIeM/copoaf20Dt7Ox5BMyYTv1r2CC\n6rI9thQ1y04wP9XsvI23XlFHP+GXh6PDRnBkyJp3aX7uPYQ5L0DBqVIanp9zv6KtBqpGRyQ0AAJd\nUco3tc8y0DfVZwo6Lbf/YJHiy9pLKdrIT9YUuwGjMmUTy8XHA2BUNlbRLd+4iD2bYAOggkzqY+dF\n5eMlV5XahHtRZhW7lNx0osMp3LijTLQ+ViEFlAgaKQHm7iu64wFlwZhdImNtCFDW5LdKED6rEO3a\nNiKCfu/Xl6w3uPQjyXXGPq4kag8LkRKqFQLPACMkeTSo4WgWmVEoP/GMyW4+nRtTiKeptyUclyhJ\n4EqGSQIDAQAB
        RestClient.modalApiService.getRsaKey(map).enqueue(object : Callback<GetRSA> {
            override fun onFailure(call: Call<GetRSA>, t: Throwable) {
                Log.i("ResponeHandler", "failure  ${t.message}")
                CommonFunctions.dismissProgress()
            }

            override fun onResponse(call: Call<GetRSA>, response: Response<GetRSA>) {
//                CommonFunctions.dismissProgress()
                if (response.isSuccessful) {
                    Log.i("ResponeHandler", "succ  ${response.body()}")
                    // if (!response.body().isNullOrEmpty()) {
                    vResponse = response.body()!!.rsa_key
                    val vEncVal = StringBuffer("")
                    vEncVal.append(
                        CommonMethods.addToPostParams(
                            Constants.AMOUNT,
                            mainIntent!!.getStringExtra(Constants.AMOUNT)
                        )
                    )
                    vEncVal.append(
                        CommonMethods.addToPostParams(
                            Constants.CURRENCY,
                            mainIntent!!.getStringExtra(Constants.CURRENCY)
                        )
                    )
                    Log.i("ProcessForPayment", " vEncVal  $vEncVal")
                    Log.i(
                        "ProcessForPayment", "  values to encypt  ${vEncVal.substring(
                            0,
                            vEncVal.length - 1
                        )}"
                    )
                    Log.i("ProcessForPayment", " rsa key  $vResponse")
                    encVal = RSAUtility.encrypt(
                        vEncVal.substring(
                            0,
                            vEncVal.length - 1
                        ), vResponse
                    )
                    Log.i("ProcessForPayment", " encrypted value  $encVal")
                    class MyJavaScriptInterface {
                        @JavascriptInterface
                        fun processHTML(html: String) { // process the html as needed by the app
                            Log.i("ProcessingUrl", "inside processHTML   $html ")
                            var status: String? = null
                            status = if (html.indexOf("Failure") != -1) {
                                "Transaction Declined!"
                            } else if (html.indexOf("Success") != -1) {
                                "Transaction Successful!"
                            } else if (html.indexOf("Aborted") != -1) {
                                "Transaction Cancelled!"
                            } else {
                                "Status Not Known!"
                            }
                            Log.i("ProcessingUrl","$status")
                            Log.i(
                                "ProcessingUrl",
                                "iindex    ${html.indexOf("tracking_id")} "
                            )
                            Log.i(
                                "ProcessingUrl",
                                "iindex    ${html.substring(html.indexOf("tracking_id"), 335)} "
                            )

                        val id = html.substring(html.indexOf("tracking_id"), 335)
                            Log.i("ProcessingUrl","id  $id")
                            Toast.makeText(this@ShowWebViewActivity, status, Toast.LENGTH_SHORT)
                                .show();
                            this@ShowWebViewActivity.sendBroadcast(
                                        Intent("Status")
                                            .putExtra("status", status)
                                            .putExtra("tracking_id",id.replace("[^0-9]".toRegex(), "").toString())
                                    )
                                    this@ShowWebViewActivity.finish()

                           // id.replace("[^0-9]".toRegex(), "").toString()
//                            AlertDialog.Builder(this@ShowWebViewActivity).apply {
//                                setTitle("Payment")
//                                setMessage(status)
//                                setPositiveButton(Html.fromHtml("<font color='#51a7d9'>OK</font>")) { dialog, _ ->
//                                    dialog.dismiss()
//                                    this@ShowWebViewActivity.sendBroadcast(
//                                        Intent("Status")
//                                            .putExtra("status", status)
//                                            .putExtra("tracking_id",id.replace("[^0-9]".toRegex(), "").toString())
//                                    )
//                                    this@ShowWebViewActivity.finish()
//                                }
//                                show()
//                            }



//                            val intent = Intent()
//                            intent.setClass(this@ShowWebViewActivity,StatusShowActivity::class.java)
//                            intent.putExtra("transStatus", status)
//                            this@ShowWebViewActivity.startActivity(intent)


//                           this@ShowWebViewActivity.startActivity(Intent(this@ShowWebViewActivity,StatusShowActivity::class.java)
//                                .putExtra("transStatus", status))
//                            finish()
                        }
                    }
                    webview.settings.javaScriptEnabled = true
                    webview.addJavascriptInterface(MyJavaScriptInterface(), "HTMLOUT")
                    webview.webViewClient = object : WebViewClient() {
                        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                            super.onPageStarted(view, url, favicon)
                        }

                        override fun onPageFinished(
                            view: WebView,
                            url: String
                        ) {
                            CommonFunctions.dismissProgress()
                            super.onPageFinished(webview, url)
                            Log.i("ProcessingUrl", "processing...  $url")
                            if (url.indexOf("/get-response") != -1) {
                                Log.i("ProcessingUrl", "inside if ")
                                webview.loadUrl("javascript:window.HTMLOUT.processHTML('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');")
                            }
                        }

                        override fun onReceivedError(
                            view: WebView,
                            errorCode: Int,
                            description: String,
                            failingUrl: String
                        ) {
                            Log.i("ProcessingUrl", "inside error ")
                            Toast.makeText(
                                this@ShowWebViewActivity,
                                "Oh no! $description",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                    /* An instance of this class will be registered as a JavaScript interface */
                    val params = StringBuffer()
                    params.append(
                        CommonMethods.addToPostParams(
                            Constants.ACCESS_CODE,
                            mainIntent!!.getStringExtra(Constants.ACCESS_CODE)
                        )
                    )
                    params.append(
                        CommonMethods.addToPostParams(
                            Constants.BILLING_NAME,
                            mainIntent!!.getStringExtra(Constants.BILLING_NAME)
                        )
                    )
                    params.append(
                        CommonMethods.addToPostParams(
                           Constants.BILLING_EMAIL,
                            mainIntent!!.getStringExtra(Constants.BILLING_EMAIL)
                        )
                    )
                    params.append(
                        CommonMethods.addToPostParams(
                            Constants.BILLING_TEL,
                            mainIntent!!.getStringExtra(Constants.BILLING_TEL)
                        )
                    )
                   // billing_name
                    params.append(
                        CommonMethods.addToPostParams(
                            Constants.MERCHANT_ID,
                            mainIntent!!.getStringExtra(Constants.MERCHANT_ID)
                        )
                    )
                    params.append(
                        CommonMethods.addToPostParams(
                            Constants.BILLING_ADDRESS,
                            mainIntent!!.getStringExtra(Constants.BILLING_ADDRESS)
                        )
                    )
                    params.append(
                        CommonMethods.addToPostParams(
                            Constants.BILLING_CITY,
                            mainIntent!!.getStringExtra(Constants.BILLING_CITY)
                        )
                    )
                    params.append(
                        CommonMethods.addToPostParams(
                            Constants.BILLING_COUNTRY,
                            "United Arab Emirates"
                        )
                    )
//
                    params.append(
                        CommonMethods.addToPostParams(
                            Constants.BILLING_ZIP,
                            mainIntent!!.getStringExtra(Constants.BILLING_ZIP)
                        )
                    )

                    params.append(
                        CommonMethods.addToPostParams(
                            Constants.ORDER_ID_FOR,
                            mainIntent!!.getStringExtra(Constants.ORDER_ID_FOR)
                        )
                    )
                    params.append(
                        CommonMethods.addToPostParams(
                            Constants.ENC_VAL,
                            URLEncoder.encode(encVal)
                        )
                    )
                    params.append(
                        CommonMethods.addToPostParams(
                            Constants.REDIRECT_URL,
                            mainIntent!!.getStringExtra(Constants.REDIRECT_URL)
                        )
                    )
                    params.append(
                        CommonMethods.addToPostParams(
                            Constants.CANCEL_URL,
                            mainIntent!!.getStringExtra(Constants.CANCEL_URL)
                        )
                    )


                    Log.i("ProcessForPayment", " custum url $params")
                    val vPostParams = params.substring(0, params.length - 1)
                    try {
                        webview.postUrl(
                            Constants.TRANS_URL,
                            EncodingUtils.getBytes(vPostParams, "UTF-8")
                        )
                    } catch (e: Exception) {
                        showToast("Exception occured while opening webview.")
                    }
                } else {
                    Log.i("ResponeHandler", "fail  ${response.errorBody()}")
                }
            }
            // }
        })
        //  RenderView().execute()
    }

    /**
     * Async task class to get json by making HTTP call
     */
    private inner class RenderView :
        AsyncTask<Void?, Void?, Void?>() {
        override fun onPreExecute() {
            super.onPreExecute()
            // Showing progress dialog
//            dialog = ProgressDialog(this@ShowWebViewActivity)
//            dialog!!.setMessage("Please wait...")
//            dialog!!.setCancelable(false)
//            dialog!!.show()
        }

//        protected override fun doInBackground(vararg arg0: Void): Void? { // Creating service handler class instance
//
//        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            // Dismiss the progress dialog
//            if (dialog!!.isShowing) dialog!!.dismiss()
//            class MyJavaScriptInterface {
//                @JavascriptInterface
//                fun processHTML(html: String) { // process the html as needed by the app
//                    var status: String? = null
//                    status = if (html.indexOf("Failure") != -1) {
//                        "Transaction Declined!"
//                    } else if (html.indexOf("Success") != -1) {
//                        "Transaction Successful!"
//                    } else if (html.indexOf("Aborted") != -1) {
//                        "Transaction Cancelled!"
//                    } else {
//                        "Status Not Known!"
//                    }
//                    //Toast.makeText(getApplicationContext(), status, Toast.LENGTH_SHORT).show();
//                    val intent = Intent(
//                        applicationContext,
//                        StatusShowActivity::class.java
//                    )
//                    intent.putExtra("transStatus", status)
//                    startActivity(intent)
//                }
//            }

            val webview =
                findViewById<View>(R.id.webview) as WebView
            webview.settings.javaScriptEnabled = true
            // webview.addJavascriptInterface(MyJavaScriptInterface(), "HTMLOUT")
            webview.webViewClient = object : WebViewClient() {
                override fun onPageFinished(
                    view: WebView,
                    url: String
                ) {
                    super.onPageFinished(webview, url)
                    if (url.indexOf("/ccavResponseHandler.jsp") != -1) {
                        webview.loadUrl("javascript:window.HTMLOUT.processHTML('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');")
                    }
                }

                override fun onReceivedError(
                    view: WebView,
                    errorCode: Int,
                    description: String,
                    failingUrl: String
                ) {
                    Toast.makeText(
                        applicationContext,
                        "Oh no! $description",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
            /* An instance of this class will be registered as a JavaScript interface */
            val params = StringBuffer()
            params.append(
                CommonMethods.addToPostParams(
                    Constants.ACCESS_CODE,
                    mainIntent!!.getStringExtra(Constants.ACCESS_CODE)
                )
            )
            params.append(
                CommonMethods.addToPostParams(
                    Constants.MERCHANT_ID,
                    mainIntent!!.getStringExtra(Constants.MERCHANT_ID)
                )
            )
            params.append(
                CommonMethods.addToPostParams(
                    Constants.ORDER_ID,
                    mainIntent!!.getStringExtra(Constants.ORDER_ID)
                )
            )
            params.append(
                CommonMethods.addToPostParams(
                    Constants.REDIRECT_URL,
                    mainIntent!!.getStringExtra(Constants.REDIRECT_URL)
                )
            )
            params.append(
                CommonMethods.addToPostParams(
                    Constants.CANCEL_URL,
                    mainIntent!!.getStringExtra(Constants.CANCEL_URL)
                )
            )
            Log.i("URLS", "params urls $params")
            params.append(
                CommonMethods.addToPostParams(
                    Constants.ENC_VAL,
                    URLEncoder.encode(encVal)
                )
            )
            val vPostParams = params.substring(0, params.length - 1)
            try {
                webview.postUrl(
                    Constants.TRANS_URL,
                    EncodingUtils.getBytes(vPostParams, "UTF-8")
                )
//                webview.loadUrl(
//                    "https://www.google.com/search?q=google&oq=&aqs=chrome.0.69i59l8.17184j0j7&sourceid=chrome&ie=UTF-8"
//                )
            } catch (e: Exception) {
                showToast("Exception occured while opening webview.")
            }
        }

        @RequiresApi(Build.VERSION_CODES.N)
        override fun doInBackground(vararg params: Void?): Void? {
            val sh = ServiceHandler()
            // Making a request to url and getting response
            val params: MutableList<NameValuePair> =
                ArrayList()
            params.add(
                BasicNameValuePair(
                    Constants.ACCESS_CODE,
                    mainIntent!!.getStringExtra(Constants.ACCESS_CODE)
                )
            )
            params.add(
                BasicNameValuePair(
                    Constants.ORDER_ID,
                    mainIntent!!.getStringExtra(Constants.ORDER_ID)
                )
            )
//            Log.i("URLS", "params  $params")
            val vResponse: String = ""
//            sh.sendGet(mainIntent!!.getStringExtra(Constants.RSA_KEY_URL),params)
            sh.makeServiceCall(
                mainIntent!!.getStringExtra(Constants.RSA_KEY_URL),
                ServiceHandler.POST,
                params
            )
            println(vResponse)


            val vEncVal = StringBuffer("")
            vEncVal.append(
                CommonMethods.addToPostParams(
                    Constants.AMOUNT,
                    "100"
                )
            )
            vEncVal.append(
                CommonMethods.addToPostParams(
                    Constants.CURRENCY,
                    "AED"
                )
            )
            Log.i("StringAppend", "$vEncVal")
            encVal = RSAUtility.encrypt(
                vEncVal.substring(
                    0,
                    vEncVal.length - 1
                ), "AIzaSyAj_6VbtdwguLPzQ7ZnAfsoOJjnqcSuipM"
            )




            Log.i("StringAppend", "$encVal")
            if (CommonMethods.chkNull(vResponse) != ""
                && CommonMethods.chkNull(vResponse).toString().indexOf(
                    "ERROR"
                ) == -1
            ) {
                val vEncVal = StringBuffer("")
                vEncVal.append(
                    CommonMethods.addToPostParams(
                        Constants.AMOUNT,
                        "100"
                    )
                )
                vEncVal.append(
                    CommonMethods.addToPostParams(
                        Constants.CURRENCY,
                        "AED"
                    )
                )
                Log.i("StringAppend", "$vEncVal")
                encVal = RSAUtility.encrypt(
                    vEncVal.substring(
                        0,
                        vEncVal.length - 1
                    ), "AIzaSyAj_6VbtdwguLPzQ7ZnAfsoOJjnqcSuipM"
                )
            }
            return null
        }
    }

    fun showToast(msg: String) {
        Toast.makeText(this, "Toast: $msg", Toast.LENGTH_LONG).show()
    }

    //    inner class MyJavaScriptInterface {
//        @JavascriptInterface
//        fun processHTML(html: String) { // process the html as needed by the app
//            var status: String? = null
//            status = if (html.indexOf("Failure") != -1) {
//                "Transaction Declined!"
//            } else if (html.indexOf("Success") != -1) {
//                "Transaction Successful!"
//            } else if (html.indexOf("Aborted") != -1) {
//                "Transaction Cancelled!"
//            } else {
//                "Status Not Known!"
//            }
//            //Toast.makeText(getApplicationContext(), status, Toast.LENGTH_SHORT).show();
//            val intent = Intent(
//                applicationContext,
//                StatusShowActivity::class.java
//            )
//            intent.putExtra("transStatus", status)
//            startActivity(intent)
//        }
//       // @JavascriptInterface
//
//    }
    fun stripNonDigits(
        input: CharSequence /* inspired by seh's comment */
    ): String? {
        val sb = StringBuilder(
            input.length /* also inspired by seh's comment */
        )
        for (i in 0 until input.length) {
            val c = input[i]
            if (c.toInt() > 47 && c.toInt() < 58) {
                sb.append(c)
            }
        }
        return sb.toString()
    }
}