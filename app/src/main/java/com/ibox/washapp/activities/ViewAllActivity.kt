package com.ibox.washapp.activities

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.ibox.washapp.R
import com.ibox.washapp.adapters.ReviewAdapter
import com.ibox.washapp.model.Detail
import com.ibox.washapp.utils.PaginationScrollListener
import com.ibox.washapp.viewModels.ReviewsViewsModel
import kotlinx.android.synthetic.main.activity_view_all.*

class ViewAllActivity : AppCompatActivity() {

    private var laundryViewModel = ReviewsViewsModel()
    val layoutManger = LinearLayoutManager(this)
    var detailList = mutableListOf<Detail>()
    var detailList2 = mutableListOf<Detail>()

    private var page = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_all)
        setupToolbar()
        intialize()
    }

    private var adapter: ReviewAdapter? = null
    private fun intialize() {

        rvReviews.layoutManager = layoutManger

        swipeRefresh.setOnRefreshListener {
            val map = HashMap<String, Int>()
            map["laundry_id"] = intent.getIntExtra("ShopID", 0)
            map["limit"] = 100
            val laundryId = intent.getIntExtra("ShopID", 0)
            page = 0
            laundryViewModel.laundryReviwList(this, laundryId, false, page)
        }

        laundryViewModel = ViewModelProviders.of(this).get(ReviewsViewsModel::class.java)
        laundryViewModel.reviwesMutableLiveData.observe(this, Observer {
            isLoading = false
            pbBarReviews.visibility = View.GONE
            swipeRefresh.isRefreshing = false
          //  Log.i("PAGINATION", "" + Gson().toJson(detailList))
            if (page == 0) {
                detailList = it.detail as MutableList<Detail>
                adapter = ReviewAdapter(it.detail as MutableList<Detail>, this)
                rvReviews.adapter = adapter
            } else {

//                detailList.addAll(it.detail)
//                adapter!!.notifyDataSetChanged()
                adapter!!.notifyData(it.detail as MutableList<Detail>)
            }
            noData.visibility = if (detailList.size > 0) View.GONE else View.VISIBLE
        })

        val map = HashMap<String, Int>()
        val laundryId = intent.getIntExtra("ShopID", 0)
        Log.i("LAUNDRY_ID", "" + laundryId)
        map["laundry_id"] = laundryId
        map["limit"] = 100
        page = 0
        laundryViewModel.laundryReviwList(this, laundryId, true, page)
        setScrollListener(layoutManger, laundryId)
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.back_arrow)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private var isLoading = false
    private fun setScrollListener(layoutManger: LinearLayoutManager, la: Int) {

        rvReviews.addOnScrollListener(object : PaginationScrollListener(layoutManger) {
            override fun isLastPage(): Boolean {
                return false
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                page += 1
                isLoading = true
                pbBarReviews.visibility = View.VISIBLE
                laundryViewModel.laundryReviwList(this@ViewAllActivity, la, false, page)
            }
        })
    }

}