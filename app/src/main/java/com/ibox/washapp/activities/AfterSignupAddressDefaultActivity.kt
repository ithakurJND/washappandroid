package com.ibox.washapp.activities

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ListPopupWindow
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.Gson
import com.ibox.washapp.R
import com.ibox.washapp.dialog.ShowLocations
import com.ibox.washapp.model.AddAddress
import com.ibox.washapp.model.Address
import com.ibox.washapp.model.Area
import com.ibox.washapp.model.Locations
import com.ibox.washapp.utils.CommonMethods
import com.ibox.washapp.utils.Constants
import com.ibox.washapp.utils.Prefs
import com.ibox.washapp.viewModels.AddressViewModel
import com.ibox.washapp.viewModels.CommonViewModel
import kotlinx.android.synthetic.main.fragment_after_signup_addressdetails.*
import kotlinx.android.synthetic.main.fragment_after_signup_addressdetails.etSelectLocation
import kotlinx.android.synthetic.main.fragment_after_signup_addressdetails.ivBackArrow
import kotlinx.android.synthetic.main.fragment_laundry_list.*
import java.lang.Exception


class AfterSignupAddressDefaultActivity : AppCompatActivity(), OnMapReadyCallback,
    View.OnClickListener {
    private var mMap: GoogleMap? = null
    var commonViewModel = CommonViewModel()
    var addressViewModel = AddressViewModel()
    lateinit var listPopupWindow: ListPopupWindow
    var positionArrayList = ArrayList<String>()
    var positionArrayListLatNlg = ArrayList<Area>()
    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.tvSkipStep -> {
                finishAffinity()
                startActivity(Intent(this, MainActivity::class.java))

            }
            R.id.ivBackArrow -> {
                onBackPressed()
            }

            R.id.btnSaveContinue -> {
//                if (etAddressName.text.trim().isEmpty()
//                    or etSelectStreet.text.trim().isEmpty()
//                    or etSelectLocation.text.trim().isEmpty() or etFlat.text.trim().isEmpty()
//                )

                if (etAddressName.text.trim().isEmpty())
                    Toast.makeText(this, "Address Name is required", Toast.LENGTH_LONG).show()
                else if (etSelectStreet.text.trim().isEmpty())
                    Toast.makeText(this, "Street Address is required", Toast.LENGTH_LONG).show()
                else if (etFlat.text.trim().isEmpty())
                    Toast.makeText(this, "Flat no. is required", Toast.LENGTH_LONG).show()
                else if (etSelectLocation.text.trim().isEmpty())
                    Toast.makeText(this, "Location is required", Toast.LENGTH_LONG).show()
                else {
                    if (isEdit) {
                        val address = AddAddress(
                            addressId, etSelectStreet.text.toString().trim(),
                            area_id,
                            etBuilding.text.toString().trim(),
                            etSelectLocation.text.toString().trim(),
                            emirate_id,
                            etFlat.text.toString().trim(),
                            if (isDefault.isChecked) 1 else 0,
                            lat,
                            lng,
                            etPostalCode.text.toString().trim(),
                            etAddressName.text.toString().trim()
                            , userId
                        )
                        addressViewModel.editAddress(address, this)
                    } else {
                        val address = AddAddress(
                            0, etSelectStreet.text.toString(),
                            area_id,
                            etBuilding.text.toString(),
                            etSelectLocation.text.toString(),
                            emirate_id,
                            etFlat.text.toString(),
                            if (isDefault.isChecked) 1 else 0,
                            lat,
                            lng,
                            etPostalCode.text.toString(),
                            etAddressName.text.toString()
                            , 0
                        )
                        addressViewModel.addAddress(address, this)
                    }
                }
//                startActivity(Intent(activity, MainActivity::class.java))
//                activity!!.finish()
            }

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_after_signup_addressdetails)
        intialize()
    }

    private var isEdit = false
    private fun intialize() {
        if (intent.hasExtra("isFromAddress") || intent.hasExtra("isFromPickup")) {
            isDefault.isEnabled = true
            if (!intent.hasExtra("data")) {
                isDefault.isChecked = false
                btnSaveContinue.text = "Save"
                tvTitle.text = "Add New Address"
            } else {
                btnSaveContinue.text = "Update"
                tvTitle.text = "Edit Address"
            }
            ivBackArrow.visibility = View.VISIBLE
            tvSkipStep.visibility = View.INVISIBLE

        } else {
            ivBackArrow.visibility = View.GONE
            tvSkipStep.visibility = View.VISIBLE
            btnSaveContinue.text = "Save & Continue"
            tvTitle.text = "Address Details"
        }


        (map as SupportMapFragment).getMapAsync(this)
        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel::class.java)
        commonViewModel.getLocations().observe(this, Observer { locations ->
            if (intent.hasExtra("isFromPickup")) {
                val areid = com.ibox.washapp.utils.Prefs.with(this).getInt(Constants.AREA_ID, 0)
                val emiraid = com.ibox.washapp.utils.Prefs.with(this).getInt(Constants.EMIRAT_ID, 0)
                for (i in locations.areas.indices) {
                    val area = locations.areas[i].area
                    val emirate = locations.areas[i].emirate
                    if (areid == locations.areas[i].id && emiraid == locations.areas[i].emirate_id) {
                        positionArrayList.add("$area, $emirate")
                        positionArrayListLatNlg.add(locations.areas[i])
                        area_id = areid
                        emirate_id = emiraid
                        try {
                            lat = positionArrayListLatNlg[0].lat
                            lng = positionArrayListLatNlg[0].lng
                            moveMao(
                                LatLng(
                                    positionArrayListLatNlg[0].lat,
                                    positionArrayListLatNlg[0].lng
                                )
                            )
                            etSelectLocation.setText(positionArrayList[0])
                            etSelectLocation.isEnabled = false
                    }
                        catch (e:Exception)
                        {

                        }

                }
                    else{
                        area_id = areid
                        emirate_id = emiraid
                        try {
                            lat =Prefs.with(this).getString(Constants.LAT2,"").toDouble()
                            lng = Prefs.with(this).getString(Constants.LNG2,"").toDouble()
                            moveMao(
                                LatLng(
                                    lat ,
                                    lng
                                )
                            )
                            etSelectLocation.setText(Prefs.with(this).getString(Constants.TEXT,""))
                            etSelectLocation.isEnabled = false
                        }
                        catch (e:Exception)
                        {

                        }

                    }

                }



            } else {
                for (i in locations.areas.indices) {

                    val area = locations.areas[i].area
                    val emirate = locations.areas[i].emirate
                    positionArrayList.add("$area, $emirate")
                }
                positionArrayListLatNlg.addAll(locations.areas)
            }

            etSelectLocation.setOnClickListener()
            {
//                listPopUp(positionArrayList, etSelectLocation, positionArrayListLatNlg)
//                listPopupWindow.show()
                var showLocations = ShowLocations()
                val bundle = Bundle()
                bundle.putBoolean("for",intent.hasExtra("isFromPickup"))
                showLocations.arguments = bundle
                CommonMethods.showDialogFragmentWithoutAnimation(this,showLocations)
            }

        })
        addressViewModel.addressResponse().observe(this, Observer {
            if (isDefault.isEnabled) {
                finish()
            } else {
                finishAffinity()
                startActivity(Intent(this, MainActivity::class.java))
            }
        })
        btnSaveContinue.setOnClickListener(this)
        tvSkipStep.setOnClickListener(this)
        ivBackArrow.setOnClickListener(this)
        commonViewModel.locations(this, "", 0, 20)
        registerReceiver(selectedLoc, IntentFilter("SelectedLoc"))
    }


    private var area_id = 0
    private var emirate_id = 0
    private var lat = 0.0
    private var lng = 0.0
    var selectedLoc = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
//            Log.i("Selected", "ksdjkfjkj  " + intent!!.getIntExtra("emirateId", 0))
//            Log.i("Selected", "ksdjkfjkj  " + intent!!.getBooleanExtra("for",false))
//            if (intent!!.getBooleanExtra("for",false))
            area_id = intent!!.getIntExtra("areaId", 0)
            emirate_id = intent!!.getIntExtra("emirateId", 0)
            lat = intent!!.getDoubleExtra("lat", 0.0)
            lng = intent!!.getDoubleExtra("lng", 0.0)
//            Log.i("ShowLatLan", "on activity $lat $lng")
//            Log.i("ShowLatLan", "on activity area  ${intent?.getStringExtra("area")} ${intent?.getStringExtra("emirate")}")
           etSelectLocation.setText(intent?.getStringExtra("area")+","+intent?.getStringExtra("emirate"))
            moveMao(LatLng(lat, lng))

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(selectedLoc)
    }


    fun listPopUp(
        arrayList: ArrayList<String>,
        view: TextView,
        positionArrayListLatNlg: ArrayList<Area>
    ) {
        listPopupWindow = ListPopupWindow(this)
        listPopupWindow.setAdapter(
            ArrayAdapter<String>(this, R.layout.item_popup_list, arrayList)
        )
        listPopupWindow.setOnItemClickListener { p0, p1, position, p3 ->
            view.text = positionArrayList[position]
            area_id = positionArrayListLatNlg[position].id
            emirate_id = positionArrayListLatNlg[position].emirate_id
            lat = positionArrayListLatNlg[position].lat
            lng = positionArrayListLatNlg[position].lng

            listPopupWindow.dismiss()
            moveMao(
                LatLng(
                    positionArrayListLatNlg[position].lat,
                    positionArrayListLatNlg[position].lng
                )
            )
        }
        listPopupWindow.isModal = true
        listPopupWindow.anchorView = view
    }


    private var addressId = 0
    private var userId = 0
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap!!.uiSettings.setAllGesturesEnabled(false)
        // Add a marker in Sydney, Australia, and move the camera.
        val sydney = LatLng((25.2048).toDouble(), 55.2708)
        moveMao(sydney)
        if (intent.hasExtra("data")) {
            isEdit = true

            tvSkipStep.visibility = View.INVISIBLE
            val data = Gson().fromJson(intent.getStringExtra("data"), Address::class.java)
            isDefault.setOnCheckedChangeListener { compoundButton, b ->
                if (data.is_default == 1) {
                    Toast.makeText(
                        this,
                        "Please check other address as default",
                        Toast.LENGTH_SHORT
                    ).show()
                    isDefault.isChecked = true
                }
            }

            addressId = data.id
            userId = data.user_id
            etAddressName.setText(data.title.toString())
            etSelectStreet.setText(data.address.toString())
            etFlat.setText(data.flat_no.toString())
            etBuilding.setText(data.building_name.toString())
            isDefault.isChecked = data.is_default == 1
            etPostalCode.setText(data.postal_code.toString())
            etSelectLocation.setText(data.city.toString())
            area_id = data.area_id
            emirate_id = data.emirate_id
            moveMao(LatLng(data.lat.toDouble(), data.lng.toDouble()))
        }
    }

    private fun moveMao(sydney: LatLng) {
        mMap!!.addMarker(MarkerOptions().position(sydney))
        mMap!!.animateCamera(
            CameraUpdateFactory.newLatLngZoom(
                sydney, 12.0f
            )
        )
    }


}