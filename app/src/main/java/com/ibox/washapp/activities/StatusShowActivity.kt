package com.ibox.washapp.activities

import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.ibox.washapp.R
import com.ibox.washapp.fragments.ConfirmationFragment
import com.ibox.washapp.utils.CommonMethods

class StatusShowActivity:AppCompatActivity() {
    public override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
        setContentView(R.layout.activity_status)
        val mainIntent = intent
        val tv4 = findViewById<View>(R.id.textView1) as TextView
        tv4.text = mainIntent.getStringExtra("transStatus")
        if (mainIntent.getStringExtra("transStatus")=="Transaction Successful!")
        {
            CommonMethods.addFragment(supportFragmentManager,ConfirmationFragment())
        }
        else
            AlertDialog.Builder(this).apply {
                                                setTitle("Payment")
                                //   setCancelable(false)
                                setMessage(mainIntent.getStringExtra("transStatus"))
                                setPositiveButton("OK") { dialog, _ ->
                                    dialog.dismiss()
                                }
                                // setNegativeButton("NO") { dialog, _ -> dialog.dismiss() }
                                show()
                            }
    }

    fun showToast(msg: String) {
        Toast.makeText(this, "Toast: $msg", Toast.LENGTH_LONG).show()
    }
}