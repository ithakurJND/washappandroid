package com.ibox.washapp.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Patterns
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.ibox.washapp.R
import com.techautovity.myeve.retrofit.RestClient
import com.ibox.washapp.utils.CommonFunctions
import com.ibox.washapp.utils.Constants
import kotlinx.android.synthetic.main.activity_contact_us.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ContactUsActivity : AppCompatActivity(), View.OnClickListener {

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.cvWhatsApp -> {
                val url = "https://api.whatsapp.com/send?phone=+971551222252"
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            }
            R.id.cvPhone -> {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse("tel:+971551222252")
                startActivity(intent)
            }
            R.id.btnSaveContinue -> {
                if (etFlat.text.isEmpty()) {
                    etFlat.error = "Please enter name"
                    etFlat.requestFocus()
                } else if (etBuilding.text.isEmpty()) {
                    etBuilding.error = "Please enter phone"
                    etBuilding.requestFocus()
                } else if (etPostalCode.text.isEmpty()) {
                    etPostalCode.error = "Please enter email"
                    etPostalCode.requestFocus()
                } else if (!Patterns.EMAIL_ADDRESS.matcher(etPostalCode.text).matches()) {
                    etPostalCode.error = "Please enter valid email"
                    etPostalCode.requestFocus()
                } else if (etSelectLocation.text.isEmpty()) {
                    etSelectLocation.error = "Please enter message"
                    etSelectLocation.requestFocus()
                } else {
                    apiHit()
                }
            }
        }
    }

    private fun apiHit() {
        CommonFunctions.showProgress(this)
        val map = HashMap<String, Any>()
        map["name"] = etFlat.text.toString()
        map["email"] = etPostalCode.text.toString()
        map["phone_no"] = etBuilding.text.toString()
        map["message"] = etSelectLocation.text.toString()

        RestClient.modalApiService.contactUs(
            com.ibox.washapp.utils.Prefs.with(this)
                .getString(Constants.PREFS_TOKEN, ""), map
        ).enqueue(object : Callback<com.ibox.washapp.model.PojoSuccess> {
            override fun onFailure(call: Call<com.ibox.washapp.model.PojoSuccess>, t: Throwable) {
                CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(this@ContactUsActivity, t.message!!)
            }

            override fun onResponse(call: Call<com.ibox.washapp.model.PojoSuccess>, response: Response<com.ibox.washapp.model.PojoSuccess>) {
                CommonFunctions.dismissProgress()
                if (response.isSuccessful) {
                    Toast.makeText(
                        this@ContactUsActivity,
                        response.body()!!.message,
                        Toast.LENGTH_SHORT
                    ).show()
                    finish()
                } else {
                    CommonFunctions.handleError(
                        this@ContactUsActivity,
                        response.errorBody(),
                        response.code()
                    )
                }
            }
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_us)
        setupToolbar()
        setListner()
    }

    private fun setListner() {
        cvWhatsApp.setOnClickListener(this)
        cvPhone.setOnClickListener(this)
        btnSaveContinue.setOnClickListener(this)
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.back_arrow)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

}