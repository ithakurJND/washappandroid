package com.ibox.washapp.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.ibox.washapp.R
import com.ibox.washapp.fragments.IntroFragment
import com.ibox.washapp.model.User
import com.ibox.washapp.utils.CommonMethods
import com.ibox.washapp.utils.Constants

class SplashActivity : AppCompatActivity() {
   // var commonViewModel = CommonViewModel()
    lateinit var user: User
    override fun onCreate(savedInstanceState: Bundle?) {
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
//        if (intent.extras != null) {
//            val value = intent.extras!!.getString("order_id")
//            Log.d("pushData", " Value: $value")
////            for (key in intent.extras!!.keySet()) {
//////                val value = intent.extras!!.getString("order_id")
//////                Log.d("pushData", "Key: $key Value: $value")
////            }
//        }
        Handler().postDelayed({
            if (com.ibox.washapp.utils.Prefs.with(this).getString(Constants.PREFS_TOKEN, "") != "") {
                finishAffinity()
                user = com.ibox.washapp.utils.Prefs.with(this).getObject(Constants.USER_DATA, User::class.java)

                if (user.phone_no == null || user.is_phone_verified == false) {
                    startActivity(Intent(this, PhoneNumberActivity::class.java))
                } else {
                    if (intent.extras != null && intent.extras!!.getString("order_id") != null) {
                        val value = intent.extras!!.getString("order_id")
                        Log.d("pushData", " Value: $value")
                        startActivity(
                            Intent(this, com.ibox.washapp.activities.OrderDetailActivity::class.java)
                                .putExtra("id",  intent.extras!!.getString("order_id")!!.toInt())
                                .putExtra("isFrom", "PushThroughSplash")
                        )
                        intent.extras!!.clear()
                    } else
                        startActivity(Intent(this, com.ibox.washapp.activities.MainActivity::class.java))
                }

            } else
                CommonMethods.addFragment(
                    supportFragmentManager,
                    IntroFragment()
                )
        }, 1500)

    }

    override fun onBackPressed() {
//        val f =
//            supportFragmentManager.findFragmentById(R.id.splashFrame)!!
//        if (f is IntroFragment)
//        {
//            supportFragmentManager.popBackStackImmediate(null,FragmentManager.POP_BACK_STACK_INCLUSIVE)
//        }
//        else
        super.onBackPressed()
    }
}