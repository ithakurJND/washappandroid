package com.ibox.washapp.activities

import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.facebook.GraphResponse
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.firebase.iid.FirebaseInstanceId
import com.ibox.washapp.R
import com.ibox.washapp.model.UserLogin
import com.ibox.washapp.viewModels.CommonViewModel
import kotlinx.android.synthetic.main.fragment_signup.*
import org.json.JSONException
import org.json.JSONObject

class SignupActivity : AppCompatActivity(), View.OnClickListener,
    com.ibox.washapp.utils.facebook.FacebookLoginListener,
    GoogleApiClient.OnConnectionFailedListener {
    lateinit var commonViewModel: CommonViewModel
    private var facebookLogin: com.ibox.washapp.utils.facebook.FacebookLogin? = null
    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.btnWithEmail -> {
                startActivity(Intent(this, com.ibox.washapp.activities.EmailSignUpActivity::class.java))
            }
            R.id.btnWithFaceBook -> {
                facebookLogin!!.performLogin()
            }
            R.id.btnWithGoogle -> {
                signInWithGoogle()
                //withGoogle()
            }
        }
    }
    private val RC_SIGN_IN = 1021
    private var mGoogleApiClient: GoogleApiClient? = null
    private fun signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback {
            // ...
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_signup)
        intialize()
    }

    override fun onFbLoginCancel() {
    }

    override fun onFbLoginError() {
    }


    override fun onFbLoginSuccess() {
        facebookLogin?.getUserProfile()

    }


    override fun onGetprofileSuccess(`object`: JSONObject?, response: GraphResponse?) {
        try {
            apiHit(`object`)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

    }

    private fun apiHit(jsonObject: JSONObject?) {
        val map = java.util.HashMap<String, String>()
        val id = jsonObject?.getString("id")!!
        map["fb_id"] = id
        map["name"] = jsonObject.getString("first_name")+" "+ jsonObject.getString("last_name")
        if (jsonObject.has("email")) {
            map["email"] = jsonObject.getString("email")
        } else {
            map["email"] = "$id@facbook.com"
        }
        map["profile_image"] =  "http://graph.facebook.com/"+id+"/picture?type=square"

        commonViewModel.userLogIn(
            FirebaseInstanceId.getInstance().token!!,1
           ,map["email"]!!,"",map["fb_id"]!!,"",map["name"]!!,this)
    }

    private fun gmailLogin(acct: GoogleSignInAccount?) {
        var name = ""
        when {
            acct!!.displayName != null -> name = acct.displayName!!
            acct.givenName != null -> name = acct.givenName!!
            else -> name = acct.email!!
        }
        val map = java.util.HashMap<String, String>()
        val id = acct.id!!
        map["google_id"] = id
        map["email"] = acct.email.toString()
        map["name"] = name
        map["profile_image"] = acct.photoUrl.toString()

        commonViewModel.userLogIn(
            FirebaseInstanceId.getInstance().token!!,1
            ,map["email"]!!,"","",id,map["name"]!!,this)

    }

        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        facebookLogin?.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            RC_SIGN_IN -> {
                val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
                handleSignInResult(result)
            }
        }
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Toast.makeText(this, connectionResult.errorMessage, Toast.LENGTH_SHORT).show()
    }
    private fun signInWithGoogle() {
        val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    private fun handleSignInResult(result: GoogleSignInResult) {
        if (result.isSuccess) {
            // Signed in successfully, show authenticated UI.
            val acct = result.signInAccount
            gmailLogin(acct)
            // mStatusTextView.setText(getString(R.string.signed_in_fmt, acct.getDisplayName()));
            //updateUI(true);
        }
    }

    private fun intialize() {
        googleIntialize()
        facebookLogin = com.ibox.washapp.utils.facebook.FacebookLogin(this, this)
        facebookLogin?.setFacebookLoginListener(this)
        tvAlreadyAccount.movementMethod = LinkMovementMethod.getInstance()
        tvAlreadyAccount.append("Already have an account? ")
        tvAlreadyAccount.append(spanString("LOGIN"))

        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel::class.java)
        commonViewModel.userResponse().observe(this, object : Observer<UserLogin> {
            override fun onChanged(t: UserLogin?) {
                if (t!!.errorMessage != null) {
                    Toast.makeText(this@SignupActivity, t.errorMessage, Toast.LENGTH_SHORT)
                        .show()
                } else if (t.user!!.is_phone_verified == true) {
                    finishAffinity()
                    startActivity(Intent(this@SignupActivity, com.ibox.washapp.activities.MainActivity::class.java))
                } else {

                    startActivity(Intent(this@SignupActivity,PhoneNumberActivity::class.java)
                        .putExtra("Change","FromGoogleOrFace"))
                }
            }
        })
        btnWithEmail.setOnClickListener(this)
        btnWithFaceBook.setOnClickListener(this)
        btnWithGoogle.setOnClickListener(this)
    }

    private fun googleIntialize() {
        try {
            val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()
            if (mGoogleApiClient == null) {
                mGoogleApiClient = GoogleApiClient.Builder(this)
                    .enableAutoManage(this, this)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .addConnectionCallbacks(object : GoogleApiClient.ConnectionCallbacks {
                        override fun onConnected(bundle: Bundle?) {
                            signOut()
                        }

                        override fun onConnectionSuspended(i: Int) {

                        }
                    })
                    .build()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun spanString(message: String): SpannableString {
        val ss = SpannableString(message)
        val span1 = object : ClickableSpan() {
            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false
            }
            override fun onClick(v: View) {
                onBackPressed()
            }
        }
        ss.setSpan(
            StyleSpan(Typeface.BOLD),
            0,
            ss.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        ss.setSpan(
            ForegroundColorSpan(Color.WHITE),
            0,
            ss.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        ss.setSpan(span1, 0, ss.length, 0)
        return ss
    }
}