package com.ibox.washapp.fragments

import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import com.ibox.washapp.R
import kotlinx.android.synthetic.main.fragment_confirmation.*

/* fragment used after you complete your payment */
class ConfirmationFragment : BaseFragment() {
    override fun setLayoutToInsert(): Int {
        return R.layout.fragment_confirmation
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        tvHOme.paintFlags = Paint.UNDERLINE_TEXT_FLAG
        tvHOme.setOnClickListener {
            activity!!.finishAffinity()
            startActivity(Intent(activity, com.ibox.washapp.activities.MainActivity::class.java))
        }
        btnNewOrder.setOnClickListener()
        {
            val intent = Intent(activity!!, com.ibox.washapp.activities.MainActivity::class.java)
            intent.putExtra("Selected","BottomSheet")
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
            activity!!.finish()
        }
        btnViewOrder.setOnClickListener()
        {
            startActivity(Intent(activity,
                com.ibox.washapp.activities.OrderDetailActivity::class.java)
                .putExtra("id",arguments!!.getInt("id")))
        }
    }
}