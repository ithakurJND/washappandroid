package com.ibox.washapp.fragments

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ListPopupWindow
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.ibox.washapp.R
import com.ibox.washapp.activities.AfterSignupAddressDefaultActivity
import com.ibox.washapp.adapters.AddedToCartAdapter
import com.ibox.washapp.adapters.CalenderAdapter
import com.ibox.washapp.model.*
import com.ibox.washapp.utils.CommonFunctions
import com.ibox.washapp.utils.CommonMethods
import com.ibox.washapp.utils.Constants
import com.ibox.washapp.utils.MarginItemDecoration
import com.ibox.washapp.viewModels.CommonViewModel
import com.ibox.washapp.viewModels.LaundryViewModel
import kotlinx.android.synthetic.main.fragment_pick_and_delivery.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


/*
* This screen is to set date/day/month on horizontal calender
* and to get the pick up and drop off address details
* user can add new address
 */

class PickAndDeliveryFragment : BaseFragment(), View.OnClickListener {
    private val arrayListCalender = ArrayList<Calender>()
    private val arrayListCalenderDrop = ArrayList<Calender>()
    private val arrayListAddToCart = ArrayList<com.ibox.washapp.model.Product>()
    var noAddress = false
    private var arrayList2 = ArrayList<OrderItem>()
    private var commonViewModel = CommonViewModel()
    private var laundryViewModel = LaundryViewModel()
    lateinit var listPopupWindow: ListPopupWindow
    private var isClicked = false
    private var change = false

    private var positionsArrayPickup: ArrayList<String> = arrayListOf()
    private var positionsArrayDelivery: ArrayList<String> = arrayListOf()
    private var positionsArrayForAddress: ArrayList<String> = arrayListOf()
    private var positionsArrayForAddressN: ArrayList<Address> = arrayListOf()


    private var positionsArrayForAddressDrop: ArrayList<String> = arrayListOf()
    private var positionsArrayForAddressNDrop: ArrayList<Address> = arrayListOf()
    var pickUpTime = ""
    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ivBackArrowPickAndDelivery -> {
                activity!!.onBackPressed()
            }
            R.id.btnContinue -> {
                when {
                    etSelectTimePickup.text.isEmpty() -> {
                        Toast.makeText(activity, "Please select pickup time", Toast.LENGTH_SHORT)
                            .show()
                    }
                    etSelectAddressPickup.text.isEmpty() -> {
                        Toast.makeText(activity, "Please select pickup address", Toast.LENGTH_SHORT)
                            .show()
                    }
                    etDropOffSelectTime.text.isEmpty() -> {
                        Toast.makeText(activity, "Please select dropoff time", Toast.LENGTH_SHORT)
                            .show()
                    }
                    etDropOffAddress.text.isEmpty() -> {
                        Toast.makeText(
                            activity,
                            "Please select dropoff address",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    else -> {
                        com.ibox.washapp.utils.Prefs.with(activity)
                            .save(Constants.PICK_TIME, etSelectTimePickup.text.toString())
                        com.ibox.washapp.utils.Prefs.with(activity)
                            .save(Constants.PICK_ADDRESS, etSelectAddressPickup.text.toString())
                        com.ibox.washapp.utils.Prefs.with(activity)
                            .save(Constants.DROP_TIME, etDropOffSelectTime.text.toString())
                        com.ibox.washapp.utils.Prefs.with(activity)
                            .save(Constants.DROP_ADDRESS, etDropOffAddress.text.toString())
                        editOrder()
                    }
                }
            }
            R.id.rlAddedToCart -> {
                if (rvAddedToCart.visibility == View.GONE) {
                    rvAddedToCart.visibility = View.VISIBLE
                    ivUpArrow.visibility = View.GONE
                    ivDropDown.visibility = View.VISIBLE
                } else {
                    rvAddedToCart.visibility = View.GONE
                    ivUpArrow.visibility = View.VISIBLE
                    ivDropDown.visibility = View.GONE
                }
            }
            R.id.etSelectTimePickup -> {
                if (pickupCalenderAdapter!!.getSelectedIndex() == 0) {
                    val day = CommonFunctions.getCurrentDate()
                    var indes = -1
                    for (sa in positionsArrayDelivery) {
                        Log.i("ShowMe", "SA " + sa[0])
                        var da = sa.split("-")[0].substring(0, 2).trim().toInt()
                        Log.i("ShowMe", "DA " + da)
                        val saam = sa.split("-")[0].substring(
                            sa.split("-")[0].length - 3,
                            sa.split("-")[0].length - 1
                        )
                        Log.i("ShowMe", "saam " + saam)
                        if (saam == "PM") {
                            da += 12
                        }
                        da = da + 1
                        if (da >= day!!) {
                            indes = positionsArrayDelivery.indexOf(sa) + 1
                            break
                        }
                    }
                    positionsArrayPickup.clear()
                    if (indes > -1) {
                        positionsArrayPickup.addAll(
                            positionsArrayDelivery.subList(
                                indes,
                                positionsArrayDelivery.size
                            )
                        )
                    }
                    if (positionsArrayPickup.size == 0) {
                        Toast.makeText(
                            activity, "No Time slot available please change date."
                            , Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        etSelectTimePickup.setText("")
                        listPopUp(positionsArrayPickup, etSelectTimePickup)
                        listPopupWindow.show()
                    }
                } else {
                    listPopUp(positionsArrayDelivery, etSelectTimePickup)
                    listPopupWindow.show()
                }
            }
            R.id.etSelectAddressPickup -> {
                if (noAddress)
                    Toast.makeText(activity, "Add Address", Toast.LENGTH_LONG).show()
                    else {
                    listPopUpNew(positionsArrayForAddress, etSelectAddressPickup, true)
                    listPopupWindow.show()
                }
            }
            R.id.etDropOffSelectTime -> {
                    listPopUp(positionsArrayDelivery, etDropOffSelectTime)
                    listPopupWindow.show()
            }
            R.id.etDropOffAddress -> {
                if (noAddress)
                    Toast.makeText(activity, "Add Address", Toast.LENGTH_LONG).show()
                else {
                    listPopUpNew(positionsArrayForAddressDrop, etDropOffAddress, false)
                    listPopupWindow.show()
                }
            }
            R.id.btnAddNewPickup -> {
                btnAddNewPickup.isClickable = false
                startActivity(
                    Intent(activity, AfterSignupAddressDefaultActivity::class.java)
                        .putExtra("isFromPickup", true)
                )
                Handler().postDelayed(Runnable {
                    btnAddNewPickup.isClickable = true
                }, 1000)
            }
            R.id.btnAddNewDelivery -> {
                btnAddNewDelivery.isClickable = false
                startActivity(
                    Intent(activity, AfterSignupAddressDefaultActivity::class.java)
                        .putExtra("isFromPickup", true)
                )
                Handler().postDelayed(Runnable {
                    btnAddNewDelivery.isClickable = true
                }, 1000)


            }

        }
    }

    private fun editOrder() {
        val pickAdd = com.ibox.washapp.utils.Prefs.with(activity).getString(Constants.PICKADD, "")
        val pickAddId = com.ibox.washapp.utils.Prefs.with(activity).getInt(Constants.PICKADDID, 0)
        val dropAdd = com.ibox.washapp.utils.Prefs.with(activity).getString(Constants.DROPADD, "")
        val dropAddId = com.ibox.washapp.utils.Prefs.with(activity).getInt(Constants.DROPADDID, 0)
        val map = HashMap<String, Any>()
        Log.i("Order_id", "" + arguments!!.getInt("id"))
        map["pickup_date"] = pickupCalenderAdapter!!.getSelectedDate()
        map["drop_off_date"] = deliveryCalenderAdapter!!.getSelectedDate()
        map["pickup_time_slot"] = etSelectTimePickup.text.toString()
        map["drop_off_time_slot"] = etDropOffSelectTime.text.toString()
        if (pickAdd != null && dropAdd != null) {
            map["pickup_address"] = pickAdd
            map["pickup_address_id"] = pickAddId
            map["dropoff_address"] = dropAdd
            map["drop_off_address_id"] = dropAddId
        } else {
            map["pickup_address"] = pickUPAddress
            map["pickup_address_id"] = pickUPAddressId
            map["dropoff_address"] = deliveryAddress
            map["drop_off_address_id"] = deliveryAddressId
        }

        if (arguments != null && arguments!!.getString("isFromInComplete") == "isFromInComplete") {
            val data = com.ibox.washapp.utils.Prefs.with(context)
                .getObject(Constants.ORDER_ITEMS_COMP_ORDER, com.ibox.washapp.model.PojoOrderDetails::class.java)
            map["order_id"] = data.id

            Log.i("order_id", "" + data.id)
        } else {
            map["order_id"] = arguments!!.getInt("id")
        }
        if (arguments != null && arguments!!.containsKey("isFromBasket")
            && arguments!!.getBoolean("isFromBasket")
        ) {
            map["laundry_id"] = com.ibox.washapp.utils.Prefs.with(context).getInt(Constants.LAUNDRY_ID, 0)
            Log.i("UpUp", "Basket")

            val data2 = com.ibox.washapp.utils.Prefs.with(context)
                .getObject(Constants.NEW_RESPONSE_USER_DATA, OrderResponeLaundry::class.java)
            val list: MutableList<HashMap<String, Any>> = mutableListOf()
            for (sa in data2.order_items) {
                val mapa = HashMap<String, Any>()
                Log.i(" ", "" + sa)
                mapa["product_id"] = sa.product_id
                mapa["quantity"] = sa.quantity
                mapa["regular_price"] = sa.regular_price
                mapa["urgent_price"] = sa.urgent_price
                list.add(mapa)
            }
            map["order_items"] = list
        } else if (arguments != null && arguments!!.getString("isFromInComplete") == "isFromInComplete") {
            val data = com.ibox.washapp.utils.Prefs.with(context)
                .getObject(Constants.ORDER_ITEMS_COMP_ORDER, com.ibox.washapp.model.PojoOrderDetails::class.java)
            map["laundry_id"] = data.laundry_id
            val list: MutableList<HashMap<String, Any>> = mutableListOf()
            for (sa in data.order_items) {
                val mapa = HashMap<String, Any>()
                mapa["product_id"] = sa.product_id
                mapa["quantity"] = sa.quantity
                Log.i("QUANTITY", "" + Gson().toJson(sa.quantity))
                mapa["regular_price"] = sa.regular_price
                mapa["urgent_price"] = sa.urgent_price
                list.add(mapa)
            }
            map["order_items"] = list

        } else {
            Log.i("UpUp", "Create")
            val data = com.ibox.washapp.utils.Prefs.with(context).getObject(Constants.ORDER_DATA, OrderData::class.java)
            val list: MutableList<HashMap<String, Any>> = mutableListOf()
            for (sa in data.product!!.values) {
                val mapa = HashMap<String, Any>()
                mapa["product_id"] = sa.id
                mapa["quantity"] = sa.quantity
                mapa["regular_price"] = sa.regular_price
                mapa["urgent_price"] = sa.urgent_price
                list.add(mapa)
            }
            map["order_items"] = list
        }

        val mapData = MapData()
        mapData.map = map
        com.ibox.washapp.utils.Prefs.with(activity).save("dataMapOrder123", mapData)
        isClicked = true
        if (arguments != null && arguments!!.containsKey("isFromBasket")
            && arguments!!.getBoolean("isFromBasket")
        ) {
            Log.i("Order","BasketOrder $map")
            laundryViewModel.editBasketOrder(activity!!, map)
        } else {
            Log.i("Order","create $map")
            laundryViewModel.editOrder(activity!!, map)
        }
    }


    private var pickupCalenderAdapter: CalenderAdapter? = null
    private var deliveryCalenderAdapter: CalenderAdapter? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)



        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel::class.java)
        commonViewModel.getUserData().observe(this, androidx.lifecycle.Observer {
            noAddress = it.addresses.size <= 0
        })
        commonViewModel.getUser(activity!!)
        val pickUpTime = com.ibox.washapp.utils.Prefs.with(activity).getString(Constants.PICK_TIME, "")
        val pickUpAdd = com.ibox.washapp.utils.Prefs.with(activity).getString(Constants.PICK_ADDRESS, "")
        val dropOffTime = com.ibox.washapp.utils.Prefs.with(activity).getString(Constants.DROP_TIME, "")
        val dropOffAdc = com.ibox.washapp.utils.Prefs.with(activity).getString(Constants.DROP_ADDRESS, "")
        if (pickUpTime != null && pickUpAdd != null && dropOffTime != null && dropOffAdc != null) {
            etSelectTimePickup.text = Editable.Factory.getInstance().newEditable(pickUpTime)
            etSelectAddressPickup.text = Editable.Factory.getInstance().newEditable(pickUpAdd)
            etDropOffSelectTime.text = Editable.Factory.getInstance().newEditable(dropOffTime)
            etDropOffAddress.text = Editable.Factory.getInstance().newEditable(dropOffAdc)
        }

        var dates = CommonMethods.getNext30Dates(0)
        for (i in dates.indices) {
            val date = SimpleDateFormat("dd", Locale.ENGLISH).format(dates[i].getTime())
            val day = SimpleDateFormat("EE", Locale.ENGLISH).format(dates[i].getTime())
            val month = SimpleDateFormat("MMM", Locale.ENGLISH).format(dates[i].getTime())
            val mon = SimpleDateFormat("MM", Locale.ENGLISH).format(dates[i].getTime())
            arrayListCalender.add(Calender(day, date, month, mon))
        }
        rvCalender.layoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.HORIZONTAL, false
        )
        rvCalender.addItemDecoration(MarginItemDecoration(resources.getDimension(R.dimen.dimens_14).toInt()))
        pickupCalenderAdapter = CalenderAdapter(arrayListCalender, activity!!)
        rvCalender.adapter = pickupCalenderAdapter

        rvCalender.addOnItemTouchListener(
            com.ibox.washapp.utils.RecyclerItemClickListener(activity,
                object :
                    com.ibox.washapp.utils.RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemLongPress(childView: View?, position: Int) {
                    }

                    override fun onItemClick(childView: View?, position: Int) {
                        pickupCalenderAdapter!!.changeIndex(position)
                        setDeliveryCalenderAdapter(position)
                    }
                })
        )
        rvCalender2.layoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.HORIZONTAL, false
        )
        rvCalender2.addItemDecoration(MarginItemDecoration(resources.getDimension(R.dimen.dimens_14).toInt()))
        setDeliveryCalenderAdapter(0)

        rvCalender2.addOnItemTouchListener(
            com.ibox.washapp.utils.RecyclerItemClickListener(activity,
                object :
                    com.ibox.washapp.utils.RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemLongPress(childView: View?, position: Int) {
                    }

                    override fun onItemClick(childView: View?, position: Int) {
                        deliveryCalenderAdapter!!.changeIndex(position)
                    }
                })
        )
        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel::class.java)
        commonViewModel.getUserData().observe(this, androidx.lifecycle.Observer {
            positionsArrayForAddress.clear()
            positionsArrayForAddressN.clear()
            positionsArrayForAddressNDrop.clear()
            positionsArrayForAddressDrop.clear()

            val area_id = com.ibox.washapp.utils.Prefs.with(context).getInt(Constants.AREA_ID, 0)
            val emirate_id = com.ibox.washapp.utils.Prefs.with(activity).getInt(Constants.EMIRAT_ID, 0)
            Log.i("IDS","$area_id $emirate_id")
            for (i in it.addresses) {
                if (i.emirate_id == emirate_id && i.area_id == area_id) {
                    positionsArrayForAddressN.add(i)
                    positionsArrayForAddress.add(i.title)
                }
                positionsArrayForAddressNDrop.add(i)
                positionsArrayForAddressDrop.add(i.title)
            }
        })


        commonViewModel.getTimeSlots().observe(this, androidx.lifecycle.Observer {
            positionsArrayPickup.clear()
            positionsArrayDelivery.clear()
            for (sa in it) {
                positionsArrayPickup.add(sa.time_slot!!)
                positionsArrayDelivery.add(sa.time_slot!!)
            }
        })

        commonViewModel.timeSlots(activity!!)

        laundryViewModel = ViewModelProviders.of(this).get(LaundryViewModel::class.java)
        laundryViewModel.createOrderMutableLiveData.observe(this, androidx.lifecycle.Observer {
            if (isClicked) {
                isClicked = false
                var isFromBasket = false
                if (arguments != null && arguments!!.containsKey("isFromBasket")
                    && arguments!!.getBoolean("isFromBasket")
                ) {
                    isFromBasket = true
                }
                val arguments = Bundle()
                arguments.putInt("id", it.id)
                arguments.putString("From", arguments.getString("isFromInComplete"))
                val pickAdd = com.ibox.washapp.utils.Prefs.with(activity).getString(Constants.PICKADD, "")
                val dropAdd = com.ibox.washapp.utils.Prefs.with(activity).getString(Constants.DROPADD, "")
                if (pickAdd != null && dropAdd != null) {
                    arguments.putString("pickupAddress", pickAdd)
                    arguments.putString("deliveryAddress", dropAdd)
                } else {
                    arguments.putString("pickupAddress", pickUPAddress)
                    arguments.putString("deliveryAddress", deliveryAddress)
                }
                arguments.putBoolean("isFromBasket", isFromBasket)
                arguments.putString(
                    "pickupDate",
                    pickupCalenderAdapter!!.getSelectedDateShow() + ", " + etSelectTimePickup.text.toString()
                )

                arguments.putString(
                    "deliveryDate",
                    deliveryCalenderAdapter!!.getSelectedDateShow() + ", " + etDropOffSelectTime.text.toString()
                )


                val fragment = ReviewFragment()
                fragment.arguments = arguments

                CommonMethods.replaceFragmentWithBackStack(
                    fragmentManager!!,
                    fragment
                )
            }
        })

        /*Product to show in addtocart recyclerview from basket */
        if (arguments != null && arguments!!.containsKey("isFromBasket")
            && arguments!!.getBoolean("isFromBasket")
        ) {
            change = true
            val data2 = com.ibox.washapp.utils.Prefs.with(context)
                .getObject(Constants.NEW_RESPONSE_USER_DATA, OrderResponeLaundry::class.java)

            arrayList2.clear()
            arrayList2.addAll(data2.order_items)
            Collections.sort(arrayList2, Comparator<OrderItem> { obj1, obj2 ->
                // ## Ascending order
                obj1.product_name.compareTo(obj2.product_name, true) // To compare string values
                // return Integer.valueOf(obj1.empId).compareTo(Integer.valueOf(obj2.empId)); // To compare integer values
                // ## Descending order
                // return obj2.firstName.compareToIgnoreCase(obj1.firstName); // To compare string values
                // return Integer.valueOf(obj2.empId).compareTo(Integer.valueOf(obj1.empId)); // To compare integer values
            })
            rvAddedToCart.adapter =
                AddedToCartAdapter(arrayList2, arrayListAddToCart, activity!!, change, 0)
            tvAddedNo.text = arrayList2.size.toString()

        }

        /*Product to show in addtocart recyclerview from IncompleteOrder*/
        else if (arguments != null && arguments!!.getString("isFromInComplete") == "isFromInComplete") {
            val data = com.ibox.washapp.utils.Prefs.with(context)
                .getObject(Constants.ORDER_ITEMS_COMP_ORDER, com.ibox.washapp.model.PojoOrderDetails::class.java)
            Log.i("DATASS", "" + Gson().toJson(data))
            change = false

            arrayListAddToCart.clear()
            arrayListAddToCart.addAll(data.order_items)
            Collections.sort(arrayList2, Comparator<OrderItem> { obj1, obj2 ->
                // ## Ascending order
                obj1.product_name.compareTo(obj2.product_name, true) // To compare string values
                // return Integer.valueOf(obj1.empId).compareTo(Integer.valueOf(obj2.empId)); // To compare integer values
                // ## Descending order
                // return obj2.firstName.compareToIgnoreCase(obj1.firstName); // To compare string values
                // return Integer.valueOf(obj2.empId).compareTo(Integer.valueOf(obj1.empId)); // To compare integer values
            })
            rvAddedToCart.adapter =
                AddedToCartAdapter(arrayList2, arrayListAddToCart, activity!!, change, 1)
            tvAddedNo.text = arrayListAddToCart.size.toString()

        }

        /*Product to show in addtocartrecyclerview from CreateOrder */
        else {
            change = false
            val data = com.ibox.washapp.utils.Prefs.with(context).getObject(Constants.ORDER_DATA, OrderData::class.java)
            Log.i("DATAS", "" + Gson().toJson(data.product))
            arrayListAddToCart.clear()
            arrayListAddToCart.addAll(data.product!!.values)
            Collections.sort(arrayListAddToCart, Comparator<com.ibox.washapp.model.Product> { obj1, obj2 ->
                // ## Ascending order
                obj1.name.compareTo(obj2.name, true) // To compare string values
                // return Integer.valueOf(obj1.empId).compareTo(Integer.valueOf(obj2.empId)); // To compare integer values
                // ## Descending order
                // return obj2.firstName.compareToIgnoreCase(obj1.firstName); // To compare string values
                // return Integer.valueOf(obj2.empId).compareTo(Integer.valueOf(obj1.empId)); // To compare integer values
            })
            rvAddedToCart.adapter =
                AddedToCartAdapter(arrayList2, arrayListAddToCart, activity!!, change, 0)
            tvAddedNo.text = arrayListAddToCart.size.toString()
        }


        rvAddedToCart.layoutManager = LinearLayoutManager(activity)


        rlAddedToCart.setOnClickListener(this)
        etSelectTimePickup.setOnClickListener(this)
        etSelectAddressPickup.setOnClickListener(this)
        ivBackArrowPickAndDelivery.setOnClickListener(this)
        btnContinue.setOnClickListener(this)
        etDropOffSelectTime.setOnClickListener(this)
        etDropOffAddress.setOnClickListener(this)
        btnAddNewDelivery.setOnClickListener(this)
        btnAddNewPickup.setOnClickListener(this)
    }

    private fun setDeliveryCalenderAdapter(size: Int) {
        arrayListCalenderDrop.clear()
        val dates1 = CommonMethods.getNext30Dates(
            (CommonFunctions.getDays(activity!!)) + size
        )
        for (i in dates1.indices) {
            val date = SimpleDateFormat("dd", Locale.ENGLISH).format(dates1[i].getTime())
            val day = SimpleDateFormat("EE", Locale.ENGLISH).format(dates1[i].getTime())
            val month = SimpleDateFormat("MMM", Locale.ENGLISH).format(dates1[i].getTime())
            val mon = SimpleDateFormat("MM", Locale.ENGLISH).format(dates1[i].getTime())
            arrayListCalenderDrop.add(Calender(day, date, month, mon))
        }

        deliveryCalenderAdapter = CalenderAdapter(arrayListCalenderDrop, activity!!)
        rvCalender2.adapter = deliveryCalenderAdapter
    }

    fun listPopUp(arrayList: ArrayList<String>, view: TextView) {
        listPopupWindow = ListPopupWindow(activity!!)
        listPopupWindow.setAdapter(
            ArrayAdapter<String>(activity!!, R.layout.item_popup_list, arrayList)
        )
        listPopupWindow.setOnItemClickListener { _, _, position, _ ->
            view.text = arrayList.get(position)
            listPopupWindow.dismiss()
        }
        listPopupWindow.isModal = true
        listPopupWindow.anchorView = view
    }


    override fun onResume() {
        super.onResume()
        commonViewModel.getUser(activity!!)
    }

    fun listPopUpNew(arrayList: ArrayList<String>, view: TextView, isPickup: Boolean) {
        listPopupWindow = ListPopupWindow(activity!!)
        listPopupWindow.setAdapter(
            ArrayAdapter<String>(activity!!, R.layout.item_popup_list, arrayList)
        )
        listPopupWindow.setOnItemClickListener { _, _, position, _ ->

            if (isPickup) {
                view.text = positionsArrayForAddress[position]
                pickUPAddressId = positionsArrayForAddressN[position].id
                pickUPAddress =
                    positionsArrayForAddressN[position].flat_no + ", " + positionsArrayForAddressN[position].building_name + ", " + positionsArrayForAddressN[position].address + ", " + positionsArrayForAddressN[position].city + ", " + positionsArrayForAddressN[position].postal_code
                com.ibox.washapp.utils.Prefs.with(activity).save(Constants.PICKADD, pickUPAddress)
                com.ibox.washapp.utils.Prefs.with(activity).save(Constants.PICKADDID, pickUPAddressId)
            } else {
                view.text = positionsArrayForAddressDrop[position]

                deliveryAddress =
                    positionsArrayForAddressNDrop[position].flat_no + ", " + positionsArrayForAddressNDrop[position].building_name + positionsArrayForAddressNDrop[position].address + ", " + ", " + positionsArrayForAddressNDrop[position].city + ", " + positionsArrayForAddressNDrop[position].postal_code
                deliveryAddressId = positionsArrayForAddressNDrop[position].id
                com.ibox.washapp.utils.Prefs.with(activity).save(Constants.DROPADD, deliveryAddress)
                com.ibox.washapp.utils.Prefs.with(activity).save(Constants.DROPADDID, deliveryAddressId)
            }
            listPopupWindow.dismiss()
        }
        listPopupWindow.isModal = true
        listPopupWindow.anchorView = view
    }

    private var pickUPAddress = ""
    private var deliveryAddress = ""
    private var pickUPAddressId = 0
    private var deliveryAddressId = 0

    override fun setLayoutToInsert(): Int {
        return R.layout.fragment_pick_and_delivery
    }
}