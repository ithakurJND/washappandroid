package com.ibox.washapp.fragments

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Recycler
import com.ibox.washapp.R
import com.ibox.washapp.adapters.BasketProductAdapter
import com.ibox.washapp.adapters.SelectItemsRvHorizontalAdapter
import com.ibox.washapp.utils.CommonFunctions
import com.ibox.washapp.utils.Constants
import com.ibox.washapp.utils.PaginationScrollListener
import com.ibox.washapp.viewModels.CommonViewModel
import kotlinx.android.synthetic.main.fragment_add_to_basket.*


/*
* fragment to add items to basket*/
class AddToBasketFragment : BaseFragment() {
    var position = 0
    private var page = 0
    var commonViewModel = CommonViewModel()
    val serivcesArrayList: ArrayList<com.ibox.washapp.model.PojoOrderCategory> = ArrayList()
    val namesArrayList: ArrayList<com.ibox.washapp.model.Product> = ArrayList()
    var noAddress = false
    override fun setLayoutToInsert(): Int {
        return R.layout.fragment_add_to_basket
    }

    /*from vertical adapter*/
    private var AddedToBasketbroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val clicked = intent.getStringExtra("Added")
            if (clicked == "AddedToBasketClicked") {
                if (CommonFunctions.addProductBasketCount(activity!!) > 0) {
                    btnAddContinue.visibility = View.VISIBLE
                } else {
                    btnAddContinue.visibility = View.GONE
                }
            }
        }
    }
    private var clicked = 0
    /*from horizontal adapter*/
    private var productbroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            clicked = intent.getIntExtra("ListOfProducts", 0)
            namesArrayList.clear()
            namesArrayList.addAll(serivcesArrayList[clicked].products)
//            rvAddToBasketVertical.adapter!!.notifyDataSetChanged()
            rvAddToBasketVertical.adapter!!.notifyItemRangeChanged(0, adapter!!.itemCount)
        }
    }
    private var adapter: BasketProductAdapter? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        com.ibox.washapp.utils.Prefs.with(activity).remove(Constants.PICK_TIME)
        com.ibox.washapp.utils.Prefs.with(activity).remove(Constants.PICK_ADDRESS)
        com.ibox.washapp.utils.Prefs.with(activity).remove(Constants.DROP_TIME)
        com.ibox.washapp.utils.Prefs.with(activity).remove(Constants.DROP_ADDRESS)
        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel::class.java)
        commonViewModel.getUserData().observe(this, Observer {
            if (it.addresses.size > 0) {
                noAddress = false
            } else
                noAddress = true
        })
        commonViewModel.getUser(activity!!)
        val intentFilterAdd = IntentFilter("AddedToBasket")
        context!!.registerReceiver(AddedToBasketbroadcastReceiver, intentFilterAdd)
        val intentFilterProduct = IntentFilter("ProductList")
        context!!.registerReceiver(productbroadcastReceiver, intentFilterProduct)
        btnAddContinue.setOnClickListener()
        {
//            if (noAddress) {
//                Toast.makeText(activity, "Add Address", Toast.LENGTH_LONG).show()
//            } else
                startActivity(
                    Intent(activity, com.ibox.washapp.activities.BasketOrderActivity::class.java)
                        .putExtra("isFromBasket", true)
                )
        }
//        swipeRefresh.setOnRefreshListener {
//            page = 0
//            commonViewModel.addToBasket(activity!!, false, 0)
//        }
        rvaddToBasketHorizontal.layoutManager =  GridLayoutManager(activity,2)
//            LinearLayoutManager(
//            activity,
//            LinearLayoutManager.HORIZONTAL, false
//        )

        commonViewModel.getAddToBasket().observe(this, Observer { t ->

            //            swipeRefresh.isRefreshing = false
            pbBar.visibility = View.GONE
            isLoading = false
            if (t[0].page == 0) {
                namesArrayList.clear()
                btnAddContinue.visibility = View.GONE

                serivcesArrayList.clear()
                serivcesArrayList.addAll(t)
                namesArrayList.addAll(t[0].products)
                rvaddToBasketHorizontal.adapter =
                    SelectItemsRvHorizontalAdapter(serivcesArrayList, activity!!)

                adapter = BasketProductAdapter(
                    namesArrayList, activity!!,
                    activity as AppCompatActivity
                )
                rvAddToBasketVertical.adapter = adapter
                com.ibox.washapp.utils.Prefs.with(context).remove(Constants.ORDER_DATABASKET)


            } else {
                if (t.isNotEmpty()) {
                    for (i in t.indices) {
                        serivcesArrayList[i].products.addAll(t[i].products)
                    }
                    namesArrayList.addAll(t[clicked].products)
                    adapter!!.notifyData(t[clicked].products)
                }
            }
        })
//        val layoutManger = LinearLayoutManager(activity)
        val layoutManger = WrapContentLinearLayoutManager(activity!!)
        rvAddToBasketVertical.layoutManager = layoutManger
        page = 0
        commonViewModel.addToBasket(activity!!, true, 0)
        setScrollListener(layoutManger)
    }

    private var isLoading = false

    private fun setScrollListener(layoutManger: LinearLayoutManager) {

        rvAddToBasketVertical.addOnScrollListener(object : PaginationScrollListener(layoutManger) {
            override fun isLastPage(): Boolean {
                return false
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                if (namesArrayList.size % 10 == 0) {
                    page += 1
                    isLoading = true
                    pbBar.visibility = View.VISIBLE
                    commonViewModel.addToBasket(activity!!, false, page)
                }
            }
        })
    }

    fun sortList() {
        if (commonViewModel != null) {
            com.ibox.washapp.utils.Prefs.with(context).remove(Constants.ORDER_DATABASKET)
            adapter = BasketProductAdapter(
                namesArrayList, activity!!,
                activity as AppCompatActivity
            )
            rvAddToBasketVertical.adapter = adapter
        }
    }

    inner class WrapContentLinearLayoutManager(context: Context) : LinearLayoutManager(context) {
        //... constructor
        override fun onLayoutChildren(
            recycler: Recycler,
            state: RecyclerView.State
        ) {
            try {
                super.onLayoutChildren(recycler, state)
            } catch (e: IndexOutOfBoundsException) {
                Log.e("Error", "IndexOutOfBoundsException in RecyclerView happens")
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        activity!!.unregisterReceiver(productbroadcastReceiver)
        activity!!.unregisterReceiver(AddedToBasketbroadcastReceiver)

    }

    override fun onResume() {
        super.onResume()
        Log.i("Resume", "Resume")
    }

    var _areLecturesLoaded = false

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser && !_areLecturesLoaded) {
            page = 0
            commonViewModel.addToBasket(activity!!, false, 0)
            commonViewModel.getUser(activity!!)
            _areLecturesLoaded = false
        }
    }

}