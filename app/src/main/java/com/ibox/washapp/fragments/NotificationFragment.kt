package com.ibox.washapp.fragments

import android.app.NotificationManager
import android.content.Context.NOTIFICATION_SERVICE
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.ibox.washapp.R
import com.ibox.washapp.adapters.NotificationAdapter
import com.ibox.washapp.viewModels.CommonViewModel
import kotlinx.android.synthetic.main.fragment_notification.*


class NotificationFragment : BaseFragment() {
    private var commonViewModel = CommonViewModel()
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        rvNotification.layoutManager = LinearLayoutManager(activity)
        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel::class.java)
        commonViewModel.getNotifications().observe(this, Observer { user ->
            Log.i("GetUser", "" + user)
            rvNotification.adapter =
                NotificationAdapter(user.data, activity!!, activity as AppCompatActivity)

        })
        commonViewModel.notificationListing(activity!!)
        ivBackArrow.setOnClickListener()
        {
            activity!!.onBackPressed()
        }
        cancelNotification()
    }

    override fun setLayoutToInsert(): Int {
        return R.layout.fragment_notification
    }
    fun cancelNotification() {
        val ns = NOTIFICATION_SERVICE
        val nMgr =
            activity!!.applicationContext.getSystemService(ns) as NotificationManager
        nMgr.cancelAll()
    }

}