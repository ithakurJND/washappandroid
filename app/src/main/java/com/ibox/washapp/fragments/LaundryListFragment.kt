package com.ibox.washapp.fragments

import android.app.AlertDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ListPopupWindow
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.ibox.washapp.R
import com.ibox.washapp.adapters.LaundryListAdapter
import com.ibox.washapp.dialog.ShowLocations
import com.ibox.washapp.model.Area
import com.ibox.washapp.model.Locations
import com.ibox.washapp.utils.CommonMethods
import com.ibox.washapp.utils.Constants
import com.ibox.washapp.utils.Prefs
import com.ibox.washapp.viewModels.CommonViewModel
import com.ibox.washapp.viewModels.LaundryViewModel
import kotlinx.android.synthetic.main.fragment_laundry_list.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class LaundryListFragment : BaseFragment(), View.OnClickListener {
    private var laundryViewModel = LaundryViewModel()
    private var commonViewModel = CommonViewModel()
    private var area_id = 0
    private var emirate_id = 0
    private var lat = 0.0
    private var lng = 0.0
    private var hit = ArrayList<String>()
    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ivBackArrow -> {
                activity!!.onBackPressed()
            }
        }
    }

    lateinit var listPopupWindow: ListPopupWindow
    var positionArrayList = ArrayList<String>()
    var positionArrayListLatNlg = ArrayList<Area>()


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        ivBackArrow.setOnClickListener(this)
        Log.i("CallFor","OnCreated laundry list")
        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel::class.java)
        commonViewModel.getLocations().observe(this, Observer { locations ->
            positionArrayList.clear()
            for (i in locations.areas.indices) {
                val area = locations.areas[i].area
                val emirate = locations.areas[i].emirate
                positionArrayList.add("$area, $emirate")
            }
            positionArrayListLatNlg.addAll(locations.areas)
//            etSelectLocation.setOnClickListener()
//            {
//                //                                listPopUp(positionArrayList, etSelectLocation, positionArrayListLatNlg)
////                listPopupWindow.show()
//                CommonMethods.showDialogFragmentWithoutAnimation(
//                    activity as AppCompatActivity,
//                    ShowLocations()
//                )
//            }

        })
        etSelectLocation.setOnClickListener()
        {
            //                                listPopUp(positionArrayList, etSelectLocation, positionArrayListLatNlg)
//                listPopupWindow.show()
            CommonMethods.showDialogFragmentWithoutAnimation(
                activity as AppCompatActivity,
                ShowLocations()
            )
        }
       // commonViewModel.locations(activity!!, "", 0, 20)
        if (hit.isEmpty()) {
            commonViewModel.getUserData().observe(this, Observer { user ->
                Log.i("GetUser", "" + user)
                if (user.addresses.size > 0) {
                    for (i in user.addresses) {
                        if (i.is_default == 1) {
                            etSelectLocation.setText(i.city)
                            area_id = i.area_id
                            emirate_id = i.emirate_id
                            lat = i.lat.toDouble()
                            lng = i.lng.toDouble()
                            com.ibox.washapp.utils.Prefs.with(activity)
                                .save(Constants.AREA_ID, area_id)
                            com.ibox.washapp.utils.Prefs.with(activity)
                                .save(Constants.EMIRAT_ID, emirate_id)
                            getLanundries()
                            break
                        }
                    }
                }
            })
        }
        if (hit.isEmpty())
            commonViewModel.getUser(activity!!)
        laundryViewModel = ViewModelProviders.of(this).get(LaundryViewModel::class.java)
        laundryViewModel.getVendors().observe(this, Observer { data ->
            Log.i("show", " " + data)
            rvLaundryList.layoutManager = LinearLayoutManager(activity)
            var orderID = -1
            if (arguments != null && arguments!!.containsKey("isFromBasket")) {
                orderID = arguments!!.getInt("id")
            }

            rvLaundryList.adapter =
                LaundryListAdapter(
                    data, activity!!, activity as AppCompatActivity, orderID, laundryViewModel
                    , this
                )
            if (data.isEmpty() && arguments != null && arguments!!.containsKey("isFromBasket")) {
                showDialog()
            }

            if (data.isNotEmpty()) {
                noData.visibility = View.GONE
            } else {
                noData.visibility = View.VISIBLE
            }
        })

        laundryViewModel.createOrderMutableLiveData.observe(this, Observer {
            if (arguments != null && arguments!!.containsKey("isFromBasket")
                && isClicked
            ) {
                isClicked = false
                val arguments = Bundle()
                arguments.putInt("id", it.id)
                arguments.putBoolean("isFromBasket", true)
                val fragment = PickAndDeliveryFragment()
                fragment.arguments = arguments
                CommonMethods.replaceFragmentWithBackStack(
                    fragmentManager!!,
                    fragment
                )
                com.ibox.washapp.utils.Prefs.with(activity)
                    .save(Constants.NEW_RESPONSE_USER_DATA, it)
            }

        })
        activity!!.registerReceiver(selectedLoc, IntentFilter("SelectedLoc"))

    }

    var lat2 = 0.0
    var lng2 = 0.0
    var selectedLoc = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            Log.i("Selected", "ksdjkfjkj  " + intent!!.getIntExtra("emirateId", 0))
            if (!intent!!.getBooleanExtra("for", false)) {
                //hit = false
                hit.add("abd")
                lat2 = intent!!.getDoubleExtra("lat", 0.0)
                lng2 = intent!!.getDoubleExtra("lng", 0.0)
                Log.i("ShowIDS", "on laundry frag latlan  $lat2 $lng2")
                area_id = intent!!.getIntExtra("areaId", 0)
                Log.i("ShowIDS", "on laundry frag $area_id $emirate_id")
                emirate_id = intent!!.getIntExtra("emirateId", 0)
                etSelectLocation.setText(
                    intent!!.getStringExtra("area") + "," + intent!!.getStringExtra(
                        "emirate"
                    )
                )
                com.ibox.washapp.utils.Prefs.with(activity).save(Constants.AREA_ID, area_id)
                com.ibox.washapp.utils.Prefs.with(activity).save(Constants.EMIRAT_ID, emirate_id)
                Prefs.with(activity).save(Constants.LAT2, lat2.toString())
                Prefs.with(activity).save(Constants.LNG2, lng2.toString())
                Prefs.with(activity).save(
                    Constants.TEXT, intent!!.getStringExtra("area") + "," + intent!!.getStringExtra(
                        "emirate"
                    )
                )

                getLanundries()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        activity!!.unregisterReceiver(selectedLoc)
    }

    var isClicked = false
    private fun showDialog() {
        val builder = AlertDialog.Builder(activity)
        builder.setTitle("Laundry not available")
        builder.setMessage("Unfortunately no laundries are available for this service. One of our Customer Care representative will call you within 24 hours. Thankyou!")
        builder.setPositiveButton("Search another Location") { dialog, which ->
            dialog.dismiss()
        }
        builder.setNegativeButton("Close") { dialog, which ->
            dialog.dismiss()
            activity!!.finishAffinity()
            startActivity(Intent(activity, com.ibox.washapp.activities.MainActivity::class.java))
        }


        val dialog: AlertDialog = builder.create()
        dialog.setOnShowListener {
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
                .setTextColor(resources.getColor(R.color.drakBlue))
            dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                .setTextColor(resources.getColor(R.color.drakBlue))
            dialog.getButton(AlertDialog.BUTTON_NEUTRAL)
                .setTextColor(resources.getColor(R.color.drakBlue))
        }
        dialog.show()
    }

    private fun getLanundries() {
        val map = HashMap<String, Any>()
        map["area_id"] = area_id
        map["emirate_id"] = emirate_id
        if (arguments != null && arguments!!.containsKey("isFromBasket")) {
            Log.i("FromFrag", "isfrombasket  id " + arguments!!.getInt("id"))
            Log.i(
                "FromFrag",
                "service_type " + com.ibox.washapp.utils.Prefs.with(activity).getInt(
                    "serviceType",
                    1
                )
            )
            map["order_id"] = arguments!!.getInt("id")
            map["service_type"] =
                com.ibox.washapp.utils.Prefs.with(activity).getInt("serviceType", 1)
            laundryViewModel.laundryBasketList(activity!!, map)
        } else {
            laundryViewModel.laundryList(activity!!, map)
        }
    }


    fun listPopUp(
        arrayList: ArrayList<String>,
        view: TextView,
        positionArrayListLatNlg: ArrayList<Area>
    ) {
        listPopupWindow = ListPopupWindow(activity!!)
        listPopupWindow.setAdapter(
            ArrayAdapter<String>(activity!!, R.layout.item_popup_list, arrayList)
        )
        listPopupWindow.setOnItemClickListener { p0, p1, position, p3 ->
            view.text = positionArrayList[position]
            Log.i("ListPopup", "visible list " + positionArrayList[position])
            Log.i("ListPopup", "all list " + positionArrayListLatNlg[position])
            Log.i(
                "ListPopup",
                "id and emirate_id  " + positionArrayListLatNlg[position].id + " " + positionArrayListLatNlg[position].emirate_id
            )
            area_id = positionArrayListLatNlg[position].id
            emirate_id = positionArrayListLatNlg[position].emirate_id
            com.ibox.washapp.utils.Prefs.with(activity).save(Constants.AREA_ID, area_id)
            com.ibox.washapp.utils.Prefs.with(activity).save(Constants.EMIRAT_ID, emirate_id)
            lat = positionArrayListLatNlg[position].lat
            lng = positionArrayListLatNlg[position].lng
            getLanundries()
            listPopupWindow.dismiss()

        }
        listPopupWindow.isModal = true
        listPopupWindow.anchorView = view
    }


    override fun setLayoutToInsert(): Int {
        return R.layout.fragment_laundry_list
    }
}