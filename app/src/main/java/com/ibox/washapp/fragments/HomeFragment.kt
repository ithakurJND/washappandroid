package com.ibox.washapp.fragments

import android.graphics.Rect
import android.os.Bundle
import android.os.Handler
import android.text.Html
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.ibox.washapp.R
import com.ibox.washapp.adapters.BannerHolderAdapter
import com.ibox.washapp.adapters.ServiceWeOfferAdapter
import com.ibox.washapp.dialog.ReviewDialog
import com.ibox.washapp.model.BannerImg
import com.ibox.washapp.model.Service
import com.ibox.washapp.utils.Constants
import com.ibox.washapp.utils.Prefs
import com.ibox.washapp.viewModels.CommonViewModel
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : BaseFragment() {
    var commonViewModel = CommonViewModel()
    val arrayListNames = ArrayList<Service>()
    override fun setLayoutToInsert(): Int {
        return R.layout.fragment_home
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        ivdot1.setImageResource(R.drawable.rectangle_dots_white)
        ivdot2.setImageResource(R.drawable.dot)
        ivdot3.setImageResource(R.drawable.dot)
        val mResources = ArrayList<BannerImg>()

        Log.i("Access", "" +Prefs.with(activity!!).getString(Constants.PREFS_TOKEN,""))

        commonViewModel.getBannerImages().observe(this, Observer {
            mResources.clear()
            mResources.addAll(it.banner_imgs)
            BannerViewPager.adapter = BannerHolderAdapter(mResources, activity!!)
            setBannerAuto()
            com.ibox.washapp.utils.Prefs.with(activity).save(Constants.HOME_DATA, it)
            arrayListNames.clear()
            arrayListNames.addAll(it.services)
            rvServiceWeOffer.layoutManager =
                GridLayoutManager(activity, 2) as RecyclerView.LayoutManager?
            rvServiceWeOffer.addItemDecoration(MarginDecoration(resources.getDimension(R.dimen.dimens_14).toInt()))
            rvServiceWeOffer.adapter = ServiceWeOfferAdapter(
                arrayListNames, activity!!,
                activity as AppCompatActivity
            )
            if (it.order != null) {
                if (it.order.laundry_name != null)
                    showReviewDialog(it.order.id, it.order.laundry_name, it.order.OrderId.toString())
            }
        })
        commonViewModel.bannerImages(activity!!)

        BannerViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                osition: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                if (BannerViewPager.currentItem == 0) {
                    ivdot1.setImageResource(R.drawable.rectangle_dots_white)
                    ivdot2.setImageResource(R.drawable.dot)
                    ivdot3.setImageResource(R.drawable.dot)
                } else if (BannerViewPager.currentItem == 1) {
                    ivdot1.setImageResource(R.drawable.dot)
                    ivdot2.setImageResource(R.drawable.rectangle_dots_white)
                    ivdot3.setImageResource(R.drawable.dot)

                } else if (BannerViewPager.currentItem == 2) {
                    ivdot2.setImageResource(R.drawable.dot)
                    ivdot3.setImageResource(R.drawable.rectangle_dots_white)

                }
            }
        })
        commonViewModel.getUser(activity!!)
    }

    private fun showReviewDialog(orderId: Int, laundryName: String, OrderId: String) {
        val dialog = ReviewDialog(activity!!, orderId, laundryName, OrderId)
        dialog.show()
    }

    private fun setBannerAuto() {
        try {
            Handler().postDelayed({
                if (BannerViewPager != null) {
                    Log.i("BannerViewPager", "" + BannerViewPager.adapter!!.count)
                    Log.i("BannerViewPager", "Current" + BannerViewPager.currentItem)
                    BannerViewPager.setScrollDuration(1000)
                    if (BannerViewPager.currentItem != BannerViewPager.adapter!!.count - 1) {
                        BannerViewPager.setCurrentItem(BannerViewPager.currentItem + 1, true)
                    } else {
                        BannerViewPager.currentItem = 0
                    }
                }
                setBannerAuto()
            }, 3000)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    inner class MarginDecoration(private val spaceHeight: Int) : RecyclerView.ItemDecoration() {
        override fun getItemOffsets(
            outRect: Rect, view: View,
            parent: RecyclerView, state: RecyclerView.State
        ) {
            with(outRect) {
                if (parent.getChildAdapterPosition(view) == 0) {

                }
                right = spaceHeight
                bottom = spaceHeight

            }
        }
    }
}