package com.ibox.washapp.fragments

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ListPopupWindow
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat.finishAffinity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ibox.washapp.viewModels.CommonViewModel
import com.ibox.washapp.R
import com.ibox.washapp.dialog.ShowLocations
import com.ibox.washapp.utils.CommonMethods
import kotlinx.android.synthetic.main.fragment_after_intro_address.*
import kotlinx.android.synthetic.main.fragment_phone_number.*

/*
* Address fragment after Introduction fragment to select location and delivery location*/
class AfterIntroAddressDetailsFragment : BaseFragment(), View.OnClickListener {
    lateinit var listPopupWindow: ListPopupWindow
    var positionsArrayList = ArrayList<String>()
    var commonViewModel = CommonViewModel()
    var delivarySelected = false
    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.btnContinue -> {
                if (etSelectAddress.text.isEmpty()) {
                    etSelectAddress.error = "Select Address"
                    etSelectAddress.startAnimation(CommonMethods.shakeError())
                } else if (etSelectMoreAddress.visibility == View.VISIBLE && etSelectMoreAddress.text.isEmpty()) {
                    etSelectMoreAddress.error = "Select Address"
                    etSelectMoreAddress.startAnimation(CommonMethods.shakeError())
                } else {
                    finishAffinity(activity!!)
                    startActivity(
                        Intent(
                            activity,
                            com.ibox.washapp.activities.LoginActivity::class.java
                        )
                    )
                }
            }
            R.id.ivUnselected -> {
                if (ivSelected.visibility == View.GONE) {
                    etSelectMoreAddress.visibility = View.VISIBLE
                    ivSelected.visibility = View.VISIBLE
                    ivUnselected.visibility = View.GONE
                }
            }
            R.id.ivSelected -> {
                if (ivUnselected.visibility == View.GONE) {
                    etSelectMoreAddress.visibility = View.GONE
                    ivSelected.visibility = View.GONE
                    ivUnselected.visibility = View.VISIBLE
                }
            }
            R.id.tvAddMoreLocation -> {
                if (ivUnselected.visibility == View.GONE) {
                    ivSelected.performClick()
                } else {
                    ivUnselected.performClick()
                }
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        btnContinue.visibility = View.GONE
        btnContinue.setOnClickListener(this)
        ivUnselected.setOnClickListener(this)
        ivSelected.setOnClickListener(this)
        tvAddMoreLocation.setOnClickListener(this)

        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel::class.java)
        commonViewModel.getLocations().observe(this, Observer { location ->
            for (i in location.areas.indices) {
                val area = location.areas[i].area
                val emirate = location.areas[i].emirate
                positionsArrayList.add("$area, $emirate")
            }
            etSelectAddress.setOnClickListener()
            {
                //                listPopUp(positionsArrayList, etSelectAddress)
//                listPopupWindow.show()
                CommonMethods.showDialogFragmentWithoutAnimation(
                    activity as AppCompatActivity,
                    ShowLocations()
                )
            }
            etSelectMoreAddress.setOnClickListener()
            {

                delivarySelected = true
                CommonMethods.showDialogFragmentWithoutAnimation(
                    activity as AppCompatActivity,
                    ShowLocations()
                )
//                listPopupWindow = ListPopupWindow(activity!!)
//                listPopupWindow.setAdapter(
//                    ArrayAdapter<String>(activity!!, R.layout.item_popup_list, positionsArrayList)
//                )
//                listPopupWindow.setOnItemClickListener { p0, p1, position, p3 ->
//                    etSelectMoreAddress.setText(positionsArrayList.get(position))
//                    listPopupWindow.dismiss()
//                }
//                listPopupWindow.isModal = true
//                listPopupWindow.anchorView = etSelectMoreAddress
//                listPopupWindow.show()
            }
        })
        commonViewModel.locations(activity!!, "", 0, 20)
        activity!!.registerReceiver(address, IntentFilter("selectedLocation"))
    }

    var address = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {

            if (delivarySelected) {
                etSelectMoreAddress.error = null
                delivarySelected = false
                etSelectMoreAddress.setText(
                    intent!!.getStringExtra("area") + "," + intent!!.getStringExtra(
                        "emirate"
                    )
                )
            }
                else {
                etSelectAddress.setText(
                    intent!!.getStringExtra("area") + "," + intent!!.getStringExtra(
                        "emirate"
                    )
                )
                btnContinue.visibility = View.VISIBLE
            }
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        activity!!.unregisterReceiver(address)
    }

    fun listPopUp(arrayList: ArrayList<String>, view: TextView) {
        listPopupWindow = ListPopupWindow(activity!!)
        listPopupWindow.setAdapter(
            ArrayAdapter<String>(activity!!, R.layout.item_popup_list, arrayList)
        )
        listPopupWindow.setOnItemClickListener { p0, p1, position, p3 ->
            view.text = positionsArrayList.get(position)
            view.error = null
            if (view.text != null) {
                btnContinue.visibility = View.VISIBLE
            }
            listPopupWindow.dismiss()
        }
        listPopupWindow.isModal = true
        listPopupWindow.anchorView = view
    }

    override fun setLayoutToInsert(): Int {
        return R.layout.fragment_after_intro_address
    }
}