package com.ibox.washapp.fragments

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.view.size
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ibox.washapp.R
import com.ibox.washapp.utils.CommonFunctions
import com.ibox.washapp.viewModels.CommonViewModel
import com.ibox.washapp.viewModels.SchdeuleViewModel
import kotlinx.android.synthetic.main.add_view_schedule_fragment.view.*
import kotlinx.android.synthetic.main.fragment_schedule.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class Schedulefragment : BaseFragment(), View.OnClickListener {
    lateinit var listPopupWindow: ListPopupWindow
    var currentIndexForPick = 1
    private var positionsArrayPickup: ArrayList<String> = arrayListOf()
    private var positionsArrayDelivery: ArrayList<String> = arrayListOf()
    private var laundryViewModel = SchdeuleViewModel()
    private var commonViewModel = CommonViewModel()
    private var currentDateInString = ""

    /*Handling all clicks*/
    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ivPlusPick -> {
                tvNodata.visibility = android.view.View.GONE
                currentIndexForPick += 1
                tvNoSchedulePick.text = currentIndexForPick.toString()
                val data = com.ibox.washapp.model.PojoSchdeule()
                data.dropoff_time_slot = ""
                data.pickup_date = ""
                data.pickup_time_slot = ""
                data.dropoff_date = ""
                listData.add(data)
                addView(llSchedulePick, currentIndexForPick, data)
            }
            R.id.ivMinusPick -> {
                if (currentIndexForPick == 0) {
                    tvNodata.visibility = android.view.View.VISIBLE
                } else if (currentIndexForPick > 0) {
                    currentIndexForPick -= 1
                    tvNoSchedulePick.text = currentIndexForPick.toString()
                    listData.removeAt(listData.size - 1)
                    if (currentIndexForPick == 0) {
                        tvNodata.visibility = android.view.View.VISIBLE
                    }
                    removeView(llSchedulePick)
                }
            }
            R.id.btnContinue -> {
                if (isVerified()) {
                    val map = HashMap<String, Any>()
                    map["schedule"] = listData
                    laundryViewModel.postSchedule(activity!!, map)
                } else {
                    Toast.makeText(activity, "Please select all valid values", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }
    }


    /*Check for valid values selection in schedule*/
    private fun isVerified(): Boolean {
        val sdf = SimpleDateFormat("dd/MM/yyyy")
        val currentDate = sdf.format(Date())
        val current = CommonFunctions.formatDate("dd/MM/yyyy", "dd-MMM-yy", currentDate)
        Log.i("DATEE","CURREENT "+CommonFunctions.formatDate("dd/MM/yyyy", "dd-MMM-yy", currentDate))

        var isVerified = true
        for (sa in listData) {
            Log.i("Time", "" + CommonFunctions.millseconds("dd-MMM-yy", sa.pickup_date))
            if (sa.pickup_date.isEmpty()) {
                isVerified = false
                break
            } else if (sa.dropoff_date.isEmpty()) {
                isVerified = false
                break
            } else if (CommonFunctions.millseconds("dd-MMM-yy", sa.pickup_date)
                > CommonFunctions.millseconds("dd-MMM-yy", sa.dropoff_date)
            ) {
                isVerified = false
                break
            } else if (sa.pickup_time_slot.isEmpty()) {
                isVerified = false
                break
            } else if (sa.dropoff_time_slot.isEmpty()) {
                isVerified = false
                break
            }
            else if (CommonFunctions.millseconds("dd-MMM-yy", sa.pickup_date)
                ==CommonFunctions.millseconds("dd-MMM-yy", current))
            {
                val saam = sa.pickup_time_slot.split("-")[0].substring(
                    sa.pickup_time_slot.split("-")[0].length - 3,
                    sa.pickup_time_slot.split("-")[0].length - 1
                )
                var da = sa.pickup_time_slot.split("-")[0].substring(0, 2).trim().toInt()
                val saamDrop = sa.dropoff_time_slot.split("-")[0].substring(
                    sa.dropoff_time_slot.split("-")[0].length - 3,
                    sa.dropoff_time_slot.split("-")[0].length - 1
                )
                var daDrop = sa.dropoff_time_slot.split("-")[0].substring(0, 2).trim().toInt()
                if (saam == "PM") {
                    da += 12
                    val sdf = SimpleDateFormat("kk")
                    val currentDate = sdf.format(Date())
                    if (da <= currentDate.toInt())
                    {
                        isVerified = false
                        break
                    }
                }

                if (saamDrop == "PM") {
                    daDrop += 12
                }

                if (da >= daDrop) {
                    isVerified = false
                }

                val sdf = SimpleDateFormat("kk")
                val currentDate = sdf.format(Date())
                if (da <= currentDate.toInt())
                {
                    isVerified = false
                    break
                }
            }

            else if (CommonFunctions.millseconds("dd-MMM-yy", sa.pickup_date)
                == CommonFunctions.millseconds("dd-MMM-yy", sa.dropoff_date)
            ) {
                val saam = sa.pickup_time_slot.split("-")[0].substring(
                    sa.pickup_time_slot.split("-")[0].length - 3,
                    sa.pickup_time_slot.split("-")[0].length - 1
                )
                var da = sa.pickup_time_slot.split("-")[0].substring(0, 2).trim().toInt()
                val saamDrop = sa.dropoff_time_slot.split("-")[0].substring(
                    sa.dropoff_time_slot.split("-")[0].length - 3,
                    sa.dropoff_time_slot.split("-")[0].length - 1
                )
                var daDrop = sa.dropoff_time_slot.split("-")[0].substring(0, 2).trim().toInt()
                if (saam == "PM") {
                    da += 12
                    val sdf = SimpleDateFormat("kk")
                    val currentDate = sdf.format(Date())
                    if (da <= currentDate.toInt())
                    {
                        isVerified = false
                        break
                    }
                }

                if (saamDrop == "PM") {
                    daDrop += 12
                }

                if (da >= daDrop) {
                    isVerified = false
                }

                val sdf = SimpleDateFormat("kk")
                val currentDate = sdf.format(Date())
                if (da <= currentDate.toInt())
                {
                    isVerified = false
                    break
                }
            }
        }
        return isVerified
    }


    private val listData: MutableList<com.ibox.washapp.model.PojoSchdeule> = mutableListOf()
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        tvNoSchedulePick.text = currentIndexForPick.toString()
        ivPlusPick.setOnClickListener(this)
        btnContinue.setOnClickListener(this)
        ivMinusPick.setOnClickListener(this)
        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel::class.java)
/*observer*/
        laundryViewModel = ViewModelProviders.of(this).get(SchdeuleViewModel::class.java)
        laundryViewModel.schdeuleMutableLiveData.observe(this, androidx.lifecycle.Observer {
            if (it.isNotEmpty()) {
                currentIndexForPick = 0
                for (sa in it) {
                    currentIndexForPick += 1
                    addView(llSchedulePick, currentIndexForPick, sa)
                }
                tvNodata.visibility = android.view.View.GONE
                tvNoSchedulePick.text = currentIndexForPick.toString()
                listData.clear()
                listData.addAll(it)
            } else {
                tvNodata.visibility = android.view.View.VISIBLE
                tvNoSchedulePick.text = "0"
                currentIndexForPick = 0

            }

        })

        laundryViewModel.postMutableLiveData.observe(this, Observer {
            Toast.makeText(activity, "Scheduled updated successfully", Toast.LENGTH_SHORT).show()
            val intent = Intent("ADD_TO_BASKET")
            intent.putExtra("CLICKED", "HOME")
            activity!!.sendBroadcast(intent)
        })
        laundryViewModel.addAddress(activity!!)

        commonViewModel.getTimeSlots().observe(this, androidx.lifecycle.Observer {
            positionsArrayPickup.clear()
            positionsArrayDelivery.clear()
            for (sa in it) {
                positionsArrayPickup.add(sa.time_slot!!)
                positionsArrayDelivery.add(sa.time_slot!!)
            }

        })

        commonViewModel.timeSlots(activity!!)

    }

    /*List PopUp on etPickUpTime && etDropOffTime*/
    private fun listPopUp(
        arrayList: ArrayList<String>, view: TextView,
        type: Int,
        pos: Int
    ) {
        listPopupWindow = ListPopupWindow(activity!!)
        listPopupWindow.setAdapter(
            ArrayAdapter<String>(activity!!, R.layout.item_popup_list, arrayList)
        )
        listPopupWindow.setOnItemClickListener { p0, p1, position, p3 ->
            view.text = arrayList.get(position)
            when (type) {

                2 -> {
                    listData[pos - 1].pickup_time_slot = arrayList.get(position)
                }

                5 -> {
                    listData[pos - 1].dropoff_time_slot = arrayList.get(position)
                }
            }
            listPopupWindow.dismiss()
        }
        listPopupWindow.isModal = true
        listPopupWindow.anchorView = view
    }

    /*Adding views on plus click in schedule*/

    private fun addView(linearLayout: LinearLayout, schedule: Int, data: com.ibox.washapp.model.PojoSchdeule) {
        val vi =
            LayoutInflater.from(context).inflate(R.layout.add_view_schedule_fragment, null)
        val tvScheduleNo = vi.tvScheduleNo
        tvScheduleNo.text = "Schedule $schedule"
        if (data.pickup_date.isNotEmpty()) {
            vi.tvWeek.text = CommonFunctions.formatDate(
                "yyyy-MM-dd",
                "dd-MMM-yy",
                data.pickup_date
            )
        } else {
            vi.tvWeek.text = data.pickup_date
        }

        vi.tvTime.text = data.pickup_time_slot
        if (data.dropoff_date.length > 0) {
            vi.tvWeekDelivery.text = CommonFunctions.formatDate(
                "yyyy-MM-dd",
                "dd-MMM-yy",
                data.dropoff_date
            )
        } else {
            vi.tvWeekDelivery.text = data.dropoff_date
        }
        vi.tvTimeDelivery.text = data.dropoff_time_slot


        vi.tvWeek.setOnClickListener {
            view = vi.tvWeek
            pos = schedule
            type = 0
            openDatePicker()
        }

        vi.tvTime.setOnClickListener {
            listPopUp(positionsArrayPickup, vi.tvTime, 2, schedule)
            listPopupWindow.show()
        }
        vi.tvWeekDelivery.setOnClickListener()
        {
            view = vi.tvWeekDelivery
            pos = schedule
            type = 1
            openDatePicker()
        }

        vi.tvTimeDelivery.setOnClickListener()
        {
            listPopUp(positionsArrayDelivery, vi.tvTimeDelivery, 5, schedule)
            listPopupWindow.show()
        }
        val insertPoint = linearLayout as ViewGroup
        insertPoint.addView(
            vi, ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
            )
        )
    }

    /*Removing views on minus click in schedule*/

    private fun removeView(linearLayout: LinearLayout) {
        val insertPoint = linearLayout as ViewGroup
        insertPoint.removeViewAt(linearLayout.size - 1)
    }


    /*Date Picker on click of etPickUpDate && etDropOffDate*/

    private fun openDatePicker() {
        val date = com.ibox.washapp.fragments.DatePickerFragment()
        /**
         * Set Up Current Date Into dialog
         */
        val now = Calendar.getInstance()

        now.add(Calendar.DATE, 0)
        val args = Bundle()
        args.putInt("year", now.get(Calendar.YEAR))
        args.putInt("month", now.get(Calendar.MONTH))
        args.putInt("day", now.get(Calendar.DAY_OF_MONTH))
        args.putLong("minTime", now.timeInMillis)
        date.arguments = args
        /**
         * Set Call back to capture selected date
         */
        date.setCallBack(ondate)
        date.show(childFragmentManager, "Date Picker")
    }

    private var view: TextView? = null
    private var pos = 0
    private var type = 0
    private var dateYYMMDD: String? = null
    private var ondate: DatePickerDialog.OnDateSetListener =
        DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            dateYYMMDD = String.format("%02d", dayOfMonth) + "/" + String.format(
                "%02d",
                monthOfYear + 1
            ) + "/" + year
              Log.i("DATEE","CALENDER "+CommonFunctions.formatDate("dd/MM/yyyy", "dd-MMM-yy", dateYYMMDD!!))
            currentDateInString = dayOfMonth.toString()

            view!!.text = CommonFunctions.formatDate("dd/MM/yyyy", "dd-MMM-yy", dateYYMMDD!!)
            if (type == 0) {
                listData[pos - 1].pickup_date = view!!.text.toString()
            } else {
                listData[pos - 1].dropoff_date = view!!.text.toString()
            }
        }

    override fun setLayoutToInsert(): Int {
        return R.layout.fragment_schedule
    }
}