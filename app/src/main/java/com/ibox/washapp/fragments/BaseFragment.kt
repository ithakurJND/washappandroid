package com.ibox.washapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.*

abstract class BaseFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(setLayoutToInsert(), container, false)
    }

    abstract fun setLayoutToInsert(): Int

    override fun onDestroyView() {
        super.onDestroyView()
        try {
            clearFindViewByIdCache()
        } catch (npe: NullPointerException) {

        }
    }
}