package com.ibox.washapp.fragments

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.ibox.washapp.R
import com.ibox.washapp.adapters.NewOrderAddapter
import com.ibox.washapp.viewModels.SchdeuleViewModel
import kotlinx.android.synthetic.main.fragment_new_order.*

class HistoryFragment :BaseFragment(){
    var laundryNameArrayList = ArrayList<com.ibox.washapp.model.PojoOrderListing>()
    private var laundryViewModel = SchdeuleViewModel()
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        rvNewOrder.layoutManager = LinearLayoutManager(activity)
        laundryViewModel = ViewModelProviders.of(this).get(SchdeuleViewModel::class.java)

        laundryViewModel.orderListMutableLiveData.observe(this, Observer {
            swipeRefresh.isRefreshing = false
            laundryNameArrayList.clear()
            laundryNameArrayList.addAll(it)
            noAddress.visibility = if(laundryNameArrayList.size==0) View.VISIBLE else View.GONE
            rvNewOrder.adapter =
                NewOrderAddapter(laundryNameArrayList, activity!!, activity as AppCompatActivity)

        })
        swipeRefresh.setOnRefreshListener {

            laundryViewModel.getOrders(activity!!,2,false)
        }
        laundryViewModel.getOrders(activity!!,2,true)



    }
    override fun setLayoutToInsert(): Int {
        return R.layout.fragment_new_order
    }
}