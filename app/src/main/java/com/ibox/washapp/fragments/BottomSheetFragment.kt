package com.ibox.washapp.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.ibox.washapp.R
import com.ibox.washapp.activities.SelectServicesActivity
import com.ibox.washapp.utils.Constants
import com.ibox.washapp.viewModels.CommonViewModel
import kotlinx.android.synthetic.main.fragment_bottom_sheet.*

class BottomSheetFragment : BottomSheetDialogFragment() {
    var commonViewModel = CommonViewModel()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_bottom_sheet, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val intent = Intent("ADD_TO_BASKET")
        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel::class.java)
        commonViewModel.getUserData().observe(this, Observer { user ->
            Log.i("GetUser", "" + user)
            rlCreateOrder.setOnClickListener()
            {
                com.ibox.washapp.utils.Prefs.with(activity).remove(Constants.PICK_TIME)
                com.ibox.washapp.utils.Prefs.with(activity).remove(Constants.PICK_ADDRESS)
                com.ibox.washapp.utils.Prefs.with(activity).remove(Constants.DROP_TIME)
                com.ibox.washapp.utils.Prefs.with(activity).remove(Constants.DROP_ADDRESS)
//                if (user.addresses.isEmpty()) {
//                    Toast.makeText(activity, "Add Address", Toast.LENGTH_LONG).show()
//                } else {
                    dismiss()
                    startActivity(Intent(activity, SelectServicesActivity::class.java))
               // }
            }
            rlAddToBasket.setOnClickListener()
            {
//                if (user.addresses.isEmpty()) {
//                    Toast.makeText(activity, "Add Address", Toast.LENGTH_LONG).show()
//                } else {
                    dismiss()
                    intent.putExtra("CLICKED", "AddToBasket")
                    activity!!.sendBroadcast(intent)
                //}

            }
            rlScheduledOrder.setOnClickListener()
            {
                com.ibox.washapp.utils.Prefs.with(activity).remove(Constants.PICK_TIME)
                com.ibox.washapp.utils.Prefs.with(activity).remove(Constants.PICK_ADDRESS)
                com.ibox.washapp.utils.Prefs.with(activity).remove(Constants.DROP_TIME)
                com.ibox.washapp.utils.Prefs.with(activity).remove(Constants.DROP_ADDRESS)
                dismiss()
                intent.putExtra("CLICKED", "ScheduledOrder")
                activity!!.sendBroadcast(intent)
            }
        })
        commonViewModel.getUser(activity!!)
    }
}