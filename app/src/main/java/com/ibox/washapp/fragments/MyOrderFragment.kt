package com.ibox.washapp.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.ibox.washapp.R
import kotlinx.android.synthetic.main.fragement_my_order.*

class MyOrderFragment : BaseFragment(), View.OnClickListener {

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ivBackArrow -> {
                activity!!.onBackPressed()
            }
            R.id.rlOrder -> {
                viewPagerTabLayout.currentItem = 0
                hideAll()
                tvOrder.isSelected = true
                ivIndicatorOrder.visibility = View.VISIBLE
            }
            R.id.rlProfile -> {
                viewPagerTabLayout.currentItem = 1
                hideAll()
                unSelectAll()
                tvProfile.isSelected = true
                ivIndicatorProfile.visibility = View.VISIBLE
            }
            R.id.rlAddress -> {
                viewPagerTabLayout.currentItem = 2
                hideAll()
                unSelectAll()
                tvAddress.isSelected = true
                ivIndicatorAddress.visibility = View.VISIBLE
            }

        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        ivBackArrow.setOnClickListener(this)
        rlOrder.setOnClickListener(this)
        rlProfile.setOnClickListener(this)
        rlAddress.setOnClickListener(this)

        viewPagerTabLayout.offscreenPageLimit = 3
        viewPagerTabLayout.adapter = tabAddapter(childFragmentManager)
        viewPagerTabLayout.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                if (viewPagerTabLayout.currentItem == 0) {
                    hideAll()
                    unSelectAll()
                    tvOrder.isSelected = true
                    ivIndicatorOrder.visibility = View.VISIBLE
                } else if (viewPagerTabLayout.currentItem == 1) {
                    hideAll()
                    unSelectAll()
                    tvProfile.isSelected = true
                    ivIndicatorProfile.visibility = View.VISIBLE
                } else {
                    hideAll()
                    unSelectAll()
                    tvAddress.isSelected = true
                    ivIndicatorAddress.visibility = View.VISIBLE
                }
            }
        })
        /* From More Fragment AnD Verification Fragment*/
        val selected = arguments!!.getString("Clicked")
        if (selected == "Order") {
            rlOrder.performClick()
        } else if (selected == "Profile") {
            rlProfile.performClick()
        } else if (selected == "ProfileFromVerify") {
            rlProfile.performClick()
        } else {
            rlAddress.performClick()
        }
    }

    fun hideAll() {
        ivIndicatorOrder.visibility = View.GONE
        ivIndicatorProfile.visibility = View.GONE
        ivIndicatorAddress.visibility = View.GONE
    }

    fun unSelectAll() {
        tvOrder.isSelected = false
        tvProfile.isSelected = false
        tvAddress.isSelected = false
    }

    inner class tabAddapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        override fun getItem(position: Int): Fragment {
            if (position == 0) {
                return OrderFragment()
            } else if (position == 1) {
                return ProfileFragment()
            } else {
                return AddressFragment()
            }
        }

        override fun getCount(): Int {
            return 3
        }
    }

    override fun setLayoutToInsert(): Int {
        return R.layout.fragement_my_order
    }
}