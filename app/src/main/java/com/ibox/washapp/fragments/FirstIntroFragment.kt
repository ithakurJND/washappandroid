package com.ibox.washapp.fragments

import android.os.Bundle
import com.ibox.washapp.R
import kotlinx.android.synthetic.main.fragment_first_intro.*

/*first introduction fragment used in viewpager */

class FirstIntroFragment(val postion: Int) : BaseFragment() {
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        when (postion) {
            0 -> {
                ivIntroLogo.setImageResource(R.drawable.intro)
                tvSpecialWashing.text = resources.getText(R.string.specialWashing)
            }
            1 -> {
                ivIntroLogo.setImageResource(R.drawable.steamiron_splashscreen)
                tvSpecialWashing.text = resources.getText(R.string.steamIroning)
            }
            else -> {
                ivIntroLogo.setImageResource(R.drawable.premiumlaundry_splashscreen)
                tvSpecialWashing.text = resources.getText(R.string.premiumlaundry)
            }
        }
    }

    override fun setLayoutToInsert(): Int {
        return R.layout.fragment_first_intro
    }
}