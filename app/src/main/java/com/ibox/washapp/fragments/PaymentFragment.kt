package com.ibox.washapp.fragments

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ibox.washapp.R
import com.ibox.washapp.activities.ShowWebViewActivity
import com.ibox.washapp.model.User
import com.ibox.washapp.utils.ApplicationGlobal
import com.ibox.washapp.utils.CommonMethods
import com.ibox.washapp.utils.Constants
import com.ibox.washapp.utils.Prefs
import com.ibox.washapp.viewModels.LaundryViewModel
import kotlinx.android.synthetic.main.fragment_payment.*
import kotlinx.android.synthetic.main.fragment_phone_number.*
import java.text.DecimalFormat

class PaymentFragment : BaseFragment(), View.OnClickListener {
    private var laundryViewModel = LaundryViewModel()
    private var isCredit = false
    private var isCash = true
    private var isCard = false
    var actualAmount =0
    var byCreditsAmount=0f
    var usedPromo = false
    var usedCredits = false
    var byCoupnAmount =0f
    val map = HashMap<String,Any>()
    lateinit var user :User
    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ivBackArrowPayment -> {
                activity!!.onBackPressed()
            }
            R.id.btnPay -> {
                com.ibox.washapp.utils.Prefs.with(activity).remove(Constants.PICK_TIME)
                com.ibox.washapp.utils.Prefs.with(activity).remove(Constants.PICK_ADDRESS)
                com.ibox.washapp.utils.Prefs.with(activity).remove(Constants.DROP_TIME)
                com.ibox.washapp.utils.Prefs.with(activity).remove(Constants.DROP_ADDRESS)
                com.ibox.washapp.utils.Prefs.with(activity).remove(Constants.PICKADDID)
                com.ibox.washapp.utils.Prefs.with(activity).remove(Constants.PICKADD)
                com.ibox.washapp.utils.Prefs.with(activity).remove(Constants.DROPADD)
                com.ibox.washapp.utils.Prefs.with(activity).remove(Constants.DROPADDID)
                var am = (btnAED.text.toString().replace("AED","")
                    .replace(" ",""))
                if (isCard)
                {
                    if (btnAED.text.toString().replace("[^0-9]".toRegex(), "") !="0") {
                        activity!!.startActivity(
                            Intent(activity, ShowWebViewActivity::class.java)
                                .putExtra(Constants.ACCESS_CODE, "AVBG03HF46BL39GBLB")
                                .putExtra(Constants.MERCHANT_ID, "46525")
                                .putExtra(
                                    Constants.ORDER_ID_FOR,
                                    arguments!!.getInt("id").toString()
                                )
                                .putExtra(
                                    Constants.REDIRECT_URL,
                                    "https://app.washapp.ae:8001/api/get-response"
                                )
                                .putExtra(
                                    Constants.CANCEL_URL,
                                    "https://app.washapp.ae:8001/api/get-response"
                                )
                                .putExtra(
                                    Constants.AMOUNT,
                                    (btnAED.text.toString().replace("AED","")
                                        .replace(" ",""))
                                )
                                .putExtra(Constants.CURRENCY, "AED")
                                .putExtra(Constants.BILLING_NAME, user.name)
                                .putExtra(Constants.BILLING_EMAIL, user.email)
                                .putExtra(Constants.BILLING_TEL, user.phone_no)
                                .putExtra(
                                    Constants.BILLING_ADDRESS,
                                    user.addresses[0].address + "," + user.addresses[0].city
                                )
                                .putExtra(Constants.BILLING_ZIP, user.addresses[0].postal_code)
                                .putExtra(Constants.BILLING_CITY, user.addresses[0].city)
//                        .putExtra(Constants.RSA_KEY_URL,"https://secure.ccavenue.com/transaction/jsp/GetRSA.jsp?access_code=AVII03HC39CA35IIAC&order_id=7395432")
                        )
                    }
                    else
                        payMent()
//                        Toast.makeText(activity,"Amount not suitable for online payment",Toast.LENGTH_LONG).show()

                  //  CommonMethods.randInt(0,999999).toString()
                }
                else
               payMent()
            }
            R.id.cardViewPayByCredits -> {
                var newAmount =btnAED.text.toString().replace("[^0-9]".toRegex(), "").toInt()
                if (ivUnselectedPayByCredit.drawable.constantState == ContextCompat.getDrawable(activity!!,R.drawable.check_selected)
                        !!.constantState){
                    usedCredits = false
                    if (usedPromo)
                    btnAED.text = "AED "+ DecimalFormat("#.##").format(byCreditsAmount.toFloat())
                    else
                        btnAED.text ="AED "+arguments!!.getString("amount")
                    isCredit= false
                    ivUnselectedPayByCredit.setImageResource(R.drawable.check_unselected)

                }else {
                    usedCredits = true
                    if (actualAmount - arguments!!.getInt("credits")<0)
                        btnAED.text = "AED "+"0"
                    else {
                        //   btnAED.text = "AED "+(actualAmount - arguments!!.getInt("credits"))
                        if (usedPromo)
                            if (byCoupnAmount - arguments!!.getInt("credits")<0)
                                byCreditsAmount = 0f
                                else
                            byCreditsAmount = byCoupnAmount - arguments!!.getInt("credits").toFloat()
                            else
                        byCreditsAmount = (actualAmount - arguments!!.getInt("credits")).toFloat()
                        btnAED.text = "AED " +  DecimalFormat("#.##").format(byCreditsAmount.toFloat())

                    }
                    isCredit = true
                    ivUnselectedPayByCredit.setImageResource(R.drawable.check_selected)
                }
            }
            R.id.rlCashPay -> {
                isCard = false
                isCash = true
                ivUnselectedCashOnDelivery.setImageResource(R.drawable.check_selected)
                ivUnselectedCreditAndDebit.setImageResource(R.drawable.check_unselected)
                rlCardInfo.visibility = View.GONE
            }
            R.id.rlCreditAndDebit -> {
                isCard = true
                isCash = false
               // rlCardInfo.visibility = View.VISIBLE
                ivUnselectedCreditAndDebit.setImageResource(R.drawable.check_selected)
                ivUnselectedCashOnDelivery.setImageResource(R.drawable.check_unselected)
            }
        }
    }



    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        ivBackArrowPayment.setOnClickListener(this)
        btnPay.setOnClickListener(this)
        cardViewPayByCredits.setOnClickListener(this)
        rlCashPay.setOnClickListener(this)
        rlCreditAndDebit.setOnClickListener(this)
        intialize()
    }

    private var amount =0
    private fun intialize() {
        user = Prefs.with(activity!!).getObject(Constants.USER_DATA,User::class.java)
        Log.i("UserData","$user")
        laundryViewModel = ViewModelProviders.of(this).get(LaundryViewModel::class.java)
        tvCreditsavailableNo.text = arguments!!.getInt("credits").toString()
        amount = arguments!!.getString("amount")!!.toFloat().toInt()
        actualAmount=arguments!!.getString("amount")!!.toFloat().toInt()
        btnAED.text = "AED "+arguments!!.getString("amount")
        laundryViewModel.appLyCoupMutableLiveData.observe(this, Observer {
            usedPromo = true
            btnAED.text = "AED "+it.final_amount
            byCoupnAmount = it.final_amount.toFloat()
            etPromoCode.isEnabled= false
            tvApply.text = "Remove"
            Toast.makeText(activity,"Coupon Applied",Toast.LENGTH_SHORT).show()
        })

        laundryViewModel.paymentMutableLiveData.observe(this, Observer {
            val fragment = ConfirmationFragment()
            val bundle = Bundle()
            bundle.putInt("id",arguments!!.getInt("id"))
            fragment.arguments = bundle
            CommonMethods.replaceFragmentWithBackStack(fragmentManager!!,fragment)
        })
        activity!!.registerReceiver(payment, IntentFilter("Status"))

        setListner()

    }
    val payment = object :BroadcastReceiver(){
        override fun onReceive(context: Context?, intent: Intent?) {
            var sts = intent!!.getStringExtra("status")
            var id = intent.getStringExtra("tracking_id")
            if (sts =="Transaction Successful!")
            {
                Log.i("Payment","Transaction Successful!")
                map["tracking_id"] = id
                payMent()
            }
            else
            {

            }
            Log.i("Payment","show me shoe me $id")
        }
    }

    private fun setListner() {
        tvApply.setOnClickListener {
            if (tvApply.text.toString().toLowerCase()!="remove") {
                usedPromo = true
                if (etPromoCode.text.isEmpty()) {
                    Toast.makeText(activity, "Please enter coupon code", Toast.LENGTH_SHORT).show()
                } else {
                    applyCoupon()
                }
            }else{
                etPromoCode.isEnabled = true
                usedPromo = false
                etPromoCode.setText("")
                tvApply.text = "Add"
                if (usedCredits)
                    btnAED.text = "AED "+ DecimalFormat("#.##").format(byCreditsAmount.toFloat())
                    else
                btnAED.text = "AED "+arguments!!.getString("amount")
            }
        }
    }

    private fun applyCoupon() {
        val map = HashMap<String,Any>()
        map["order_id"] = arguments!!.getInt("id")
        map["coupon_code"] = etPromoCode.text.toString()
        map["amount"]=(btnAED.text.toString().replace("AED","")
            .replace(" ","")).toFloat()
            //btnAED.text.toString().replace("[^0-9]".toRegex(), "")
        laundryViewModel.applyCoupon(activity!!,map)
    }

    private fun payMent() {
        map["order_id"] = arguments!!.getInt("id")
        map["amount"] =arguments!!.getString("amount")!!.toFloat()
//            ( btnAED.text.toString().replace("AED","")
//            .replace(" ","")).toFloat()
        map["payment_method"] = if (isCredit && isCard) 5 else if (isCredit && isCash) 4 else if (isCard) 2 else 1
        if (etPromoCode.text.isNotEmpty()){
            map["coupon_code"] = etPromoCode.text.toString()
        }
        if (isCredit){
            map["credits"] = arguments!!.getInt("credits")
        }else{
            map["credits"] =0
        }
        laundryViewModel.payment(activity!!,map)
    }

    override fun setLayoutToInsert(): Int {
        return R.layout.fragment_payment
    }

    override fun onDestroy() {
        super.onDestroy()
        activity!!.unregisterReceiver(payment)
    }
}