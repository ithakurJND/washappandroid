package com.ibox.washapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.ibox.washapp.R
import kotlinx.android.synthetic.main.fragment_order.*

class OrderFragment : BaseFragment() {

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        tabLayout.addTab(tabLayout.newTab().setText("New Orders"))
        tabLayout.addTab(tabLayout.newTab().setText("History"))
        nonSwipableViewPager.adapter = tabAddapter(childFragmentManager)
        tabLayout.setupWithViewPager(nonSwipableViewPager)


    }
    inner class tabAddapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        override fun getPageTitle(position: Int): CharSequence? {
            return if (position ==0) "New Orders" else "History"
        }
        override fun getItem(position: Int): Fragment {
            return if (position == 0) {
                NewOrderFragment()
            }  else  {
                HistoryFragment()
            }
        }
        override fun getCount(): Int {
            return 2
        }
    }

    override fun setLayoutToInsert(): Int {
        return R.layout.fragment_order
    }
}