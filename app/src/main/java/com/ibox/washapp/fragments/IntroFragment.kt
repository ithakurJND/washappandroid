package com.ibox.washapp.fragments

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.rd.animation.type.AnimationType
import com.ibox.washapp.R
import com.ibox.washapp.utils.CommonMethods
import kotlinx.android.synthetic.main.frgment_intro.*

/*Introduction fragment where other intro fragment are set in viewpager*/

class IntroFragment : BaseFragment() {
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewPager.currentItem = 0
        btnNext.setOnClickListener()
        {
            if (viewPager.currentItem < 2) {
                viewPager.currentItem = viewPager.currentItem + 1
            } else
                CommonMethods.addFragmentwithBackStack(
                    fragmentManager!!
                    , AfterIntroAddressDetailsFragment()
                )
        }
        tvSkip.setOnClickListener()
        {
            CommonMethods.addFragmentwithBackStack(
                fragmentManager!!
                , AfterIntroAddressDetailsFragment()
            )
        }
        viewPager.offscreenPageLimit = 3
        viewPager.adapter = TabAdapter(childFragmentManager)
        pageIndicatorView.setAnimationType(AnimationType.WORM)
        pageIndicatorView.selectedColor = ContextCompat.getColor(activity!!,R.color.textColor)
        pageIndicatorView.unselectedColor = ContextCompat.getColor(activity!!,R.color.browser_actions_bg_grey)

        pageIndicatorView.setViewPager(viewPager)

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int, positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                if (viewPager.currentItem == 0) {
                    tvSkip.visibility = View.VISIBLE
                    btnNext.text = "NEXT"
                } else if (viewPager.currentItem == 1) {
                    tvSkip.visibility = View.VISIBLE
                    btnNext.text = "NEXT"
                } else if (viewPager.currentItem == 2) {
                    tvSkip.visibility = View.INVISIBLE
                    btnNext.text = "GET STARTED"
                }
            }
        })
    }



    inner class TabAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        override fun getItem(position: Int): Fragment {
            return FirstIntroFragment(position)
        }

        override fun getCount(): Int {
            return 3
        }
    }

    override fun setLayoutToInsert(): Int {
        return R.layout.frgment_intro
    }
}

