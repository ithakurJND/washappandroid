package com.ibox.washapp.fragments


import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.iid.FirebaseInstanceId
import com.ibox.washapp.R
import com.ibox.washapp.activities.*
import com.ibox.washapp.viewModels.CommonViewModel
import kotlinx.android.synthetic.main.fragmet_more.*


class MoreFragment : BaseFragment(), View.OnClickListener {
    var commonViewModel = CommonViewModel()

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.tvProfile -> {
                val intent = Intent(activity!!, com.ibox.washapp.activities.OderProfileAddressActivity::class.java)
                intent.putExtra("Selected", "Profile")
                startActivity(intent)
            }
            R.id.tvLogout -> {
                val builder = AlertDialog.Builder(activity)
                builder.setTitle("Logout?")
                builder.setMessage("Are you sure you want to logout?")
                builder.setPositiveButton("YES") { dialog, which ->
                    commonViewModel.logout(FirebaseInstanceId.getInstance().token!!,activity!!)

                }

                builder.setNegativeButton("No") { dialog, which ->
                    dialog.dismiss()
                }
                val dialog: AlertDialog = builder.create()
                dialog.setOnShowListener {
                    dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
                        .setTextColor(resources.getColor(R.color.drakBlue))
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                        .setTextColor(resources.getColor(R.color.drakBlue))
                    dialog.getButton(AlertDialog.BUTTON_NEUTRAL)
                        .setTextColor(resources.getColor(R.color.drakBlue))
                }
                dialog.show()
            }
            R.id.tvAddress -> {
                val intent = Intent(activity!!, com.ibox.washapp.activities.OderProfileAddressActivity::class.java)
                intent.putExtra("Selected", "Address")
                startActivity(intent)
            }
            R.id.tvOrder -> {
                val intent = Intent(activity!!, com.ibox.washapp.activities.OderProfileAddressActivity::class.java)
                intent.putExtra("Selected", "Order")
                startActivity(intent)
            }
            R.id.tvNotification -> {
                val intent = Intent(activity!!, com.ibox.washapp.activities.OderProfileAddressActivity::class.java)
                intent.putExtra("Selected", "Notification")
                startActivity(intent)
            }

            R.id.tvAbout ->{
               // openLink("https://washapp.ae/#about")
                startActivity(Intent(activity,WebViewActivity::class.java)
                    .putExtra("link","https://washapp.ae/about-us/")
                    .putExtra("title","About"))
            }
            R.id.tvHowitWorks->{
               // openLink("http://washapp.ae/#how-works")
                startActivity(Intent(activity,WebViewActivity::class.java)
                    .putExtra("link","https://washapp.ae/how-it-works/")
                    .putExtra("title","How it works"))
            }
            R.id.tvPrivacyPolicy->{
                //openLink("http://washapp.ae/privacy-policy/")
                startActivity(Intent(activity,WebViewActivity::class.java)
                    .putExtra("link","https://washapp.ae/privacy-policy/")
                    .putExtra("title","Privacy Policy"))
            }
            R.id.tvtermsConditions ->{
              //  openLink("http://washapp.ae/terms-conditions/")
                startActivity(Intent(activity,WebViewActivity::class.java)
                    .putExtra("link","https://washapp.ae/terms-conditions/")
                    .putExtra("title","Terms and Conditions"))
            }
            R.id.tvContactUs ->{
                startActivity(Intent(activity,
                    com.ibox.washapp.activities.ContactUsActivity::class.java))
            }
        }
    }

    fun openLink(link:String){
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(link)
        startActivity(i)
    }

    override fun setLayoutToInsert(): Int {
        return R.layout.fragmet_more
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel::class.java)
        commonViewModel.getEditProfile().observe(this, Observer { logout ->
            if (logout.message == "Logged out Successfully") {
                com.ibox.washapp.utils.Prefs.with(activity).removeAll(activity)
                startActivity(Intent(activity, com.ibox.washapp.activities.LoginActivity::class.java))
                activity!!.finish()
            }
        })
        tvProfile.setOnClickListener(this)
        tvContactUs.setOnClickListener(this)
        tvLogout.setOnClickListener(this)
        tvAddress.setOnClickListener(this)
        tvOrder.setOnClickListener(this)
        tvAbout.setOnClickListener(this)
        tvtermsConditions.setOnClickListener(this)
        tvHowitWorks.setOnClickListener(this)
        tvPrivacyPolicy.setOnClickListener(this)
        tvNotification.setOnClickListener(this)
    }
}