package com.ibox.washapp.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.os.SystemClock;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.ibox.washapp.R;


public class DatePickerFragment extends DialogFragment {
    DatePickerDialog.OnDateSetListener ondateSet;
    private int year, month, day;
    private long time;
    private boolean isMin = false;

    public DatePickerFragment() {
        time = SystemClock.currentThreadTimeMillis();
    }

    public void setCallBack(DatePickerDialog.OnDateSetListener ondate) {
        ondateSet = ondate;
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        year = args.getInt("year");
        month = args.getInt("month");
        day = args.getInt("day");
        time = args.getLong("minTime");
        if (args.containsKey("isMin")) {
            isMin = args.getBoolean("isMin");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), R.style.DialogTheme, ondateSet, year, month, day);
        try {
            dialog.getDatePicker().setMinDate(time);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        return dialog;
    }
} 