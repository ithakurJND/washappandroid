package com.ibox.washapp.fragments

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ibox.washapp.R
import com.ibox.washapp.model.OrderData
import com.ibox.washapp.utils.CommonFunctions
import com.ibox.washapp.utils.CommonMethods
import com.ibox.washapp.utils.Constants
import com.ibox.washapp.viewModels.LaundryViewModel
import kotlinx.android.synthetic.main.fragment_select_service.*

class SelectServicesFragment : BaseFragment() {
    private var isClicked  = false
    private var laundryViewModel = LaundryViewModel()
    override fun setLayoutToInsert(): Int {
        return R.layout.fragment_select_service
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Log.i("CallFor","OnCreated items list")
        intialize()
        laundryViewModel = ViewModelProviders.of(this).get(LaundryViewModel::class.java)
        laundryViewModel.createOrder().observe(this, Observer {
            if (isClicked) {
                isClicked = false
                val arguments = Bundle()
                arguments.putInt("id", it.id)
                arguments.putBoolean("isFromBasket", true)
                val fragment = LaundryListFragment()
                fragment.arguments = arguments
                CommonMethods.replaceFragmentWithBackStack(
                    fragmentManager!!,
                    fragment

                )
            }
        })

    }

    private fun createOrder() {
        val data = com.ibox.washapp.utils.Prefs.with(context).getObject(Constants.ORDER_DATABASKET, OrderData::class.java)
        if (data != null) {
            data.service_type = com.ibox.washapp.utils.Prefs.with(activity).getInt("serviceType", 1)
            data.order_type = 2
            com.ibox.washapp.utils.Prefs.with(activity).save(Constants.ORDER_DATA, data)
            val map = HashMap<String, Any>()
            map["service_type"] = data.service_type
            map["order_type"] = 2
            val list: MutableList<HashMap<String, Any>> = mutableListOf()
            for (sa in data.product!!.values) {
                val mapa = HashMap<String, Any>()
                mapa["product_id"] = sa.id
                mapa["quantity"] = sa.quantity
                mapa["regular_price"] = sa.regular_price
                mapa["urgent_price"] = sa.urgent_price
                list.add(mapa)
            }
            map["order_items"] = list
            isClicked =true
            laundryViewModel.createBasketOrder(activity!!, map)
        } else {
            Toast.makeText(activity, "Please add product in cart", Toast.LENGTH_SHORT).show()
        }
    }

    private fun intialize() {
        com.ibox.washapp.utils.Prefs.with(activity).remove(Constants.ORDER_DATA)
        if (arguments != null && arguments!!.containsKey("isFromBasket")) {
            rvMain.visibility = View.GONE
            rvSecondary.visibility = View.VISIBLE
            tvRegularPrice.text = "AED "+CommonFunctions.getBasketTotal(activity!!,1)
            tvUrgentPrice.text = "AED "+CommonFunctions.getBasketTotal(activity!!,2)
            tvUrgentQuant.text = ""+CommonFunctions.getBasketQuant(activity!!)+" Items"
            tvRegularQuant.text = ""+CommonFunctions.getBasketQuant(activity!!)+" Items"
        }else{
            rvMain.visibility = View.VISIBLE
            rvSecondary.visibility = View.GONE
        }
        cardViewRegular.setOnClickListener()
        {
            com.ibox.washapp.utils.Prefs.with(activity).save("serviceType", 1)
            if (arguments != null && arguments!!.containsKey("isFromBasket")) {
                createOrder()
            } else
                CommonMethods.replaceFragmentWithBackStack(fragmentManager!!, LaundryListFragment())
        }
        cardViewUrgent.setOnClickListener()
        {
            com.ibox.washapp.utils.Prefs.with(activity).save("serviceType", 2)
            if (arguments != null && arguments!!.containsKey("isFromBasket")) {
                createOrder()
            } else
                CommonMethods.replaceFragmentWithBackStack(fragmentManager!!, LaundryListFragment())

        }

        cvSecondaryRegular.setOnClickListener()
        {
            com.ibox.washapp.utils.Prefs.with(activity).save("serviceType", 1)
            if (arguments != null && arguments!!.containsKey("isFromBasket")) {
                createOrder()
            } else
                CommonMethods.replaceFragmentWithBackStack(fragmentManager!!, LaundryListFragment())
        }
        cvSecondaryUrgent.setOnClickListener()
        {
            com.ibox.washapp.utils.Prefs.with(activity).save("serviceType", 2)
            if (arguments != null && arguments!!.containsKey("isFromBasket")) {
                createOrder()
            } else
                CommonMethods.replaceFragmentWithBackStack(fragmentManager!!, LaundryListFragment())

        }

        ivBackArrow.setOnClickListener()
        {
            activity!!.onBackPressed()
        }
    }
}