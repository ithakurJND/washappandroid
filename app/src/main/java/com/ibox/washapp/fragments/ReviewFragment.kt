package com.ibox.washapp.fragments

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ListPopupWindow
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.ibox.washapp.R
import com.ibox.washapp.adapters.AddedToCartAdapter
import com.ibox.washapp.model.MapData
import com.ibox.washapp.model.OrderData
import com.ibox.washapp.model.OrderItem
import com.ibox.washapp.model.OrderResponeLaundry
import com.ibox.washapp.utils.CommonFunctions
import com.ibox.washapp.utils.CommonMethods
import com.ibox.washapp.utils.Constants
import com.ibox.washapp.viewModels.CommonViewModel
import com.ibox.washapp.viewModels.LaundryViewModel
import kotlinx.android.synthetic.main.fragment_review.*
import java.util.*
import kotlin.Comparator
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class ReviewFragment : BaseFragment(), View.OnClickListener {
    private lateinit var listPopupWindow: ListPopupWindow
    private var isClicked = false
    private var positionsArray: ArrayList<String> = arrayListOf()
    private var positionsArrayMain: ArrayList<com.ibox.washapp.model.PojoPreference> = arrayListOf()
    private var commonViewModel = CommonViewModel()
    private var laundryViewModel = LaundryViewModel()
    private val arrayListAddToCart = ArrayList<com.ibox.washapp.model.Product>()
    private var arrayList2 = ArrayList<OrderItem>()
    private var change = false
    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ivBackArrowReview -> {
                activity!!.onBackPressed()
            }
            R.id.btnContinueAndPay -> {
                editOrder()
            }
            R.id.etPrefrence -> {
                listPopUp(positionsArray, etPrefrence)
                listPopupWindow.show()
            }
            R.id.rlAddedToCartReview -> {
                if (rvAddedToCartReview.visibility == View.GONE) {
                    rvAddedToCartReview.visibility = View.VISIBLE
                    ivUpArrowReview.visibility = View.GONE
                    ivDropDownReview.visibility = View.VISIBLE
                } else {
                    rvAddedToCartReview.visibility = View.GONE
                    ivUpArrowReview.visibility = View.VISIBLE
                    ivDropDownReview.visibility = View.GONE
                }
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        ivBackArrowReview.setOnClickListener(this)
        btnContinueAndPay.setOnClickListener(this)
        etPrefrence.setOnClickListener(this)
        rlAddedToCartReview.setOnClickListener(this)

        if (arguments != null && arguments!!.get("isFrom") == "ReviewInComplete") {
            Log.i("isFrom", "" + arguments!!.get("isFrom"))
            tvPickUpAddress.text = arguments!!.getString("pickupAddress")
            tvDropOffAddress.text = arguments!!.getString("deliveryAddress")
            Log.i("deliveryDate", "" + arguments!!.getString("deliveryDate"))
            tvDropOffTime.text = CommonFunctions.formatDate(
                "yyyy-MM-dd'T'HH:mm:ss.sss'Z'",
                "dd-MMM",
                arguments!!.getString("deliveryDate")!!
            ) + ", " + arguments!!.getString("TimeDrop")
            tvPickUpTime.text = CommonFunctions.formatDate(
                "yyyy-MM-dd'T'HH:mm:ss.sss'Z'",
                "dd-MMM",
                arguments!!.getString("pickupDate")!!
            ) + ", " + arguments!!.getString("TimePick")
        } else {
            Log.i("Address","in else")
            tvPickUpAddress.text = arguments!!.getString("pickupAddress")
            tvDropOffAddress.text = arguments!!.getString("deliveryAddress")
            tvPickUpTime.text = arguments!!.getString("pickupDate")
            tvDropOffTime.text = arguments!!.getString("deliveryDate")
        }
        laundryViewModel = ViewModelProviders.of(this).get(LaundryViewModel::class.java)

        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel::class.java)
        commonViewModel.getPrefernces().observe(this, Observer {
            positionsArrayMain.clear()
            positionsArray.clear()
            positionsArrayMain.addAll(it)
            for (sa in it) {
                positionsArray.add(sa.preferrence)
            }
        })
        commonViewModel.preferences(activity!!)

        laundryViewModel.createOrderMutableLiveData.observe(this, androidx.lifecycle.Observer {
            if (isClicked) {
                isClicked = false
                var isFromBasket = false
                if (arguments != null && arguments!!.containsKey("isFromBasket")
                    && arguments!!.getBoolean("isFromBasket")
                ) {
                    isFromBasket = true
                }
                val arguments = Bundle()
                arguments.putBoolean("isFromBasket", isFromBasket)
                arguments.putInt("id", it.id)
                arguments.putString("amount", it.amount.toString())
                arguments.putInt("credits", if (it.credits != null) it.credits else 0)
                val fragment = PaymentFragment()
                fragment.arguments = arguments
                CommonMethods.replaceFragmentWithBackStack(
                    fragmentManager!!,
                    fragment

                )
            }
        })
        if (arguments != null && arguments!!.containsKey("isFromBasket")
            && arguments!!.getBoolean("isFromBasket")
        ) {
            change = true
            val data2 = com.ibox.washapp.utils.Prefs.with(context)
                .getObject(Constants.NEW_RESPONSE_USER_DATA, OrderResponeLaundry::class.java)
            //  Log.i("DATAS", "DATAS" + Gson().toJson(data2))

            arrayList2.clear()
            arrayList2.addAll(data2.order_items)
            Collections.sort(arrayList2, Comparator<OrderItem> { obj1, obj2 ->
                // ## Ascending order
                obj1.product_name.compareTo(obj2.product_name, true) // To compare string values
                // return Integer.valueOf(obj1.empId).compareTo(Integer.valueOf(obj2.empId)); // To compare integer values
                // ## Descending order
                // return obj2.firstName.compareToIgnoreCase(obj1.firstName); // To compare string values
                // return Integer.valueOf(obj2.empId).compareTo(Integer.valueOf(obj1.empId)); // To compare integer values
            })
            rvAddedToCartReview.adapter =
                AddedToCartAdapter(arrayList2, arrayListAddToCart, activity!!, change, 0)
            tvAddedNo.text = arrayList2.size.toString()

        } else if (arguments != null && arguments!!.getString("isFrom") == "ReviewInComplete") {
            val data = com.ibox.washapp.utils.Prefs.with(context)
                .getObject(Constants.ORDER_ITEMS_COMP_ORDER, com.ibox.washapp.model.PojoOrderDetails::class.java)
            Log.i("DATASS", "" + Gson().toJson(data))
            change = false

            arrayListAddToCart.clear()
            arrayListAddToCart.addAll(data.order_items)
            Collections.sort(arrayListAddToCart, Comparator<com.ibox.washapp.model.Product> { obj1, obj2 ->
                // ## Ascending order
                obj1.product_name.compareTo(obj2.product_name, true) // To compare string values
                // return Integer.valueOf(obj1.empId).compareTo(Integer.valueOf(obj2.empId)); // To compare integer values
                // ## Descending order
                // return obj2.firstName.compareToIgnoreCase(obj1.firstName); // To compare string values
                // return Integer.valueOf(obj2.empId).compareTo(Integer.valueOf(obj1.empId)); // To compare integer values
            })
            rvAddedToCartReview.adapter =
                AddedToCartAdapter(arrayList2, arrayListAddToCart, activity!!, change, 1)
            tvAddedNo.text = data.order_items.size.toString()
        } else {
            change = false
            val data = com.ibox.washapp.utils.Prefs.with(context).getObject(Constants.ORDER_DATA, OrderData::class.java)
            //    Log.i("DATAS", "" + Gson().toJson(data.product))
            arrayListAddToCart.clear()
            arrayListAddToCart.addAll(data.product!!.values)
            Collections.sort(arrayListAddToCart, Comparator<com.ibox.washapp.model.Product> { obj1, obj2 ->
                // ## Ascending order
                obj1.name.compareTo(obj2.name, true) // To compare string values
                // return Integer.valueOf(obj1.empId).compareTo(Integer.valueOf(obj2.empId)); // To compare integer values
                // ## Descending order
                // return obj2.firstName.compareToIgnoreCase(obj1.firstName); // To compare string values
                // return Integer.valueOf(obj2.empId).compareTo(Integer.valueOf(obj1.empId)); // To compare integer values
            })
            rvAddedToCartReview.adapter =
                AddedToCartAdapter(arrayList2, arrayListAddToCart, activity!!, change, 0)
            tvAddedNo.text = arrayListAddToCart.size.toString()
        }
        rvAddedToCartReview.layoutManager = LinearLayoutManager(activity)
    }

    private fun editOrder() {
        var map = HashMap<String, Any>()
        if (arguments != null && arguments!!.getString("isFrom") == "ReviewInComplete") {
            Log.i("REVIEW","IN")
            val data = com.ibox.washapp.utils.Prefs.with(context)
                .getObject(Constants.ORDER_ITEMS_COMP_ORDER, com.ibox.washapp.model.PojoOrderDetails::class.java)
            map["pickup_date"] = data.pickup_date
            map["drop_off_date"] = data.drop_off_date
            map["pickup_time_slot"] = data.pickup_time_slot
            map["drop_off_time_slot"] = data.drop_off_time_slot
            map["pickup_address"] = data.pickup_address
            map["dropoff_address"] = data.dropoff_address
            map["pickup_address_id"] = data.pickup_address_id
            map["drop_off_address_id"] = data.drop_off_address_id
            map["preferrence_id"] = prefsId
            map["special_note"] = etSpecialNote.text.toString()
            map["order_id"] = data.id
            map["laundry_id"] = data.laundry_id
            map["status"] = data.status
            val list: MutableList<HashMap<String, Any>> = mutableListOf()
            for (sa in data.order_items) {
                val mapa = HashMap<String, Any>()
                mapa["product_id"] = sa.product_id
                mapa["quantity"] = sa.quantity
                mapa["regular_price"] = sa.regular_price
                mapa["urgent_price"] = sa.urgent_price
                list.add(mapa)
            }
            map["order_items"] = list
        } else {
            Log.i("REVIEW","OUT")
            val de = com.ibox.washapp.utils.Prefs.with(activity).getObject("dataMapOrder123", MapData::class.java)
            map = de.map
            Log.i("Review", "" + map)
            map["preferrence_id"] = prefsId
            map["special_note"] = etSpecialNote.text.toString()
        }
        isClicked = true
        if (arguments != null && arguments!!.containsKey("isFromBasket")
            && arguments!!.getBoolean("isFromBasket")
        ) {
            laundryViewModel.editBasketOrder(activity!!, map)
        } else {
            laundryViewModel.editOrder(activity!!, map)
        }
    }


    private var prefsId = 0
    fun listPopUp(arrayList: ArrayList<String>, view: TextView) {
        listPopupWindow = ListPopupWindow(activity!!)
        listPopupWindow.setAdapter(
            ArrayAdapter<String>(activity!!, R.layout.item_popup_list, arrayList)
        )
        listPopupWindow.setOnItemClickListener { p0, p1, position, p3 ->
            view.text = positionsArray.get(position)
            prefsId = positionsArrayMain[position].id
            listPopupWindow.dismiss()
        }
        listPopupWindow.isModal = true
        listPopupWindow.anchorView = view
    }

    override fun setLayoutToInsert(): Int {
        return R.layout.fragment_review
    }
}