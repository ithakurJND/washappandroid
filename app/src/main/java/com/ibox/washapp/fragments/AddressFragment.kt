package com.ibox.washapp.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.ibox.washapp.R
import com.ibox.washapp.activities.AfterSignupAddressDefaultActivity
import com.ibox.washapp.adapters.AddressAdapter
import com.ibox.washapp.viewModels.CommonViewModel
import kotlinx.android.synthetic.main.fragment_address.*

/*
* fragment to show address which are added*/
class AddressFragment :BaseFragment(){
       var commonViewModel = CommonViewModel()
       override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        intialize()
        btnAddNewAddress.setOnClickListener()
        {
            btnAddNewAddress.isEnabled = false
            startActivity(Intent(activity,
                AfterSignupAddressDefaultActivity::class.java)
                .putExtra("isFromAddress",true))
        }
    }

       private fun intialize() {
           rvAddress.layoutManager = LinearLayoutManager(activity)
           commonViewModel = ViewModelProviders.of(this).get(CommonViewModel::class.java)
           commonViewModel.getUserData().observe(this, Observer { user ->
               Log.i("GetUser", "" + user)
               if (user.addresses.size>0){
                   noAddress.visibility = View.GONE
               }else{
                   noAddress.visibility = View.VISIBLE
               }
                rvAddress.adapter = AddressAdapter(user.addresses,activity!!)
           })

       }

       override fun setLayoutToInsert(): Int {
        return R.layout.fragment_address
    }

       override fun onResume() {
           super.onResume()
           btnAddNewAddress.isEnabled = true
           commonViewModel.getUser(activity!!)
       }
}