package com.ibox.washapp.fragments

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.ibox.washapp.R
import com.ibox.washapp.activities.ViewAllActivity
import com.ibox.washapp.adapters.SelectItemsRvHorizontalAdapter
import com.ibox.washapp.adapters.SelectItemsRvVerticalAdapter
import com.ibox.washapp.model.LaundryList
import com.ibox.washapp.model.OrderData
import com.ibox.washapp.utils.*
import com.ibox.washapp.viewModels.LaundryViewModel
import kotlinx.android.synthetic.main.framgent_select_items.*


/*  This fragment is used to set horizontal and vertical recycler view
    and provide shop detail and services
* */
class SelectItemsFragment : BaseFragment() {
    private var laundryViewModel = LaundryViewModel()
    private val serivcesArrayList: ArrayList<com.ibox.washapp.model.PojoOrderCategory> = ArrayList()
    private var namesArrayList: ArrayList<com.ibox.washapp.model.Product> = ArrayList()
    private var namesArrayListPaging: ArrayList<com.ibox.washapp.model.Product> = ArrayList()
    private var isClicked = false
    var OrderId = 0
    var page = 0
    var isLoading = false
    val mapPaging = HashMap<String, Int>()

    override fun setLayoutToInsert(): Int {
        return R.layout.framgent_select_items
    }

    /*Observer form Vertical Recyclerview*/
    internal var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val clicked = intent.getStringExtra("clicked")
            if (clicked == "plus") {
                if (CommonFunctions.addProductCount(activity!!)!! > 0)
                    btnSelectItemsContinue.visibility = View.VISIBLE
                else
                    btnSelectItemsContinue.visibility = View.GONE
            }
        }
    }

    /*Observer form horizontal Recyclerview*/
    private var clicked = 0
    internal var broadcastReceiverProducts: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            clicked = intent.getIntExtra("ListOfProducts", 0)
            namesArrayList.clear()
            namesArrayList.addAll(serivcesArrayList[clicked].products)
            page=0
            mapPaging["category_id"] = serivcesArrayList[clicked].id
            Log.i("IIDD",""+mapPaging.get("category_id"))
            rvSelectItemsVertical.adapter = SelectItemsRvVerticalAdapter(
                namesArrayList, activity!!
                , activity as AppCompatActivity
            )
            rvSelectItemsVertical.adapter!!.notifyDataSetChanged()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Log.i("CallFor","OnCreated selectitem list")
        val layoutManger = LinearLayoutManager(activity)
        val intentFilter = IntentFilter("Plus")
        activity!!.registerReceiver(broadcastReceiver, intentFilter)

        val intentFilter1 = IntentFilter("ProductList")
        activity!!.registerReceiver(broadcastReceiverProducts, intentFilter1)


        ivBackArrow.setOnClickListener()
        {
            activity!!.onBackPressed()
        }
        rvSelectItemsHorizontal.layoutManager = GridLayoutManager(activity,2)
        rvSelectItemsVertical.layoutManager = layoutManger

        /* observer for pagination */
        laundryViewModel = ViewModelProviders.of(this).get(LaundryViewModel::class.java)
        laundryViewModel.getPaging().observe(this, Observer {
            Log.i("PAGINATION", "PRODUCT  :  " + Gson().toJson(it))
            pbBarSelectItems.visibility = View.GONE
            isLoading = false
            Log.i("PAGEE", "" + it)
            serivcesArrayList[clicked].products.addAll(it)
            namesArrayList.addAll(it)
            rvSelectItemsVertical.adapter!!.notifyDataSetChanged()
        })


        rvSelectItemsHorizontal.adapter =
            SelectItemsRvHorizontalAdapter(serivcesArrayList, activity!!)
        rvSelectItemsVertical.adapter = SelectItemsRvVerticalAdapter(
            namesArrayList, activity!!
            , activity as AppCompatActivity
        )
        if (serivcesArrayList.size == 0) {
            laundryViewModel.getProducts().observe(this, Observer {
                if(it.isNotEmpty()) {
                    mapPaging["category_id"] = it[0].id
                    namesArrayList.clear()
                    serivcesArrayList.addAll(it)
                    rvSelectItemsHorizontal.adapter?.notifyDataSetChanged()
                    if (it.isNotEmpty()) {
                        namesArrayList.addAll(it[0].products)
                    }
                    rvSelectItemsVertical.adapter?.notifyDataSetChanged()
                }
            })
        } else {
            btnSelectItemsContinue.visibility = View.VISIBLE
        }

        val shopName = Gson().fromJson(arguments!!.getString("ShopName"), LaundryList::class.java)
        tvShopName.text = shopName.name
        if (!shopName.logo.isNullOrEmpty())
        Glide.with(activity!!).load(Constants.BASE_URL+shopName.logo).into(ivImage)
        else
            Glide.with(activity!!).load(R.drawable.app_logo_placeholder).into(ivImage)
        if (shopName.badge != null) {
            ivbadge.setImageURI(Uri.parse(Constants.BASE_URL + shopName.badge), activity)
        }

        var s = ""
        for (ia in shopName.categories!!) {
            s = if (s == "") {
                ia.name
            } else {
                s + ", " + ia.name
            }
        }
        tvDescription.text = s
        ratingBar.rating = shopName.rating
        if (serivcesArrayList.size == 0)
            laundryViewModel.productListing(activity!!, arguments!!.getInt("ShopID", 0))
        laundryViewModel.createOrder().observe(this, Observer {
            if (isClicked) {
                isClicked = false
                OrderId = it.id
                val arguments = Bundle()
                arguments.putInt("id", it.id)
                val fragment = PickAndDeliveryFragment()
                fragment.arguments = arguments
                CommonMethods.replaceFragmentWithBackStack(
                    fragmentManager!!,
                    fragment

                )
            }

        })

        tvViewAll.setOnClickListener {
            startActivity(
                Intent(activity!!, ViewAllActivity::class.java)
                    .putExtra("ShopID", arguments!!.getInt("ShopID", 0))
            )
        }

        btnSelectItemsContinue.setOnClickListener()
        {
            val data = com.ibox.washapp.utils.Prefs.with(context).getObject(Constants.ORDER_DATA, OrderData::class.java)
            if (data != null) {
                data.laundry_id = arguments!!.getInt("ShopID", 0)
                data.service_type = com.ibox.washapp.utils.Prefs.with(activity).getInt("serviceType", 1)
                data.order_type = 1
                com.ibox.washapp.utils.Prefs.with(activity).save(Constants.ORDER_DATA, data)
                val map = HashMap<String, Any>()
                map["laundry_id"] = data.laundry_id
                map["service_type"] = data.service_type
                map["order_type"] = data.order_type
                map["area_id"] = com.ibox.washapp.utils.Prefs.with(activity).getInt(Constants.AREA_ID, 0)
                val list: MutableList<HashMap<String, Any>> = mutableListOf()
                for (sa in data.product!!.values) {
                    val mapa = HashMap<String, Any>()
                    mapa["product_id"] = sa.id
                    mapa["quantity"] = sa.quantity
                    mapa["regular_price"] = sa.regular_price
                    mapa["urgent_price"] = sa.urgent_price
                    list.add(mapa)
                }
                map["order_items"] = list
                isClicked = true
                laundryViewModel.createOrder(activity!!, map)
            } else {
                Toast.makeText(activity, "Please add product in cart", Toast.LENGTH_SHORT).show()
            }
        }
      //  setScrollListener(layoutManger)

        var pastVisiblesItems = 0
        var visibleItemCount: Int = 0
        var totalItemCount: Int = 0
        var page: Int = 0
        rvSelectItemsVertical.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = layoutManger.getChildCount()
                    totalItemCount = layoutManger.getItemCount()
                    pastVisiblesItems = layoutManger.findFirstVisibleItemPosition()
                    if (!isLoading) {
                        if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
                            page += 1
                            isLoading = true
                            pbBarSelectItems.visibility = View.VISIBLE
                            mapPaging["laundry_id"] = arguments!!.getInt("ShopID", 0)
                            mapPaging["page"] = page
                            Log.i("LLL", "" + mapPaging.get("category_id"))
                            laundryViewModel.productPaging(activity!!, mapPaging)
                        }
                    }
                }
            }
        })
    }



    private fun setScrollListener(layoutManger: LinearLayoutManager) {

//        rvSelectItemsVertical.addOnScrollListener(object : PaginationScrollListener(layoutManger) {
//            override fun isLastPage(): Boolean {
//                return false
//            }
//
//            override fun isLoading(): Boolean {
//                return isLoading
//            }
//
//            override fun loadMoreItems() {
//                page += 1
//                isLoading = true
//                pbBarSelectItems.visibility = View.VISIBLE
//                mapPaging["laundry_id"] = arguments!!.getInt("ShopID", 0)
//                mapPaging["page"] = page
//                Log.i("LLL", "" + mapPaging.get("category_id"))
//                laundryViewModel.productPaging(activity!!, mapPaging)
//            }
//        })
    }

    override fun onDestroy() {
        super.onDestroy()
        activity!!.unregisterReceiver(broadcastReceiver)
        activity!!.unregisterReceiver(broadcastReceiverProducts)
    }
}