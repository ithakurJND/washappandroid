package com.ibox.washapp.fragments

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ibox.washapp.R
import com.ibox.washapp.activities.PhoneNumberActivity
import com.ibox.washapp.utils.CommonMethods
import com.ibox.washapp.viewModels.CommonViewModel
import com.ibox.washapp.viewModels.ProfileViewModel
import kotlinx.android.synthetic.main.fragment_profile.*

class ProfileFragment() : BaseFragment(), View.OnClickListener {
    var profileViewModel = ProfileViewModel()
    var commonViewModel = CommonViewModel()

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.btnUpdateGeneralInfo -> {
                if (etFullName.text.trim().isNotEmpty()) {
                    profileViewModel.putUser(etFullName.text.toString(), "", activity!!)
                }else{
                    etFullName.error= "Field required"
                    etFullName.requestFocus()
                }
            }
            R.id.btnChangeNum -> {
                val intentq = Intent(activity!!, PhoneNumberActivity::class.java)
                intentq.putExtra("Change", "Address")
                intentq.putExtra("phone", etPhone.text.toString())
                startActivity(intentq)

            }
            R.id.btnUpdatePass -> {
                if (etOldPassword.text.isEmpty()) {
                    etOldPassword.startAnimation(CommonMethods.shakeError())
                    etOldPassword.error = "Old password required"
                    etOldPassword.requestFocus()
                } else if (etNewPassword.text.isEmpty()) {
                    etNewPassword.startAnimation(CommonMethods.shakeError())
                    etNewPassword.error = "New password required"
                    etNewPassword.requestFocus()
                } else if (etConfirmPassword.text.isEmpty()) {
                    etConfirmPassword.startAnimation(CommonMethods.shakeError())
                    etConfirmPassword.error = "Confirm password required"
                    etConfirmPassword.requestFocus()
                } else if (!(etNewPassword.text.matches(Regex(".*[A-Za-z].*")) && etNewPassword.text.matches(Regex(".*[0-9].*")) && etNewPassword.text.matches(Regex("[A-Za-z0-9]*")))
                    || etNewPassword.text.length < 8
                ) {
                    etNewPassword.startAnimation(CommonMethods.shakeError())
                    etNewPassword.error = "Password should be alphanumeric and 8 char"
                    etNewPassword.requestFocus()
                } else if (!etNewPassword.text.toString().equals(etConfirmPassword.text.toString())) {
                    etConfirmPassword.startAnimation(CommonMethods.shakeError())
                    etConfirmPassword.error = "Password does not match"
                    etConfirmPassword.requestFocus()
                } else {
                    profileViewModel.changePass(
                        etOldPassword.text.toString(),
                        etConfirmPassword.text.toString()
                        , activity!!
                    )
                }
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel::class.java)
        commonViewModel.getUserData().observe(this, Observer { user ->
            Log.i("GetUser", "" + user)
            etFullName.text = Editable.Factory.getInstance().newEditable(user.name)
            etEmail.text = Editable.Factory.getInstance().newEditable(user.email)
            etPhone.text = Editable.Factory.getInstance().newEditable(user.phone_no)
            if ((user.fb_id!=null && user.fb_id.isNotEmpty())||(user.google_id!=null && user.google_id.isNotEmpty())){
                cvChangePassword.visibility = View.GONE
            }else{
                cvChangePassword.visibility = View.VISIBLE
            }

        })

        profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel::class.java)

        profileViewModel.showMessage().observe(this, Observer { message ->
            Toast.makeText(activity, "" + message.message, Toast.LENGTH_LONG).show()
            Log.i("Toast", "" + message.message)
        })
        profileViewModel.showChangePass().observe(this, Observer { message ->
            etNewPassword.setText("")
            etConfirmPassword.setText("")
            etOldPassword.setText("")
            Toast.makeText(activity, "" + message.message, Toast.LENGTH_LONG).show()
        })
        btnUpdateGeneralInfo.setOnClickListener(this)
        btnChangeNum.setOnClickListener(this)
        btnUpdatePass.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()
        commonViewModel.getUser(activity!!)
        profileViewModel.getValues(activity!!)
    }

    override fun setLayoutToInsert(): Int {
        return R.layout.fragment_profile
    }
}