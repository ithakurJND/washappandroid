package com.ibox.washapp.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager

abstract class BaseDialogFragment : androidx.fragment.app.DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        return inflater.inflate(setLayoutToInsert(), container, false)
    }

    abstract fun setLayoutToInsert(): Int

/*    override fun onDestroyView() {
        super.onDestroyView()
        try {
            clearFindViewByIdCache()
        } catch (npe: NullPointerException) {

        }
    }*/


}