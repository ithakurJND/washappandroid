package com.ibox.washapp.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.Toast
import com.techautovity.myeve.retrofit.RestClient
import com.ibox.washapp.R
import com.ibox.washapp.utils.CommonFunctions
import com.ibox.washapp.utils.Constants
import kotlinx.android.synthetic.main.dialog_review.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ReviewDialog(
    private val cont: Context,
    private val id: Int,
    val laundryName: String,
    val OrderId: String
) :
    Dialog(cont, R.style.TransparentDilaog) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_review)
        setCancelable(true)
        title.text = "Rate $laundryName"
        tvOrderId.text = "OrderId : $OrderId"
        tvSkip.setOnClickListener { dismissDialogRate() }


        tvContinue.setOnClickListener {
            apiHit()
        }

    }

    private fun apiHit() {
        CommonFunctions.showProgress(cont)
        val map = HashMap<String, Any>()
        map["order_id"] = id
        map["review"] = etFullName.text.toString()
        map["rating"] = tvRating.rating
        RestClient.modalApiService.rateUs(
            com.ibox.washapp.utils.Prefs.with(context).getString(Constants.PREFS_TOKEN, ""),
            map
        ).enqueue(object :
            Callback<com.ibox.washapp.model.PojoSuccess> {
            override fun onFailure(call: Call<com.ibox.washapp.model.PojoSuccess>, t: Throwable) {
                CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(cont, t.message!!)
            }

            override fun onResponse(call: Call<com.ibox.washapp.model.PojoSuccess>, response: Response<com.ibox.washapp.model.PojoSuccess>) {
                CommonFunctions.dismissProgress()
                if (response.isSuccessful) {
                    Toast.makeText(cont, "Review added successfully", Toast.LENGTH_SHORT).show()
                    dismiss()
                } else {
                    CommonFunctions.handleError(cont, response.errorBody(), response.code())
                }
            }

        })
    }

    private fun dismissDialogRate() {
        CommonFunctions.showProgress(cont)
        RestClient.modalApiService.dismissRating(
            com.ibox.washapp.utils.Prefs.with(context).getString(
                Constants.PREFS_TOKEN,
                ""
            )
        ).enqueue(object :
            Callback<com.ibox.washapp.model.PojoSuccess> {
            override fun onFailure(call: Call<com.ibox.washapp.model.PojoSuccess>, t: Throwable) {
                CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(cont, t.message!!)
            }

            override fun onResponse(call: Call<com.ibox.washapp.model.PojoSuccess>, response: Response<com.ibox.washapp.model.PojoSuccess>) {
                CommonFunctions.dismissProgress()
                if (response.isSuccessful) {
                    dismiss()
                } else {
                    CommonFunctions.handleError(cont, response.errorBody(), response.code())


                }
            }

        })
    }


}