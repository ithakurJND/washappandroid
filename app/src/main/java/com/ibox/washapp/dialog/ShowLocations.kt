package com.ibox.washapp.dialog

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.ibox.washapp.R
import com.ibox.washapp.adapters.ShowLocationsAdapter
import com.ibox.washapp.model.Area
import com.ibox.washapp.model.LaundryList
import com.ibox.washapp.model.Locations
import com.ibox.washapp.utils.CommonFunctions
import com.ibox.washapp.utils.CommonMethods
import com.ibox.washapp.viewModels.CommonViewModel
import kotlinx.android.synthetic.main.dialog_fragment_show_locations.*
import kotlinx.android.synthetic.main.fragment_laundry_list.*

class ShowLocations : BaseDialogFragment() {
    private var commonViewModel = CommonViewModel()
    var positionArrayListLatNlg = ArrayList<Area?>()
    var page = 0
    var limit = 20
    private var firstVisibleItem = 0
    private var visibleItemCount = 0
    private var totalItemCount = 0
    var forTo = false
    override fun setLayoutToInsert(): Int {
        return R.layout.dialog_fragment_show_locations
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        val win = dialog!!.window!!.attributes
        win.gravity = Gravity.TOP
        super.onActivityCreated(savedInstanceState)
//        positionArrayListLatNlg.add(
//
//        )
//        Log.i("LocationData", "" + Gson().fromJson(
//            arguments!!.getString("locations"),
//            Locations::class.java
//        ))
       if (arguments?.getBoolean("for") != null)
           forTo = true
        rvLocationsList.layoutManager = LinearLayoutManager(activity)
        rvLocationsList.adapter = ShowLocationsAdapter(activity!!, positionArrayListLatNlg)
        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel::class.java)
        commonViewModel.getLocations().observe(this, Observer { locations ->
            proBar.visibility = View.GONE
            if (page ==0)
            {
                positionArrayListLatNlg.clear()
                positionArrayListLatNlg.addAll(locations.areas)
                rvLocationsList.adapter!!.notifyDataSetChanged()
            }
            else
            {
                if (positionArrayListLatNlg[positionArrayListLatNlg.size - 1] == null) {
                    positionArrayListLatNlg.removeAt(positionArrayListLatNlg.size - 1)
                    rvLocationsList.adapter!!.notifyItemRemoved(positionArrayListLatNlg.size - 1)
                }
                positionArrayListLatNlg.addAll(locations.areas)
                rvLocationsList.adapter!!.notifyDataSetChanged()
            }

        })
        commonViewModel.locations(activity!!,"",0,20)
        proBar.visibility = View.VISIBLE
        activity!!.registerReceiver(selectedLoc, IntentFilter("selectedLocation"))
        etSearch.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                page = 0
                if (s.toString().trim().isNotEmpty())
               commonViewModel.locations(activity!!,s.toString(),0,0)
                else
                    commonViewModel.locations(activity!!,s.toString(),0,20)
            }
        })
        rvLocationsList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                (recyclerView.layoutManager as LinearLayoutManager?)?.let { llManager ->
                    if (dy > 0) {
                        visibleItemCount = recyclerView.childCount
                        totalItemCount = llManager.itemCount
                        firstVisibleItem = llManager.findFirstVisibleItemPosition()

                        if (positionArrayListLatNlg.size % 10 == 0)
                            if (visibleItemCount + firstVisibleItem >= totalItemCount) {
                                page += 1
                                positionArrayListLatNlg.add(null)
                                commonViewModel.locations(activity!!,"",page,20)
                                proBar.visibility = View.GONE
                                rvLocationsList.adapter!!.notifyItemInserted(positionArrayListLatNlg.size - 1)
                            }
                    }
                }
            }
        })
    }

    var selectedLoc = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            activity!!.sendBroadcast(Intent("SelectedLoc")
                .putExtra("for",forTo)
                .putExtra("areaId",intent!!.getIntExtra("areaId",0))
                .putExtra("emirateId",intent!!.getIntExtra("emirateId",0))
                .putExtra("area",intent!!.getStringExtra("area"))
                .putExtra("emirate",intent!!.getStringExtra("emirate"))
                .putExtra("lat",intent!!.getDoubleExtra("lat",0.0))
                .putExtra("lng",intent!!.getDoubleExtra("lng",0.0)))
            Log.i("ShowIDS","on loc  frag ${intent!!.getIntExtra("areaId",0)} ${intent!!.getIntExtra("emirateId",0)}")
            Log.i("ShowLatLan", "on frag  ${intent!!.getDoubleExtra("lat",0.0)} ${intent!!.getDoubleExtra("lng",0.0)}")
            Log.i("Selected","on dialog  "+intent!!.getIntExtra("emirateId",0))
            dialog!!.dismiss()
        }
    }
    override fun onDestroy() {
        super.onDestroy()
        activity!!.unregisterReceiver(selectedLoc)
    }

    override fun onStart() {
        super.onStart()
        val dia = dialog
        if (dia != null) {
            dia.window!!.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
        }
    }

}