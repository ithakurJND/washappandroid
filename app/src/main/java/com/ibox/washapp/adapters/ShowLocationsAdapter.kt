package com.ibox.washapp.adapters

import android.content.Context
import android.content.Intent
import android.location.Location
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ibox.washapp.R
import com.ibox.washapp.model.Area
import com.ibox.washapp.model.Locations
import kotlinx.android.synthetic.main.item_show_locations.view.*

class ShowLocationsAdapter(val context: Context, val locationList: ArrayList<Area?>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    val viewList = 1
    val viewLoader = 2
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == viewList)
            MyViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_show_locations,
                    parent,
                    false
                )
            )
        else
            ProgressViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.veritical_load_data, parent, false
                )
            )
    }

    override fun getItemViewType(position: Int): Int {
        if (locationList[position] == null)
            return viewLoader
        else
            return viewList
    }

    override fun getItemCount(): Int {
        return locationList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder.itemViewType == viewList)
            (holder as MyViewHolder).bindValues(locationList[position])
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindValues(item: Area?) {
            item!!.let {
                itemView.tvLocationNames.text = it.area+","+it.emirate
                var areaId = it.id
                var emirateId = it.emirate_id
                var area = it.area
                var emirate = it.emirate
                var lat = it.lat
                var lng = it.lng

                itemView.rlLayout.setOnClickListener {
                    Log.i("Selected","on adapter  $areaId")
                    Log.i("ShowLatLan", "on adpater $lat $lng")
                    Log.i("ShowIDS","on laundry frag $areaId $emirateId")

                     context.sendBroadcast(Intent("selectedLocation")
                         .putExtra("areaId",areaId)
                         .putExtra("emirateId",emirateId)
                         .putExtra("area",area)
                         .putExtra("emirate",emirate)
                         .putExtra("lat",lat)
                         .putExtra("lng",lng))
                }
            }

        }
    }

    inner class ProgressViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }
}