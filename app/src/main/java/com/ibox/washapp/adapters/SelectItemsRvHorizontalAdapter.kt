package com.ibox.washapp.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ibox.washapp.R
import kotlinx.android.synthetic.main.item_rvhorizontal_select_items.view.*


class SelectItemsRvHorizontalAdapter(
    val items: ArrayList<com.ibox.washapp.model.PojoOrderCategory>,
    val context: Context
) :
    RecyclerView.Adapter<SelectItemsRvHorizontalAdapter.ViewHolder>() {
    var index = 0
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val viewHolder = ViewHolder(
            LayoutInflater.from(context)
                .inflate(R.layout.item_rvhorizontal_select_items, parent, false)
        )
        return viewHolder
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvService.text = items[position].name
        //sendBroadCast(position)
        holder.rvHorizontal.setOnClickListener(View.OnClickListener {
            index = position
            sendBroadCast(position)
        })
        if (index == position) {
            holder.rvHorizontal.setBackgroundResource(R.color.lightBlue)
            holder.tvService.setTextColor(context.resources.getColor(R.color.whiteColor))

        } else {
            holder.rvHorizontal.setBackgroundResource(R.color.whiteColor)
            holder.tvService.setTextColor(context.resources.getColor(R.color.textColor))
        }
    }

    inner class ViewHolder(itemsView: View) : RecyclerView.ViewHolder(itemsView) {
        val tvService = itemsView.tvService
        val rvHorizontal = itemsView.rlHorizontal

    }
    private fun sendBroadCast(pos:Int)
    {
        val intent = Intent("ProductList")
        intent.putExtra("ListOfProducts", pos)
        context.sendBroadcast(intent)
        notifyDataSetChanged()
    }
}