package com.ibox.washapp.adapters

import android.content.Context
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.ibox.washapp.R
import com.ibox.washapp.fragments.LaundryListFragment
import com.ibox.washapp.fragments.SelectItemsFragment
import com.ibox.washapp.model.LaundryList
import com.ibox.washapp.model.OrderData
import com.ibox.washapp.utils.CommonMethods
import com.ibox.washapp.utils.Constants
import com.ibox.washapp.viewModels.LaundryViewModel
import kotlinx.android.synthetic.main.item_laundry_listing.view.*
import java.net.URL


class LaundryListAdapter(
    val item: MutableList<LaundryList>, val context: Context,
    val activity: AppCompatActivity
,val orderID : Int,val model : LaundryViewModel,private var fragmet : LaundryListFragment) :
    RecyclerView.Adapter<LaundryListAdapter.Viewholder>() {
    var selectItemsFragment = SelectItemsFragment()

    override fun onBindViewHolder(holder: Viewholder, position: Int) {
        holder.itemView.tvShopName.text = item[position].name
        var s = ""
        if (item[position].categories!=null) {
            for (ia in item[position].categories!!) {
                s = if (s == "") {
                    ia.name
                } else {
                    s + ", " + ia.name
                }
            }
        }
        holder.itemView.ratingBar.rating = item[position].rating
        holder.itemView.tvDescription.text = s
        Log.i("Image",""+Constants.BASE_URL+item[position].logo)
//        holder.itemView.ivImage.setImageURI(Uri.parse(Constants.BASE_URL+item[position].logo),context)
        if (!item[position].logo.isNullOrEmpty())
        Glide.with(context).load(Constants.BASE_URL+item[position].logo).into(holder.itemView.ivImage)
        else
            Glide.with(context).load(R.drawable.app_logo_placeholder).centerCrop().into(holder.itemView.ivImage)


        // holder.itemView.ivImage.setImageURI(Uri.parse(Constants.BASE_URL+item[position].logo))

        if (item[position].badge!=null){
            holder.itemView.ivbadge.setImageURI(Uri.parse(Constants.BASE_URL+item[position]
                .badge),context)
        }else{
            holder.itemView.ivbadge.setImageURI(Uri.parse(""),context)
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): Viewholder {
        val viewholder = Viewholder(
            LayoutInflater.from(context)
                .inflate(R.layout.item_laundry_listing, parent, false)
        )
        viewholder.init()
        return viewholder
    }

    override fun getItemCount(): Int {
        return item.size
    }

    inner class Viewholder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun init() {
            itemView.cardViewShop.setOnClickListener {
                if (orderID == -1) {
                    val bundle = Bundle()
                    bundle.putString("ShopName", Gson().toJson(item.get(adapterPosition)))
                    bundle.putInt("ShopID", item.get(adapterPosition).id)
                    selectItemsFragment.arguments = bundle
                    CommonMethods.replaceFragmentWithBackStack(
                        activity.supportFragmentManager,
                        selectItemsFragment
                    )
                }else{
                    val map = HashMap<String,Any>()
                    map["order_id"] = orderID
                    map["laundry_id"] = item.get(adapterPosition).id

                    com.ibox.washapp.utils.Prefs.with(context).save(Constants.LAUNDRY_ID,item.get(adapterPosition).id)
                    val data = com.ibox.washapp.utils.Prefs.with(context).getObject(Constants.ORDER_DATA, OrderData::class.java)
                    val list : MutableList<HashMap<String,Any>> = mutableListOf()
                    for (sa in data.product!!.values){
                        val mapa = HashMap<String,Any>()
                        mapa["product_id"] = sa.id
                        mapa["quantity"] = sa.quantity
                        mapa["regular_price"] = sa.regular_price
                        mapa["urgent_price"] = sa.urgent_price
                        list.add(mapa)

                        Log.i("Price","regular "+sa.regular_price)
                        Log.i("Price","urgent "+sa.urgent_price)

                    }
                    map["order_items"] = list
                    fragmet.isClicked = true
                    model.editBasketOrder(activity,map)
                }
            }
        }

    }

}