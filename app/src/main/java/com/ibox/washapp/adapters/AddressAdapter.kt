package com.ibox.washapp.adapters

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.ibox.washapp.R
import com.techautovity.myeve.retrofit.RestClient
import com.ibox.washapp.activities.AfterSignupAddressDefaultActivity
import com.ibox.washapp.model.Address
import com.ibox.washapp.model.EditProfile
import com.ibox.washapp.utils.CommonFunctions
import com.ibox.washapp.utils.Constants
import kotlinx.android.synthetic.main.item_address.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddressAdapter(val items: MutableList<Address>, val context: Context)
    : RecyclerView.Adapter<AddressAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressAdapter.ViewHolder {
        val viewHolder =ViewHolder(
            LayoutInflater.from(context)
                .inflate(R.layout.item_address,parent,false))
        viewHolder.init()
        return viewHolder
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tvHome.text = items[position].title
        /*G01, A1 Majaz Pearl Tower, Sharjah, UAE, 6500*/
        holder.itemView.tvDelete.paintFlags = Paint.UNDERLINE_TEXT_FLAG
        holder.itemView.tvEdit.paintFlags = Paint.UNDERLINE_TEXT_FLAG
        holder.itemView.tvAddress.text = items[position].flat_no+", "+items[position].building_name+", "+items[position].address+", "+items[position].city+", "+items[position].postal_code
        if (items[position].is_default ==1){
            holder.itemView.tvHome.setTextColor(ContextCompat.getColor(context, R.color.textColor))
            holder.itemView.tvAddress.setTextColor(ContextCompat.getColor(context,R.color.textColor))
            holder.itemView.tvDelete.visibility = View.GONE
        }else{
            holder.itemView.tvHome.setTextColor(ContextCompat.getColor(context,R.color.darkBlack))
            holder.itemView.tvAddress.setTextColor(ContextCompat.getColor(context,R.color.lightBlack))

            holder.itemView.tvDelete.visibility = View.VISIBLE
        }
    }
    inner class ViewHolder(itemsView: View): RecyclerView.ViewHolder(itemsView)
    {
        fun init()
        {
            itemView.tvDelete.setOnClickListener {
                logoutDialog("Are you sure you want to delete this address?",adapterPosition)
            }

            itemView.tvEdit.setOnClickListener {
                itemView.tvEdit.isEnabled = false
                context.startActivity(
                    Intent(context, AfterSignupAddressDefaultActivity::class.java)
                        .putExtra("isFromAddress",true)
                        .putExtra("data",Gson().toJson(items[adapterPosition])))
            }
        }
    }

    private fun logoutDialog(messae: String, adapterPosition: Int) {
        val builder = AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle)
        builder.setMessage(messae)
        builder.setPositiveButton("Yes") { _, _ ->

            deleteAccount(adapterPosition)
        }
        builder.setNegativeButton("No") { dialog, _ -> dialog.cancel() }
        builder.setCancelable(false)
        builder.show()
    }
    private fun deleteAccount(adapterPosition: Int) {
        CommonFunctions.showProgress(context)
        RestClient.modalApiService.deleteAddress(
            com.ibox.washapp.utils.Prefs.with(context).getString(Constants.PREFS_TOKEN,""),items[adapterPosition]
            .id).enqueue(object :
            Callback<EditProfile> {
            override fun onFailure(call: Call<EditProfile>, t: Throwable) {
                CommonFunctions.dismissProgress()
                CommonFunctions.handelInternet(context,t.message!!)
            }

            override fun onResponse(call: Call<EditProfile>, response: Response<EditProfile>) {
                CommonFunctions.dismissProgress()
                if (response.isSuccessful) {
                    items.removeAt(adapterPosition)
                    notifyDataSetChanged()
                } else {
                    CommonFunctions.handleError(context,response.errorBody(),response.code())


                }
            }

        })

    }
}