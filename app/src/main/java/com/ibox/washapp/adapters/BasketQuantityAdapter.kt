package com.ibox.washapp.adapters

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ibox.washapp.R
import com.ibox.washapp.utils.CommonFunctions
import com.ibox.washapp.utils.Constants
import kotlinx.android.synthetic.main.item_basket.view.*
import kotlinx.android.synthetic.main.item_rvvertical_select_items.view.*
import kotlinx.android.synthetic.main.item_rvvertical_select_items.view.ivcardViewImage
import kotlinx.android.synthetic.main.item_rvvertical_select_items.view.tvTraditional

class BasketQuantityAdapter(
    val items: MutableList<com.ibox.washapp.model.Product>, val context: Context
) :
    RecyclerView.Adapter<BasketQuantityAdapter.ViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val viewHolder = ViewHolder(
            LayoutInflater.from(context)
                .inflate(R.layout.item_rvvertical_select_items, parent, false)
        )
        viewHolder.init()
        return viewHolder
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tvTraditional.text = items[position].name
        holder.itemView.tvNumOf.text = items[position].quantity.toString()
        holder.itemView.tvProce.text = " "
        if (!items[position].image.isNullOrEmpty())
        {
            Glide.with(context).load(Constants.BASE_URL + items[position].image).into(holder.itemView.ivcardViewImage)}
        else
        {
            Glide.with(context).load(R.drawable.product_placeholder).centerCrop().into(holder.itemView.ivcardViewImage)}
//        holder.itemView.ivcardViewImage.setImageURI(
//            Uri.parse(Constants.BASE_URL + items[position].image),
//            context
//        )
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun init() {
            itemView.ivPlus.setOnClickListener()
            {
                var sa = items[adapterPosition].quantity
                sa += 1
                items[adapterPosition].quantity = sa
                CommonFunctions.addProductBasket(context, items[adapterPosition])
                notifyItemRangeChanged(0, itemCount)
            }
            itemView.ivMinus.setOnClickListener {
                var sa = items[adapterPosition].quantity
                if (sa > 1) {
                    sa -= 1
                    items[adapterPosition].quantity = sa
//                    if (sa ==0)
//                    {items[adapterPosition].quantity = sa}
                }
                CommonFunctions.addProductBasket(context, items[adapterPosition])
                items[adapterPosition].quantity = sa
                notifyItemRangeChanged(0, itemCount)
            }

        }
    }
}
