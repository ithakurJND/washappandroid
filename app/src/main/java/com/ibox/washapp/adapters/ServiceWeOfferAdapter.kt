package com.ibox.washapp.adapters

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.ibox.washapp.R
import com.ibox.washapp.model.Service
import com.ibox.washapp.utils.CommonFunctions
import com.ibox.washapp.utils.Constants
import kotlinx.android.synthetic.main.item_service_we_offers.view.*

class ServiceWeOfferAdapter(
    val item: ArrayList<Service>, val context: Context,
    val activity: AppCompatActivity
) : RecyclerView.Adapter<ServiceWeOfferAdapter.Viewholder>() {
    override fun onBindViewHolder(holder: Viewholder, position: Int) {
        holder.itemView.cardViewServiceWeOffer.layoutParams.width = ((CommonFunctions.getScreenWidth(context)/2.3).toInt())
        holder.itemView.ivService.setImageURI(Uri.parse(Constants.BASE_URL+item[position]
            .image+Constants.IMAGE_FOLDER),context)
        holder.tvService.text = item[position].name
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): Viewholder {
        val viewholder = Viewholder(
            LayoutInflater.from(context)
                .inflate(R.layout.item_service_we_offers, parent, false)
        )

        return viewholder
    }

    override fun getItemCount(): Int {
        return item.size
    }

    inner class Viewholder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val ivService = itemView.ivService
        val tvService = itemView.tvService

    }

}