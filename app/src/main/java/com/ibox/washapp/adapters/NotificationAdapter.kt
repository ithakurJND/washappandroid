package com.ibox.washapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.ibox.washapp.R
import kotlinx.android.synthetic.main.item_notification.view.*

class NotificationAdapter(
    val items: MutableList<com.ibox.washapp.model.Datum>,
    val context: Context, val activity: AppCompatActivity):
    RecyclerView.Adapter<NotificationAdapter.ViewHolder>()
{
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val viewHolder = ViewHolder(LayoutInflater.from(context)
            .inflate(R.layout.item_notification,parent,false))
        return viewHolder
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder:ViewHolder, position: Int) {
        holder.itemView.tvBrightSolution.text = items.get(position).subject
        holder.itemView.tvMessage.text = items[position].notif_msg
        holder.itemView.tvNotificationTime.text = items[position].time_since
    }
    inner  class ViewHolder(itemsView:View):RecyclerView.ViewHolder(itemsView)
    {
    }
}