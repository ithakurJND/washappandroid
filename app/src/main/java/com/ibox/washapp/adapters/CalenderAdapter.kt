package com.ibox.washapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ibox.washapp.R
import com.ibox.washapp.model.Calender
import kotlinx.android.synthetic.main.item_horizontal_calender.view.*
import java.util.*
import kotlin.collections.ArrayList

class CalenderAdapter(val items: ArrayList<Calender>, val context: Context) :
    RecyclerView.Adapter<CalenderAdapter.ViewHolder>() {
    private var index = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CalenderAdapter.ViewHolder {
        val viewHolder = ViewHolder(
            LayoutInflater.from(context)
                .inflate(R.layout.item_horizontal_calender, parent, false))
        return viewHolder
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: CalenderAdapter.ViewHolder, position: Int) {
        val calender: Calender = items[position]
        holder.tvDate.text = calender.date
        holder.tvday.text = calender.day
        holder.tvMonth.text = calender.month
        if (index == position) {
            holder.rl.setBackgroundResource(R.drawable.gradiant_pickup)
            holder.tvMonth.setTextColor(context.resources.getColor(R.color.whiteColor))
            holder.tvDate.setTextColor(context.resources.getColor(R.color.whiteColor))
            holder.tvday.setTextColor(context.resources.getColor(R.color.whiteColor))
        } else {
            holder.rl.setBackgroundResource(R.color.lightGray)
            holder.tvMonth.setTextColor(context.resources.getColor(R.color.lightBlack))
            holder.tvDate.setTextColor(context.resources.getColor(R.color.darkBlack))
            holder.tvday.setTextColor(context.resources.getColor(R.color.lightBlack))
        }

    }

    fun changeIndex(position: Int){
        index = position
        notifyDataSetChanged()
    }

    fun getSelectedDate():String{
        var ss = ""
        val calendar = Calendar.getInstance()
        ss = ""+calendar.get(Calendar.YEAR)+"-"+items[index].mon+"-"+items[index].date
        return ss
    }

    fun getSelectedIndex():Int{
        return index
    }

    fun getSelectedDateShow():String{
        var ss = ""
        ss = items[index].date+"-"+items[index].month
        return ss
    }

    inner class ViewHolder(itemsView: View) : RecyclerView.ViewHolder(itemsView) {
        val tvDate = itemsView.tvDate
        val tvday = itemsView.tvDay
        val tvMonth = itemsView.tvMonth
        val rl = itemsView.rl
    }
}