package com.ibox.washapp.adapters

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ibox.washapp.R
import com.ibox.washapp.utils.CommonFunctions
import com.ibox.washapp.utils.Constants
import kotlinx.android.synthetic.main.item_new_order.view.*

class NewOrderAddapter(
    val items: ArrayList<com.ibox.washapp.model.PojoOrderListing>,
    val context: Context,
    val activity: AppCompatActivity
) : RecyclerView.Adapter<NewOrderAddapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewOrderAddapter.ViewHolder {
        val viewHolder = ViewHolder(
            LayoutInflater.from(context)
                .inflate(R.layout.item_new_order, parent, false)
        )
        viewHolder.init()
        return viewHolder
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: NewOrderAddapter.ViewHolder, position: Int) {
        if (items[position].laundry_name!=null && items[position].laundry_name.isNotEmpty())
            holder.tvLaundryName.text = items[position].laundry_name
        else
            holder.tvLaundryName.text = "Basket Order"
        if (!items.get(position).laundry_logo.isNullOrEmpty())
            Glide.with(context).load(Constants.BASE_URL + items.get(position).laundry_logo).into( holder.itemView.viewImage)
        else
            Glide.with(context).load(R.drawable.app_logo_placeholder).into( holder.itemView.viewImage)


//        holder.itemView.viewImage.setImageURI(
//            Uri.parse(Constants.BASE_URL + items.get(position).laundry_logo),
//            context
//        )
        holder.itemView.tvOrderDate.text = "Order Date " + CommonFunctions.formatDate(
            "yyyy-MM-dd'T'HH:mm:ss.sss'Z'",
            "dd MMM yy",
            items[position].created_at
        )
        holder.itemView.tvOrderId.text = "Order ID: " + items[position].OrderId
        when (items[position].status) {
            0 -> {
                holder.itemView.tvStatus.text = "Hold"
                holder.itemView.tvStatus.background = ContextCompat.getDrawable(context,R.drawable.txt_new_order_background)

            }
            1 -> {
                holder.itemView.tvStatus.text = "Cancelled"
                holder.itemView.tvStatus.background = ContextCompat.getDrawable(context,R.drawable.cancel_status)

            }
            2 -> {
                holder.itemView.tvStatus.background = ContextCompat.getDrawable(context,R.drawable.txt_new_order_background)

                holder.itemView.tvStatus.text = "Failed"
            }
            3 -> {
                holder.itemView.tvStatus.background = ContextCompat.getDrawable(context,R.drawable.txt_new_order_background)

                holder.itemView.tvStatus.text = "Receive the goods"
            }
            4 -> {
                holder.itemView.tvStatus.background = ContextCompat.getDrawable(context,R.drawable.txt_new_order_background)

                holder.itemView.tvStatus.text = "Processing"
            }
            5 -> {
                holder.itemView.tvStatus.background = ContextCompat.getDrawable(context,R.drawable.txt_new_order_background)

                holder.itemView.tvStatus.text = "Ready to pick"
            }
            6 -> {
                holder.itemView.tvStatus.background = ContextCompat.getDrawable(context,R.drawable.txt_new_order_background)

                holder.itemView.tvStatus.text = "Driver picked"
            }
            7 -> {
                holder.itemView.tvStatus.background = ContextCompat.getDrawable(context,R.drawable.txt_new_order_background)

                holder.itemView.tvStatus.text = "Delivered"
            }
            8 -> {
                holder.itemView.tvStatus.background = ContextCompat.getDrawable(context,R.drawable.txt_new_order_background)

                holder.itemView.tvStatus.text = "Completed"
            }
            9 -> {
                holder.itemView.tvStatus.background = ContextCompat.getDrawable(context,R.drawable.txt_new_order_background)

                holder.itemView.tvStatus.text = "Order Reviewed"
            }
        }
    }

    inner class ViewHolder(itemsView: View) : RecyclerView.ViewHolder(itemsView) {
        val tvLaundryName = itemsView.tvLaundryName
        fun init() {
            itemView.cardViewLaundry.setOnClickListener()
            {
                context.startActivity(
                    Intent(activity, com.ibox.washapp.activities.OrderDetailActivity::class.java)
                        .putExtra("id", items[adapterPosition].id)
                )
                //  CommonMethods.replaceFragmentWithBackStack(activity.supportFragmentManager,ViewOderFragment())
            }

        }
    }
}