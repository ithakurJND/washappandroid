package com.ibox.washapp.adapters

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.viewpager.widget.PagerAdapter
import com.ibox.washapp.R
import com.ibox.washapp.model.BannerImg
import com.ibox.washapp.utils.Constants
import kotlinx.android.synthetic.main.item_banner_holder.view.*


class BannerHolderAdapter(val images: ArrayList<BannerImg>, val context: Context) : PagerAdapter() {
    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as LinearLayout
    }

    override fun getCount(): Int {
        return images.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.item_banner_holder, container, false)
        val image = view.imageView
        image.setImageURI(Uri.parse(Constants.BASE_URL+images[position].image),context)
        container.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as LinearLayout)
    }
}