package com.ibox.washapp.adapters

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ibox.washapp.R
import com.ibox.washapp.utils.CommonFunctions
import com.ibox.washapp.utils.Constants
import com.ibox.washapp.viewModels.CommonViewModel
import kotlinx.android.synthetic.main.item_basket.view.*

class BasketProductAdapter(
    val items: ArrayList<com.ibox.washapp.model.Product>, val context: Context,
    val activity: AppCompatActivity
) :
    RecyclerView.Adapter<BasketProductAdapter.ViewHolder>() {
    var commonViewModel = CommonViewModel()
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val viewHolder = ViewHolder(
            LayoutInflater.from(context)
                .inflate(R.layout.item_basket, parent, false)
        )
        viewHolder.init()
        return viewHolder
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun notifyData(ites: MutableList<com.ibox.washapp.model.Product>) {
        items.addAll(ites)
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return position

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tvTraditional.text = items[position].name
//        holder.itemView.ivcardViewImage.setImageURI(
//            Uri.parse(Constants.BASE_URL + items[position].image),
//            context
//        )
        if (!items[position].image.isNullOrEmpty())
        {Glide.with(context).load(Constants.BASE_URL + items[position].image).into(holder.itemView.ivcardViewImage)}
        else
        {Glide.with(context).load(R.drawable.product_placeholder).centerCrop().into(holder.itemView.ivcardViewImage)}
        if (items[position].quantity == 1) {
            holder.itemView.tvPlus.text = "Remove"
        } else {
            holder.itemView.tvPlus.text = "add to basket"
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun init() {
            itemView.tvPlus.setOnClickListener()
            {
                commonViewModel.getUser(context)
                if (itemView.tvPlus.text.toString().toLowerCase() == "add to basket") {
                    items[adapterPosition].quantity = 1
                } else {
                    items[adapterPosition].quantity = 0
                }
                CommonFunctions.addProductBasket(context, items[adapterPosition])
                val intent = Intent("AddedToBasket")
                intent.putExtra("Added", "AddedToBasketClicked")
                activity.sendBroadcast(intent)
                notifyItemChanged(adapterPosition)
            }
        }
    }
}