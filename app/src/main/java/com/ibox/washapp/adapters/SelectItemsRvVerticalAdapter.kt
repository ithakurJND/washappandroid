package com.ibox.washapp.adapters

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.ibox.washapp.R
import com.ibox.washapp.utils.CommonFunctions
import com.ibox.washapp.utils.Constants
import kotlinx.android.synthetic.main.item_rvvertical_select_items.view.*

class SelectItemsRvVerticalAdapter(
    val items: ArrayList<com.ibox.washapp.model.Product>, val context: Context,
    val activity: AppCompatActivity
) :
    RecyclerView.Adapter<SelectItemsRvVerticalAdapter.ViewHolder>() {
    private var isPrinted = false

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        if (!isPrinted) {
            Log.i("arrayGot", Gson().toJson(items))
            isPrinted = true
        }
        val viewHolder = ViewHolder(
            LayoutInflater.from(context)
                .inflate(R.layout.item_rvvertical_select_items, parent, false)
        )
        viewHolder.init()
        return viewHolder
    }

    override fun getItemCount(): Int {
        return items.size
    }
    override fun getItemViewType(position: Int): Int {
        return position

    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tvTraditional.text = items[position].name
        holder.itemView.tvNumOf.text = items[position].quantity.toString()
        holder.itemView.tvProce.text = "AED " +
                if (com.ibox.washapp.utils.Prefs.with(activity).getInt("serviceType", 1) == 1) items[position]
                    .regular_price else items[position].urgent_price
//        holder.itemView.ivcardViewImage.setImageURI(
//            Uri.parse(Constants.BASE_URL + items[position].image),
//            context
//        )
        if (!items[position].image.isNullOrEmpty())
        {
            Glide.with(context).load(Constants.BASE_URL + items[position].image).into(holder.itemView.ivcardViewImage)}
        else
        {Glide.with(context).load(R.drawable.product_placeholder).centerCrop().into(holder.itemView.ivcardViewImage)}
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun init() {
            itemView.ivPlus.setOnClickListener()
            {
                var sa = items[adapterPosition].quantity
                sa += 1
                items[adapterPosition].quantity = sa
                CommonFunctions.addProduct(context, items[adapterPosition])
                notifyDataSetChanged()
                val intent = Intent("Plus")
                intent.putExtra("clicked", "plus")
                activity.sendBroadcast(intent)
                Log.i("arrayUpdated", Gson().toJson(items))
                Log.i("SA","plus "+sa)
            }
            itemView.ivMinus.setOnClickListener {
                var sa = items[adapterPosition].quantity
                if (sa > 0) {
                    sa -= 1
                    items[adapterPosition].quantity = sa
                    Log.i("SA","minus "+sa)
                }
                CommonFunctions.addProduct(context, items[adapterPosition])
                items[adapterPosition].quantity = sa
                Log.i("SA","minus... "+sa)
                Log.i("arrayUpdated", Gson().toJson(items))
                notifyDataSetChanged()
                val intent = Intent("Plus")
                intent.putExtra("clicked", "plus")
                activity.sendBroadcast(intent)
            }
        }
    }
}