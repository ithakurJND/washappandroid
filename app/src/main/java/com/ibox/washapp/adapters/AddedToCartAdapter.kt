package com.ibox.washapp.adapters

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ibox.washapp.R
import com.ibox.washapp.model.OrderItem
import com.ibox.washapp.utils.Constants
import kotlinx.android.synthetic.main.item_added_to_cart.view.*

class AddedToCartAdapter(
    val items: ArrayList<OrderItem>, val items2: ArrayList<com.ibox.washapp.model.Product>,
    val context: Context, val change: Boolean, val int: Int
) :
    RecyclerView.Adapter<AddedToCartAdapter.ViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val viewHolder = ViewHolder(
            LayoutInflater.from(context)
                .inflate(R.layout.item_added_to_cart, parent, false)
        )
        return viewHolder
    }

    override fun getItemCount(): Int {
        if (change)
            return items.size
        else
            return items2.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (change)
        {
            holder.itemView.tvTraditional.text = items[position].product_name
            holder.itemView.tvAddedNo.text = items[position].quantity.toString()
            holder.itemView.tvProce.text = "AED " +
                    if (com.ibox.washapp.utils.Prefs.with(context).getInt("serviceType", 1) == 1)
                        (items[position].regular_price * items[position].quantity)
                    else
                        (items[position].urgent_price * items[position].quantity)
            if (!items[position].product_image.isNullOrEmpty())
            {
                Glide.with(context).load(Constants.BASE_URL + items[position].product_image).into(holder.itemView.ivcardViewImage)}
            else
            {
                Glide.with(context).load(R.drawable.product_placeholder).centerCrop().into(holder.itemView.ivcardViewImage)
            }
//            holder.itemView.ivcardViewImage.setImageURI(
//                Uri.parse(Constants.BASE_URL + items[position].product_image),
//                context
//            )
        }
        else {
            if (int==1)
            {
                holder.itemView.tvTraditional.text = items2[position].product_name
                holder.itemView.tvAddedNo.text = items2[position].quantity.toString()
                holder.itemView.tvProce.text = "AED " +
                        if (com.ibox.washapp.utils.Prefs.with(context).getInt("serviceType", 1) == 1)
                            (items2[position].regular_price * items2[position].quantity)
                        else (items2[position].urgent_price * items2[position].quantity)
                if (!items2[position].product_image.isNullOrEmpty())
                {
                    Glide.with(context).load(Constants.BASE_URL + items2[position].product_image).into(holder.itemView.ivcardViewImage)}
                else
                {
                    Glide.with(context).load(R.drawable.placeholder_logo).centerCrop().into(holder.itemView.ivcardViewImage)}
//                holder.itemView.ivcardViewImage.setImageURI(
//                    Uri.parse(Constants.BASE_URL + items2[position].product_image),
//                    context
//                )
            }
            else
            {
                holder.itemView.tvTraditional.text = items2[position].name
                holder.itemView.tvAddedNo.text = items2[position].quantity.toString()
                holder.itemView.tvProce.text = "AED " +
                        if (com.ibox.washapp.utils.Prefs.with(context).getInt("serviceType", 1) == 1)
                            (items2[position].regular_price * items2[position].quantity)
                        else (items2[position].urgent_price * items2[position].quantity)

                if (!items2[position].image.isNullOrEmpty())
                {
                    Glide.with(context).load(Constants.BASE_URL + items2[position].image).into(holder.itemView.ivcardViewImage)}
                else
                {
                    Glide.with(context).load(R.drawable.placeholder_logo).centerCrop().into(holder.itemView.ivcardViewImage)}
//                holder.itemView.ivcardViewImage.setImageURI(
//                    Uri.parse(Constants.BASE_URL + items2[position].image),
//                    context
//                )
            }

        }
    }

    inner class ViewHolder(itemsView: View) : RecyclerView.ViewHolder(itemsView) {
    }
}