package com.ibox.washapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ibox.washapp.R
import com.ibox.washapp.model.Detail
import com.ibox.washapp.utils.CommonFunctions
import kotlinx.android.synthetic.main.item_review.view.*

class ReviewAdapter(val items: MutableList<Detail>, val context: Context) :
    RecyclerView.Adapter<ReviewAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewHolder = ViewHolder(
            LayoutInflater.from(context)
                .inflate(R.layout.item_review, parent, false)
        )
        viewHolder.init()
        return viewHolder
    }

    override fun getItemCount(): Int {
        return items.size
    }
    fun notifyData(ites: MutableList<Detail>) {
        items.addAll(ites)
        notifyDataSetChanged()
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tvShopName.text = items[position].rated_by
        holder.itemView.ratingBar.rating = items[position].rating
        holder.itemView.tvDescription.text = items[position].review
        holder.itemView.tvTime.text = CommonFunctions.formatDate(
            "yyyy-MM-dd'T'HH:mm:ss.sss'Z'",
            "dd-MMM-yy",
            items[position].created_at
        )

    }

    inner class ViewHolder(itemsView: View) : RecyclerView.ViewHolder(itemsView) {
        fun init() {

        }
    }


}
