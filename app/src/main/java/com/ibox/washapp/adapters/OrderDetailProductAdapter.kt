package com.ibox.washapp.adapters

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ibox.washapp.R
import com.ibox.washapp.utils.Constants
import kotlinx.android.synthetic.main.item_added_to_cart.view.*

class OrderDetailProductAdapter (val items: MutableList<com.ibox.washapp.model.Product>, val context: Context
                                 , val serviceType : Int) :
    RecyclerView.Adapter<OrderDetailProductAdapter.ViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val viewHolder = ViewHolder(
            LayoutInflater.from(context)
                .inflate(R.layout.item_added_to_cart, parent, false))
        return viewHolder
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tvTraditional.text = items[position].product_name
        holder.itemView.tvAddedNo.text = items[position].quantity.toString()
        try {
            holder.itemView.tvProce.text = "AED "+if (serviceType == 1)
                items[position].regular_price* items[position].quantity
            else items[position].urgent_price * items[position].quantity

        }catch (e:Exception){
            holder.itemView.tvProce.text = "AED 0"
        }
        if (!items[position].product_image.isNullOrEmpty())
        {
            Glide.with(context).load(Constants.BASE_URL + items[position].product_image).into(holder.itemView.ivcardViewImage)}
        else
        {
            Glide.with(context).load(R.drawable.product_placeholder).centerCrop().into( holder.itemView.ivcardViewImage)}

//        holder.itemView.ivcardViewImage.setImageURI(
//            Uri.parse(Constants.BASE_URL + items[position].product_image),
//            context
//        )
    }

    inner class ViewHolder(itemsView: View) : RecyclerView.ViewHolder(itemsView)
    {
    }
}