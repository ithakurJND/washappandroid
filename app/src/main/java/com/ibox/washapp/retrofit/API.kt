package com.ibox.washapp.retrofit

import com.ibox.washapp.model.*
import retrofit2.Call
import retrofit2.http.*


interface API {
    @POST("api/user/login")
    fun UserLogin(@Body loginModel: LoginModel): Call<UserLogin>

    @POST("api/user/signup")
    fun UserSignUp(@Body signUpModel: SignUpModel): Call<UserLogin>

    @POST("api/user/forgot-password")
    fun forgotPassword(@Body email: HashMap<String, String>): Call<Message>

    @PUT("api/user")
    fun phoneNumber(@Header("Authorization") token: String, @Body phone: HashMap<String, String>): Call<EditProfile>

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "api/user/logout", hasBody = true)
    fun logout(@Header("Authorization") token: String, @Field("fcm_id") fcm_id: HashMap<String, String>): Call<EditProfile>

    @GET("api/basket-products")
    fun addToBasket(@Header("Authorization") token: String): Call<List<com.ibox.washapp.model.PojoOrderCategory>>

    @GET("api/basket-products")
    fun addToBasketPaging(@Header("Authorization") token: String, @QueryMap map: HashMap<String, Any>): Call<List<com.ibox.washapp.model.PojoOrderCategory>>

    @GET("api/basket-laundries")
    fun basketLaundriesList(@Header("Authorization") token: String, @QueryMap map: HashMap<String, Any>): Call<MutableList<LaundryList>>

    @GET("api/laundries")
    fun laundriesList(@Header("Authorization") token: String, @QueryMap map: HashMap<String, Any>): Call<MutableList<LaundryList>>

    @GET("api/static/categories")
    fun getCategories(): Call<List<com.ibox.washapp.model.PojoCategory>>

    @GET("api/static/order-categories")
    fun getOrderCategories(@Query("laundry_id") id: Int): Call<List<com.ibox.washapp.model.PojoOrderCategory>>

    @POST("api/order")
    fun createOrder(@Header("Authorization") token: String, @Body map: HashMap<String, Any>): Call<OrderResponeLaundry>


    @PUT("api/order")
    fun editOrder(@Header("Authorization") token: String, @Body map: HashMap<String, Any>): Call<OrderResponeLaundry>


    @POST("api/basket-order")
    fun createBasketOrder(@Header("Authorization") token: String, @Body map: HashMap<String, Any>): Call<OrderResponeLaundry>


    @PUT("api/basket-order")
    fun editBasketOrder(@Header("Authorization") token: String, @Body map: HashMap<String, Any>): Call<OrderResponeLaundry>


    @GET("api/static/locations")
    fun locations(@QueryMap map: HashMap<String, Any>): Call<NewLocations>

    @GET("api/static/time-slots")
    fun getTimeSlots(): Call<List<com.ibox.washapp.model.PojoTimeSlot>>

    @GET("api/static/preferrences")
    fun getPreferences(): Call<List<com.ibox.washapp.model.PojoPreference>>


    @GET("/api/static/home")
    fun getBanner(@Header("Authorization") token: String): Call<HomeModel>

    @GET("api/user")
    fun user(@Header("Authorization") token: String): Call<User>


    @GET("api/order/list")
    fun getOrderListing(@Header("Authorization") token: String, @QueryMap map: HashMap<String, Any>): Call<List<com.ibox.washapp.model.PojoOrderListing>>


    @GET("api/user/schedule")
    fun getSchedules(@Header("Authorization") token: String): Call<List<com.ibox.washapp.model.PojoSchdeule>>

    @GET("api/notification/notif-list")
    fun getNotifications(@Header("Authorization") token: String): Call<com.ibox.washapp.model.PojoNotification>

    @GET("api/order")
    fun getOrderDetails(@Header("Authorization") token: String, @Query("order_id") orderId: Int): Call<com.ibox.washapp.model.PojoOrderDetails>


    @POST("api/user/schedule")
    fun postSchedules(@Header("Authorization") token: String, @Body map: HashMap<String, Any>): Call<com.ibox.washapp.model.PojoSuccess>


    @POST("api/static/contact-us")
    fun contactUs(@Header("Authorization") token: String, @Body map: HashMap<String, Any>): Call<com.ibox.washapp.model.PojoSuccess>


    @PUT("api/user/change-password")
    fun changePassword(@Header("Authorization") token: String, @Body passwords: HashMap<String, String>): Call<EditProfile>

    @POST("api/user/verify-otp")
    fun otpVerify(@Header("Authorization") token: String, @Body otp: HashMap<String, String>): Call<Message>

    @POST("api/user/resend-otp")
    fun resendOtp(@Header("Authorization") token: String, @Body otp: HashMap<String, String>): Call<Message>


    @POST("api/apply-coupon")
    fun applyCoupon(@Header("Authorization") token: String, @Body otp: HashMap<String, Any>): Call<com.ibox.washapp.model.PojoSuccess>


    @POST("api/payment")
    fun payment(@Header("Authorization") token: String, @Body otp: HashMap<String, Any>): Call<com.ibox.washapp.model.PojoSuccess>


    @POST("api/user/address")
    fun addAddress(@Header("Authorization") token: String, @Body addaddress: AddAddress): Call<EditProfile>


    @POST("api/order/rating")
    fun rateUs(@Header("Authorization") token: String, @Body addaddress: HashMap<String, Any>): Call<com.ibox.washapp.model.PojoSuccess>


    @PUT("api/user/address")
    fun editAddress(@Header("Authorization") token: String, @Body addaddress: AddAddress): Call<EditProfile>


    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "api/user/address", hasBody = true)
    fun deleteAddress(@Header("Authorization") token: String, @Field("address_id") fcm_id: Int): Call<EditProfile>

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "api/order/cancel", hasBody = true)
    fun cancelOrder(@Header("Authorization") token: String, @Field("order_id") fcm_id: Int): Call<com.ibox.washapp.model.PojoSuccess>

    @GET("api/user/dismiss-rating")
    fun dismissRating(@Header("Authorization") token: String): Call<com.ibox.washapp.model.PojoSuccess>

    @GET("/api/user/laundry-reviews")
    fun getReviewAl(@Header("Authorization") token: String, @QueryMap map: HashMap<String, Any>): Call<ReviewResponse>

    @GET("api/static/order-products")
    fun getPaging(@Header("Authorization") token: String, @QueryMap map: HashMap<String, Int>): Call<List<com.ibox.washapp.model.Product>>

    @POST("getRSAKey")
    fun getRSA(@QueryMap map: HashMap<String, Any>): Call<String>

    ///request-payment
    @POST("api/get-rsa")
    fun getRsaKey(@Body ap: HashMap<String, Any>): Call<GetRSA>
}

