package com.ibox.washapp.retrofit;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.ibox.washapp.R;
import com.ibox.washapp.activities.OrderDetailActivity;

public class MyFcmListenerService extends FirebaseMessagingService {


    @Override
    public void onMessageReceived(RemoteMessage message) {
        sendNotification(message);
    }
    //This method is only generating push notification

    private void sendNotification(RemoteMessage remoteMessage) {
        Intent intent = new Intent(this, OrderDetailActivity.class)
                .putExtra("id", Integer.parseInt(remoteMessage.getData().get("order_id")));
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, PendingIntent.FLAG_ONE_SHOT);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        String channelId = getString(R.string.defult_id);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId, "General", NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(channel);
        }

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId)
                .setContentTitle(remoteMessage.getData().get("subject"))
                .setContentText(remoteMessage.getData().get("notif_msg"))
                .setAutoCancel(true)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(remoteMessage.getData().get("notif_msg")))
                .setSound(defaultSoundUri)/*gtin*/
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.ic_stat_washapp_logo)
                .setColor(getResources().getColor(R.color.drakBlue));
        notificationManager.notify((int) System.currentTimeMillis(), notificationBuilder.build());
    }
}