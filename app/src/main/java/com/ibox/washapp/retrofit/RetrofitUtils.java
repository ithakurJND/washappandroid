package com.ibox.washapp.retrofit;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okio.Buffer;


public class RetrofitUtils {

    private static final String MIME_TYPE_TEXT = "text/plain";
    private static final String MIME_TYPE_IMAGE = "image/*";

    public static RequestBody StringtoRequestBody(String string){
        return RequestBody.create(MediaType.parse(MIME_TYPE_TEXT), string);
    }

    public static RequestBody ImagetoRequestBody(File file){
        return RequestBody.create(MediaType.parse(MIME_TYPE_IMAGE), file);
    }
    public static MultipartBody.Part imageToRequestBody(File file, String fieldName) {
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);

        // MultipartBody.Part is used to send also the actual file name

        return MultipartBody.Part.createFormData(fieldName, file.getName(), requestFile);
    }

    public static String bodyToString(final RequestBody request) {
        try {
            final Buffer buffer = new Buffer();
            if (request != null)
                request.writeTo(buffer);
            else
                return "";
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }

}
