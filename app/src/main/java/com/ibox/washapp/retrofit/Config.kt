package com.ibox.washapp.retrofit

/**
 * credential used for creating dev, test and live build
 */
object Config {

    private var BASE_URL = ""
    private val appMode = AppMode.DEV


    internal val baseURL: String
        get() {
            init(appMode)
            return BASE_URL
        }


    /**
     * Initialize all the variable in this method
     */
    fun init(appMode: AppMode) {
        when (appMode) {

            AppMode.RDS -> {
                BASE_URL = "http://163.47.10.222:8000"
            }
            AppMode.DEV -> {
                BASE_URL = "https://app.washapp.ae:8001"
                    //"http://18.224.18.13:8000"

            }
            AppMode.TEST -> {
                BASE_URL = "http://178.128.101.31:8000"
            }

        }
    }

    enum class AppMode {
        DEV, TEST,RDS
    }
}