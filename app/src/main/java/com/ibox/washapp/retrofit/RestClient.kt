package com.techautovity.myeve.retrofit


import com.google.gson.GsonBuilder
import com.ibox.washapp.retrofit.API
import com.ibox.washapp.retrofit.Config
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


/**
 * Rest client
 */
object RestClient {
    private var apiServiceModel: API? = null

    /**
     * By using this method your response will be coming in  modal class object
     *
     * @return object of ApiService interface
     */
    // .client(fetchHeaders(context))
    val modalApiService: API
        get() {
            val gson = GsonBuilder()
                    .setLenient()
                    .create()
            if (apiServiceModel == null) {

                val okHttpClient = OkHttpClient()
                val retrofit = Retrofit.Builder()
                        .baseUrl(Config.baseURL)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .client(getOkHttpClient())
                           // okHttpClient.newBuilder().connectTimeout(60, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS).writeTimeout(60, TimeUnit.SECONDS).build()
                        .build()

                apiServiceModel = retrofit.create(API::class.java)
            }
            return apiServiceModel!!
        }
    fun getOkHttpClient(): OkHttpClient {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        val builder = OkHttpClient.Builder()
        builder.connectTimeout(60, TimeUnit.SECONDS)
        builder.readTimeout(60, TimeUnit.SECONDS)
        builder.writeTimeout(60, TimeUnit.SECONDS)
        builder.addNetworkInterceptor(httpLoggingInterceptor)
//        builder.addInterceptor(object : Interceptor {
//            override fun intercept(chain: Interceptor.Chain): Response {
//                val request = chain.request()
//                val header = request.newBuilder().header(
//                    "Authorization", ApplicationGlobal.sessionId)
//                val build = header.build()
//                return chain.proceed(build)
//            }
//
//        })
        return builder.build()
    }


}